#!/usr/bin/env bash
# (c) https://github.com/MontiCore/monticore  

# This script re-compile all *.fmu files in the current directory using
# current available c++ compiler. This makes the fmu files compatible with
# the host OS.

# For example if you have .fmu files exported from ModelicaEditor running
# in Windows but you need linux version. To make these .fmu files compatible
# with linux, you should:
#   1. import and re-export the Windows version .fmu files using linux ModelicaEditor
#   2. copy this script in the same directory where the re-exported .fmu files are located
#   3. run this script with ./recompile_fmu.sh
#   4. all .fmu files should be re-compiled and compatible with linux

tmp_dir=./unzip_dir

function compile() {
  filename="$1"
  unzip $filename -d $tmp_dir
  cd ${tmp_dir}/sources && ./configure && make
  cd ../..
  zip -r $filename $tmp_dir
  rm -rf $tmp_dir
  echo "$filename"
}

for f in ./*.fmu; do
    compile "$f"
done

