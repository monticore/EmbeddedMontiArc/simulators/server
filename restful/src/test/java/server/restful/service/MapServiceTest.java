/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.service;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;
import server.restful.dao.MapDAO;
import server.restful.util.Util;

import java.io.FileNotFoundException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class MapServiceTest {
    int mapID;

    @Before
    public void skipOnWindows() {
        // TODO: may work on windows, if you can get metis to work
        //Assume.assumeFalse(Util.isWindows());
    }

    @Before
    public void setUp(){
        mapID = MapDAO.create("straight.osm", getClass().getResource("/straight.osm").getPath());
    }

    @Test
    public void getNearestOsmVertex() {
        Vertex result;
        long expectedVertex = 5170132468L;
        double expectedLat = 50.8354394;
        double expectedLong = 5.8813385;

        result = MapService.getNearestVertex(getClass().getResource("/straight.osm").getPath(), expectedLong, expectedLat);
        long actualVertex = result.getOsmId();
        assertEquals(expectedVertex, actualVertex);

        // add some offset
        result = MapService.getNearestVertex(getClass().getResource("/straight.osm").getPath(), expectedLong + 1e-5, expectedLat + 1e-5);
        actualVertex = result.getOsmId();
        assertEquals(expectedVertex, actualVertex);

        // try coordinate of another node
        result = MapService.getNearestVertex(getClass().getResource("/straight.osm").getPath(),5.8804947, 50.8368530);
        actualVertex = result.getOsmId();
        assertNotEquals(5170132468L, actualVertex);
    }

    @Test
    public void sectorizeMap() throws Exception {
        // TODO mock MetisGraph and OverlayUtil
        MapService mapService = new MapService(mapID, 1);
        mapService.sectorizeMap();

        mapService = new MapService(mapID, 3);
        mapService.sectorizeMap();
    }

    @Test
    public void getSectorIdxByOsmID() throws FileNotFoundException {
        MapService mapService = new MapService(mapID, 1);
        assertEquals(getClass().getResource("/straight.osm").getPath(), mapService.getSectorMapByIdx(0));
        assertEquals(getClass().getResource("/straight.osm").getPath(), mapService.getSectorMapByIdx(99));

        mapService = new MapService(mapID, 2);
        String basePath = getClass().getResource("/straight.osm").getPath().replace(".osm", "");
        if (Util.isWindows()) {
            basePath = basePath.substring(1);
        }
        assertEquals(basePath + ".part.2-0.osm", mapService.getSectorMapByIdx(0));
        assertEquals(basePath + ".part.2-1.osm", mapService.getSectorMapByIdx(1));
    }

    @Test
    public void getSectorMapByIdx() throws Exception {
        MapService mapService = new MapService(mapID, 1);
        assertEquals(0, mapService.getSectorIdxByOsmID(0));
        assertEquals(0, mapService.getSectorIdxByOsmID(-1));

        mapService = new MapService(mapID, 2);
        assertEquals(0, mapService.getSectorIdxByOsmID(41956861L));
        assertEquals(1, mapService.getSectorIdxByOsmID(5170132217L));
    }
}
