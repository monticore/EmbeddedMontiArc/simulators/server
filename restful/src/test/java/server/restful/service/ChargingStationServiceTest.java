/* (c) https://github.com/MontiCore/monticore */
package server.restful.service;

import org.junit.Test;
import server.restful.dao.MapDAO;

import java.util.List;
import java.util.Map;

import static junit.framework.TestCase.assertEquals;

public class ChargingStationServiceTest {

    @Test
    public void getNearestChargingStation() throws Exception {
        int mapID = MapDAO.create("charging_station", getClass().getResource("/charging_station.osm").getPath());
        MapService svc = new MapService(mapID, 2);
        if (!svc.isSectorized()) {
            svc.sectorizeMap();
        }
        long result = ChargingStationService.getNearestChargingStation(svc, 383872737L);
        assertEquals(4846216969L, result);
    }

    @Test
    public void listChargingStationOsmID() {
        List<Map<String, String>> result = ChargingStationService.listChargingStationOsmID(getClass().getResource("/charging_station.osm").getPath());
        assertEquals(2, result.size());
        assertEquals("2871079478", result.get(0).get("id"));
        assertEquals("99971079478", result.get(1).get("id"));

        result = ChargingStationService.listChargingStationOsmID(getClass().getResource("/straight.osm").getPath());
        assertEquals(0, result.size());
    }
}
