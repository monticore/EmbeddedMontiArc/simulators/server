/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.service;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;
import org.jgrapht.Graph;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import server.restful.dao.MapDAO;
import server.restful.graph.MetisGraph;
import server.restful.graph.OverlayUtil;
import server.restful.graph.structures.OverlayEdge;

import java.io.FileNotFoundException;
import java.util.*;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class NavigationServiceTest {
    private final String mapPath = getClass().getResource("/Aachen.osm").getPath();
    private MapService mapService;
    private NavigationService navService;

    @Before
    public void setUp() throws Exception {
        int id = MapDAO.create("aachen", mapPath);
        mapService = new MapService(id, 4);
        if (!mapService.isSectorized()) {
            mapService.sectorizeMap();
        }
        navService = new NavigationService();
    }


    @Test
    public void getShortestPath() throws FileNotFoundException {
        Graph<Vertex, OverlayEdge> g = OverlayUtil.readOverlay(getClass().getResource("/overlay.dot").getPath());
        Object[] vertices = g.vertexSet().toArray();
        int length = NavigationService.findShortestOverlayPath(g, (Vertex) vertices[0], (Vertex) vertices[2]).getLength();
        Assert.assertTrue(length > 0);
    }

    @Test
    public void findShortestPath() throws Exception {
        long source = 1245248648;
        long target = 60698562;

        MetisGraph metis = new MetisGraph(mapPath, 1);
        List<Vertex> vertices = metis.getOriginalGraph().getVertices();
        // Only test 5% of source <-> target pairs
        List<Vertex> testVertices = new ArrayList<>();
        for (Vertex v : vertices) {
            if (Math.random() > 0.95) {
                testVertices.add(v);
            }
        }
        for (Vertex s : testVertices) {
            for (Vertex t : testVertices) {
                if (s.equals(t)) continue;
                testTrajectory(s.getOsmId(), t.getOsmId());
            }
        }
//        testTrajectory(4180733590L, 3671293038L, navService, mapService);
    }

    private void testTrajectory(long source, long target) throws NavigationService.PathNotExistException, FileNotFoundException {
        double referenceDist = 0;
        try {
            List<Vertex> trajectory = navService.findShortestOSMPath(mapService.getMap().getPath(), source, target);
            printTrajectory(mapService, trajectory);
            // distance computed with dijkstra on the whole mapService
            referenceDist = navService.getDistance(trajectory);
            //System.out.println(referenceDist);
        } catch (NavigationService.PathNotExistException e) {
            return;
        }

        List<Vertex> result = navService.findShortestPath(mapService, source, target);
        List<Vertex> temp = new ArrayList<>();
        for (Vertex v : result) {
            temp.add(mapService.getVertexByOsmID(v.getOsmId()));
        }
        printTrajectory(mapService, temp);

        double distance = navService.getDistance(temp);
        //System.out.println(distance);
        Assert.assertTrue(distance <= referenceDist);
    }

    private void printTrajectory(MapService map, List<Vertex> vertices) {
        List<String> str = new ArrayList<>();
        for (Vertex v : vertices) {
            str.add(v.getOsmId() + "(" + map.getSectorIdxByOsmID(v.getOsmId()) + ")");
        }
        //System.out.println(String.join("->", str));
    }

    @Test
    public void computeTrajectorySegments() {
        Map<Long, Integer> nodeSectorMapping = new HashMap<Long, Integer>() {{
            put(10000L, 0); put(20000L, 0); put(30000L, 0); put(40000L, 0);
            put(10001L, 1); put(20001L, 1); put(30001L, 1); put(40001L, 1);
            put(10002L, 2); put(20002L, 2); put(30002L, 2); put(40002L, 2);
            put(10003L, 3); put(20003L, 3); put(30003L, 3); put(40003L, 3);
        }};
        MapService mapService = mock(MapService.class);
        for (long node : nodeSectorMapping.keySet()) {
            when(mapService.getSectorIdxByOsmID(node)).thenReturn(nodeSectorMapping.get(node));
        }

        List<List<Long>> trajectories = new ArrayList<>();
        trajectories.add(Collections.singletonList(10000L));
        trajectories.add(new ArrayList<>(Arrays.asList(10000L, 10000L, 10000L, 10000L)));
        trajectories.add(new ArrayList<>(Arrays.asList(10000L, 20000L, 30000L)));
        trajectories.add(new ArrayList<>(Arrays.asList(10000L, 20000L, 10003L, 10002L, 20002L)));
        trajectories.add(new ArrayList<>(Arrays.asList(10000L, 20000L, 10003L, 10002L, 20001L)));
        trajectories.add(new ArrayList<>(Arrays.asList(10000L, 20000L, 10003L, 10000L, 20000L, 30000L)));
        trajectories.add(new ArrayList<>(Arrays.asList(10000L, 20000L, 10001L, 10003L, 20003L, 30000L)));
        trajectories.add(new ArrayList<>(Arrays.asList(10000L, 20000L, 10003L, 10000L, 20000L, 30002L)));
        trajectories.add(new ArrayList<>(Arrays.asList(10000L, 20001L, 10002L, 10003L)));
        trajectories.add(new ArrayList<>(Arrays.asList(10000L, 20001L, 10002L, 10000L)));
        trajectories.add(new ArrayList<>(Arrays.asList(10000L, 20001L, 10002L, 10001L)));


        // add some random trajectories
        Random rand = new Random();
        for (int i = 0; i < 500; i++) {
            trajectories.add(getRandomTrajectory(rand.nextInt(50) + 1, nodeSectorMapping));
        }

        for (List<Long> trajectory : trajectories) {
            List<List<Long>> result;
            try {
                result = navService.computeTrajectorySegments(mapService, trajectory);
            } catch (AssertionError e) {
                continue;
            }

            if (result.get(0).size() > 1){
                Assert.assertEquals(result.get(0).get(0), trajectory.get(0));
                List<Long> lastSegment = result.get(result.size() - 1);
                Assert.assertEquals(lastSegment.get(lastSegment.size() - 1), trajectory.get(trajectory.size() - 1));
            }
            validateTrajectorySegments(mapService, result);
        }
    }

    private void validateTrajectorySegments(MapService mapService, List<List<Long>> segments) {
        // skip trivial trajectory
        if (segments.size() == 1 && segments.get(0).size() <= 1 ){
            return;
        }

        // each segment has size >= 2
        // segment belongs to sector with smallest sector number
        // each segment has cut edge to sector with larger sector number
        // cut edge connects only current sector to a other
        for(List<Long> segment: segments){
            Assert.assertTrue(segment.size() >= 2);
            int segmentBelongsToSector = segment.stream()
                    .map(mapService::getSectorIdxByOsmID).min(Integer::compare).get();
            for (int i = 1; i < segment.size(); i++) {
                Assert.assertTrue(
                        mapService.getSectorIdxByOsmID(segment.get(i - 1)) == segmentBelongsToSector
                                || mapService.getSectorIdxByOsmID(segment.get(i)) == segmentBelongsToSector);

                Assert.assertTrue(mapService.getSectorIdxByOsmID(segment.get(i)) >= segmentBelongsToSector);
            }

        }
    }

    private List<Long> getRandomTrajectory(int size, Map<Long, Integer> mapping) {
        List<Long> result = new ArrayList<>();
        Random rand = new Random();
        List<Long> keyList = new ArrayList<>(mapping.keySet());
        for (int i = 0; i < size; i++) {
            int idx = rand.nextInt(keyList.size());
            result.add(keyList.get(idx));
        }
        return result;
    }
}
