///* (c) https://github.com/MontiCore/monticore */
package server.restful.service;

import junit.framework.TestCase;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import server.restful.dao.MapDAO;
import server.restful.dao.ScenarioDAO;
import server.restful.dao.SimulationDAO;
import server.restful.model.Dataframe;
import server.restful.model.ScenarioModel;
import server.restful.registry.RegistryFactory;
import server.restful.util.Util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Properties;

public class IntegrationTest extends TestCase {

    /**
     * This test will execute a complete simulation task using chargingstation.sim.
     * In the simulation, we create a vehicle with 19.9% initial battery percentage.
     * It will
     *   1. query the restful server for the nearest charging station
     *   2. re-plan its trajectory to drive there
     *   3. after it's fully charged (battery > 99%), drive to its original destination
     *
     * @throws FileNotFoundException
     */
    @Test
    public void testSimulation() throws Exception {
        Logger logger = LoggerFactory.getLogger(IntegrationTest.class);
        ScenarioModel scenario = ScenarioService.saveScenario(
                new FileInputStream(getClass().getResource("/chargingstation.sim").getPath()),
                "uploaded.sim"
        );
        int mapID = MapDAO.create("chargingstation.osm", getClass().getResource("/chargingstation.osm").getPath());
        int simID = SimulationDAO.create(scenario.getId(), 1);
        SimulationService sim = new SimulationService(simID);

        sim.startSimulation(RegistryFactory.getZookeeperRegistry());

        // wait for battery to be charged from 19% to 100%, it will take some time...
        // if we can set the charging battery target to a lower value rather than 100%,
        // it will be much quicker.
        while(!sim.isSimulationFinished()){
            logger.info("Waiting for simulation result...");
            Thread.sleep(5000);
        }

        List<Dataframe> frames = sim.getResult().get(0).getFrames();

        // expect 1 vehicle
        assertEquals(1, sim.getResult().size());
        assertTrue(frames.size() > 0);

        // not testing EV for now
//        double initialBattery = frames.get(0).getBattery();
//        double finalBattery = frames.get(frames.size() - 1).getBattery();
        // after charging, final battery percentage should be higher than initial
//        assertTr ue(finalBattery > initialBattery);

    }

    @Test
    public void testConfig() {
        int scenarioID = ScenarioDAO.create("straight.sim", getClass().getResource("/straight.sim").getPath());
        int mapID = MapDAO.create("straight.osm", getClass().getResource("/straight.osm").getPath());
        int simID = SimulationDAO.create(scenarioID, 2);
        SimulationService sim = new SimulationService(simID);
        Properties prop = Util.getProperties();
        assertTrue(prop.size() > 0);
    }

//    @Test
    public void testGetScenario() throws FileNotFoundException {
        ScenarioModel scenario = ScenarioService.saveScenario(
                new FileInputStream(getClass().getResource("/straight.sim").getPath()),
                "my-uploaded-scenario.sim"
        );
        int mapID = MapDAO.create("straight.osm", getClass().getResource("/straight.osm").getPath());
        int simID = SimulationDAO.create(scenario.getId(), 2);
        SimulationService sim = new SimulationService(simID);
        sim.getScenario();
    }
}
