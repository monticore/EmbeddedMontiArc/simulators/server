/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.service;

import org.junit.Test;
import server.restful.model.MapModel;

import static org.junit.Assert.*;

public class WorldBuilderServiceTest {
    @Test
    public void buildWorld() throws Exception {
        WorldBuilderService wb = new WorldBuilderService(getClass().getResource("/Aachen.osm").getPath());
        MapModel result = wb.buildWorld();
        assertTrue(result.getStreets().size() > 0);
        assertTrue(result.getBuildings().size() > 0);
//        assertTrue(result.getWaterways().size() > 0); // Aachen.osm has no waterway
        assertTrue(result.getBounds() != null);
        assertTrue(result.getHeightMap().getHeightMapDeltaX() != 0);
        assertTrue(result.getHeightMap().getHeightMapDeltaY() != 0);
        assertTrue(result.getHeightMap().getHeightMapMinX() != 0);
        assertTrue(result.getHeightMap().getHeightMapMinY() != 0);
        assertTrue(result.getHeightMap().getHeightMapMaxX() != 0);
        assertTrue(result.getHeightMap().getHeightMapMaxY() != 0);
    }

    @Test
    public void buildWorldWithChargingStation() throws Exception {
        WorldBuilderService wb = new WorldBuilderService(getClass().getResource("/charging_station.osm").getPath());
        MapModel result = wb.buildWorld();
        assertTrue( result.getChargingStations().size() > 0);
    }
}
