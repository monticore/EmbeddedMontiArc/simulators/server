/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.service;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;
import org.junit.Assume;
import org.junit.BeforeClass;
import org.junit.Test;
import server.restful.dao.MapDAO;
import server.restful.graph.MetisGraph;
import server.restful.util.Util;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class VehicleServiceTest {
    static MetisGraph metis;
    static MapService mapService;

    @BeforeClass
    public static void stetUpClass() throws Exception {
        // TODO: may work on windows, if you can get metis to work
        //Assume.assumeFalse(Util.isWindows());

        String mapPath = VehicleService.class.getResource("/Aachen.osm").getPath();
        int numSectors = 6;
        int mapId = MapDAO.create("Aachen.osm", mapPath);
        metis = new MetisGraph(mapPath, numSectors);
        mapService = new MapService(mapId, numSectors);
        if (!mapService.isSectorized()){
            mapService.sectorizeMap();
        }
    }

    @Test
    public void calcTrajectorySegments() {
//        try {
//            new VehicleService(mapService, 5176404007L, 4180733590L, null);
//        } catch (VehicleService.PathNotExistException e) {
//            e.printStackTrace();
//        }
        int limit = 1000;
        int cnt = 0;
        for (Vertex s : metis.getOriginalGraph().getVertices()) {
            for (Vertex t : metis.getOriginalGraph().getVertices()) {
                if (s.equals(t)) {
                    continue;
                }

                if(metis.getOsmIdSectorMapping().get(s.getOsmId()) == 3 || metis.getOsmIdSectorMapping().get(t.getOsmId()) == 3){
                    continue;
                }


                // System.out.printf("%s(%d)->%s(%d)\n",
                //         s.getOsmId(),
                //         metis.getOsmIdSectorMapping().get(s.getOsmId()),
                //         t.getOsmId(),
                //         metis.getOsmIdSectorMapping().get(t.getOsmId())
                // );

                VehicleService vehicleService;
                try {
                   vehicleService = new VehicleService(mapService, s.getOsmId(), t.getOsmId(), null);
                } catch (NavigationService.PathNotExistException e) {
                    System.out.println(e.getMessage());
                    continue;
                } catch (NullPointerException e){
                    e.printStackTrace();
                    continue;
                }

                for (List<Long> segment : vehicleService.getTrajectories()) {
                    // each segments contains at least 2 way points
                    assertTrue(segment.size() >= 2);
                }

                if (cnt++ > limit) {
                    break;
                }

            }
        }
    }

    @Test
    public void navigateToChargingStationSingleSector() throws Exception {
        String mapPath = VehicleService.class.getResource("/charging_station.osm").getPath();
        int numSectors = 1;
        int mapId = MapDAO.create("charging_station.osm", mapPath);
        metis = new MetisGraph(mapPath, numSectors);
        mapService = new MapService(mapId, numSectors);
        if (!mapService.isSectorized()){
            mapService.sectorizeMap();
        }

        VehicleService svc = new VehicleService(mapService, 2194039104L, 2157509951L, null);
        List<List<Long>> trajectories = svc.getTrajectories();
        svc.insertNewRoadPoint(2194039104L, 4846216969L);
        List<List<Long>> newTrajectories = svc.getTrajectories();

        List<Long> lastSegment = newTrajectories.get(trajectories.size() - 1);

        assertEquals(lastSegment.get(lastSegment.size() - 1).longValue(), 2157509951L);
    }

    @Test
    public void navigateToChargingStationMultiSectors() throws Exception {
        String mapPath = VehicleService.class.getResource("/charging_station.osm").getPath();
        int numSectors = 3;
        int mapId = MapDAO.create("charging_station.osm", mapPath);
        metis = new MetisGraph(mapPath, numSectors);
        mapService = new MapService(mapId, numSectors);
        if (!mapService.isSectorized()){
            mapService.sectorizeMap();
        }

        // in sector 0
        long chargingStation = 4846216969L;

        // 5695350973L and 4846216969L are from sector 0
        VehicleService svc = new VehicleService(mapService, 5695350973L, 4846216969L, null);
        List<List<Long>> trajectories = svc.getTrajectories();
        svc.insertNewRoadPoint(2326537604L, chargingStation);
        List<List<Long>> newTrajectories = svc.getTrajectories();
        List<Long> lastSegment = newTrajectories.get(newTrajectories.size() - 1);
        assertEquals(4846216969L, lastSegment.get(lastSegment.size() - 1).longValue());

        // 2194039119L sector 0, 2157509951L sector 1
        svc = new VehicleService(mapService, 2194039119L, 2157509951L, null);
        trajectories = svc.getTrajectories();
        svc.insertNewRoadPoint(2326537604L, chargingStation);
        newTrajectories = svc.getTrajectories();
        boolean hasChargingStation = false;
        for (List<Long> segment: newTrajectories){
            for(long osmID: segment){
                if (osmID == chargingStation){
                    hasChargingStation = true;
                }
            }
        }
        assertTrue(hasChargingStation);
        lastSegment = newTrajectories.get(newTrajectories.size() - 1);
        assertEquals(2326537604L, newTrajectories.get(0).get(0).longValue());
        assertEquals(2157509951L, lastSegment.get(lastSegment.size() - 1).longValue());

        // 2552488907L sector 1, 25263620L sector 2
        svc = new VehicleService(mapService, 1102430952L, 5695348539L, null);
        trajectories = svc.getTrajectories();
        svc.insertNewRoadPoint(155412834L, chargingStation);
        newTrajectories = svc.getTrajectories();
        hasChargingStation = false;
        for (List<Long> segment: newTrajectories){
            for(long osmID: segment){
                if (osmID == chargingStation){
                    hasChargingStation = true;
                }
            }
        }
        assertTrue(hasChargingStation);
        lastSegment = newTrajectories.get(newTrajectories.size() - 1);
        assertEquals(155412834L, newTrajectories.get(0).get(0).longValue());
        assertEquals(5695348539L, lastSegment.get(lastSegment.size() - 1).longValue());
    }
}
