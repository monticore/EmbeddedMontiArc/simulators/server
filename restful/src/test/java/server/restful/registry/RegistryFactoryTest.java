/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.registry;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class RegistryFactoryTest {

    @Test
    public void getInstance() {
        assertTrue(RegistryFactory.newInstance() instanceof ZookeeperRegistry);
    }

    @Test
    public void getDefaultRegistry() {
        assertTrue(RegistryFactory.getDefaultRegistry() instanceof DefaultRegistry);
    }

    @Test
    public void getZookeeperRegistry() {
        assertTrue(RegistryFactory.getZookeeperRegistry() instanceof ZookeeperRegistry);
    }
}
