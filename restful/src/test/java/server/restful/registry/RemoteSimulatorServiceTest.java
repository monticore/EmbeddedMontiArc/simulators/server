/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.registry;

import org.I0Itec.zkclient.ZkClient;
import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;
import server.restful.registry.util.ZookeeperFactory;
import server.restful.util.Util;

import static org.junit.Assert.*;

public class RemoteSimulatorServiceTest {
    ZkClient zkClient;
    String path;
    String host;
    int port;

    @Before
    public void skip() {
        // TODO: this test should not be skipped, but is for now because it does not run (at least on windows, reason unknown)
        Assume.assumeFalse(Util.isWindows());
    }

    @Before
    public void setUp() throws Exception {
        zkClient = ZookeeperFactory.newInstance();
        if (zkClient.exists("/simulators")){
            zkClient.deleteRecursive("/simulators");
        }
        zkClient.createPersistent("/simulators");

        host = "sim-worker-111";
        port = 6000;
        path = String.format("/simulators/%s:%d", host, port);
        zkClient.createEphemeral(path);
    }

    @After
    public void tearDown() throws Exception {
        zkClient.deleteRecursive("/simulators");
    }

    @Test
    public void isAvailable() {
        RemoteSimulatorService svc = new RemoteSimulatorService(path);
        assertEquals(host, svc.getHost());
        assertEquals(port, svc.getPort());
        assertTrue(svc.isAvailable());

        zkClient.createEphemeral(String.format("%s/%s:%d", RemoteSimulatorService.OCCUPATION_PATH, host, port));
        assertFalse(svc.isAvailable());

        svc = new RemoteSimulatorService("/simulators/i-dont-exist:404");
        assertFalse(svc.isAvailable());
    }

    @Test
    public void reserveAndRelease() {
        RemoteSimulatorService svc = new RemoteSimulatorService(path);
        assertTrue(!zkClient.exists(RemoteSimulatorService.OCCUPATION_PATH)
                || zkClient.getChildren(RemoteSimulatorService.OCCUPATION_PATH).size() == 0
        );
        svc.reserve();
        assertEquals(host + ":" + port, zkClient.getChildren(RemoteSimulatorService.OCCUPATION_PATH).get(0));

        svc.release();
        assertEquals(0, zkClient.getChildren(RemoteSimulatorService.OCCUPATION_PATH).size());
        assertTrue(svc.isAvailable());
    }
}
