/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.registry.util;

import org.I0Itec.zkclient.ZkClient;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class LockTest {

    @Test
    public void getLockSequenceFromPath() {
        ZkClient zk = ZookeeperFactory.newInstance();
        Lock lock = new Lock("/resource", zk);
        assertEquals("0001", lock.getLockSequenceFromPath("/ak-path-example/lock-0001"));
        assertEquals("0001", lock.getLockSequenceFromPath("lock-0001"));
    }

    private class AcquireAndRelease implements Runnable{
        private Lock lock;
        private long releaseAfterMs;

        AcquireAndRelease(Lock lock, long releaseAfterMs) {
            this.lock = lock;
            this.releaseAfterMs = releaseAfterMs;
        }

        @Override
        public void run() {
            try {
                lock.acquire(0);
                Thread.sleep(releaseAfterMs);
            } catch (Lock.TimeoutException | InterruptedException | Lock.AlreadyAcquired ignored) {
            }finally {
                lock.release();
            }
        }
    }

    @Test
    public void aquire() throws Exception {
        ZkClient zk = ZookeeperFactory.newInstance();
        zk.deleteRecursive("/resource");

        List<Lock> locks = new ArrayList<>();
        for (int i=0;i<10;i++){
            locks.add(new Lock("/resource", zk));
        }

        try {
            locks.get(0).acquire(1000);
        } catch (Lock.TimeoutException | Lock.AlreadyAcquired e) {
            e.printStackTrace();
        }finally {
            locks.get(0).release();
        }

        boolean raisedTimeout = false, raisedAquired = false;
        try {
            locks.get(0).acquire(0);
            locks.get(1).acquire(0);
        } catch (Lock.TimeoutException e) {
            raisedTimeout = true;
        } catch (Lock.AlreadyAcquired ignored) {
        }finally {
            locks.get(0).release();
        }
        assertTrue(raisedTimeout);

        try {
            locks.get(0).acquire(0);
            locks.get(0).acquire(0);
        } catch (Lock.TimeoutException ignored) {
        } catch (Lock.AlreadyAcquired ignored) {
            raisedAquired = true;
        } finally {
            locks.get(0).release();
        }
        assertTrue(raisedAquired);


        try {
            AcquireAndRelease runnable = new AcquireAndRelease(locks.get(0), 1000);
            Thread thread = new Thread(runnable);
            thread.start();
            assertFalse(locks.get(1).acquired());
            locks.get(1).acquire(3000);

            thread.join();
            assertTrue(locks.get(1).acquired());
        } catch (Lock.TimeoutException | Lock.AlreadyAcquired | InterruptedException ignored) {
            throw new Exception("should not have any exception");
        }
    }

    @Test
    public void release() throws Lock.TimeoutException, Lock.AlreadyAcquired {
        ZkClient zk = ZookeeperFactory.newInstance();
        zk.deleteRecursive("/resource");

        Lock lock = new Lock("/resource", zk);
        lock.acquire(0);
        assertTrue(lock.acquired());
        assertTrue(zk.getChildren("/resource/locks").size() > 0);
        lock.release();
        assertEquals(0, zk.getChildren("/resource/locks").size());
        assertFalse(lock.acquired());
    }
}
