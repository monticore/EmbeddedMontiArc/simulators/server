/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.registry;

import org.junit.Test;

import static org.junit.Assert.*;

public class DefaultRegistryTest {

    @Test
    public void getAutopilot() {
        assertEquals("localhost", new DefaultRegistry().getAutopilot().getHost());
        assertEquals(10101, new DefaultRegistry().getAutopilot().getPort());
    }

    @Test
    public void getSimulator() {
        assertEquals("localhost", new DefaultRegistry().getSimulator().getHost());
        assertEquals(6000, new DefaultRegistry().getSimulator().getPort());
    }
}
