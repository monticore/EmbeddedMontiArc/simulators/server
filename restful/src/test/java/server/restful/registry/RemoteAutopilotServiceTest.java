/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.registry;

import org.I0Itec.zkclient.ZkClient;
import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;
import server.restful.registry.util.ZookeeperFactory;
import server.restful.util.Util;

import static org.junit.Assert.*;

public class RemoteAutopilotServiceTest {
    ZkClient zkClient;
    String path;
    String host;
    int port;

    @Before
    public void skip() {
        // TODO: this test should not be skipped, but is for now because it does not run (at least on windows, reason unknown)
        Assume.assumeFalse(Util.isWindows());
    }

    @Before
    public void setUp() throws Exception {
        zkClient = ZookeeperFactory.newInstance();
        if (!zkClient.exists(RemoteAutopilotService.BASE_PATH)){
            zkClient.createPersistent(RemoteAutopilotService.BASE_PATH, true);
        }

        host = "autopilot-worker-007";
        port = 10101;
        path = String.format(RemoteAutopilotService.ROOT_PATH + "/all/%s:%d", host, port);
        zkClient.createEphemeral(path);
    }

    @After
    public void tearDown() throws Exception {
        zkClient.deleteRecursive(RemoteAutopilotService.ROOT_PATH);
    }

    @Test
    public void isAvailable() {
        RemoteAutopilotService svc = new RemoteAutopilotService(path);
        assertEquals(host, svc.getHost());
        assertEquals(port, svc.getPort());
        assertTrue(svc.isAvailable());

        zkClient.createEphemeralSequential(String.format(
                "%s/busy/%s:%d/client-", RemoteAutopilotService.ROOT_PATH, host, port), null
        );
        assertFalse(svc.isAvailable());

        svc = new RemoteAutopilotService(RemoteAutopilotService.ROOT_PATH + "/i-dont-exist:404");
        assertFalse(svc.isAvailable());
    }

    @Test
    public void reserveAndRelease() {
        RemoteAutopilotService svc = new RemoteAutopilotService(path);
        assertFalse( zkClient.exists(RemoteAutopilotService.ROOT_PATH + "/busy"));
        assertTrue(svc.reserve());
        assertEquals(1, zkClient.getChildren(String.format(
                RemoteAutopilotService.ROOT_PATH + "/busy/%s:%d", host, port)).size()
        );

        svc.release();
        assertEquals(0, zkClient.getChildren(
                String.format(RemoteAutopilotService.ROOT_PATH + "/busy/%s:%d", host, port)).size()
        );
        assertTrue(svc.isAvailable());
    }

    @Test
    public void release() {
    }
}
