/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.registry;

import org.I0Itec.zkclient.ZkClient;
import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;
import server.restful.registry.util.ZookeeperFactory;
import server.restful.util.Util;

import static org.junit.Assert.*;

public class ZookeeperRegistryTest {
    ZkClient zkClient;
    ServiceRegistry registry;

    @Before
    public void skip() {
        // TODO: this test should not be skipped, but is for now because it does not run (at least on windows, reason unknown)
        Assume.assumeFalse(Util.isWindows());
    }

    @Before
    public void setUp() throws Exception {
        zkClient = ZookeeperFactory.newInstance();
        registry = RegistryFactory.getZookeeperRegistry();
        if(!zkClient.exists("/simulators")){
            zkClient.createPersistent("/simulators", true);
        }
        if(!zkClient.exists(RemoteAutopilotService.BASE_PATH)){
            zkClient.createPersistent(RemoteAutopilotService.BASE_PATH, true);
        }
    }

    @After
    public void tearDown() throws Exception {
        zkClient.deleteRecursive("/simulators");
        zkClient.deleteRecursive(RemoteAutopilotService.ROOT_PATH);
    }

    @Test
    public void getAutopilot() throws ServiceRegistry.NoServiceException {
        String host = "autopilot-50";
        int port = 10101;
        zkClient.createPersistent(RemoteAutopilotService.ROOT_PATH + "/busy", true);
        zkClient.createEphemeral(String.format("%s/%s:%d",RemoteAutopilotService.BASE_PATH, host, port));

        RemoteAutopilotService svc = registry.getAutopilot();
        assertEquals(host, svc.getHost());
        assertEquals(port, svc.getPort());

        boolean raised = false;
        try {
            registry.getAutopilot();
        }catch (ServiceRegistry.NoServiceException e){
            raised = true;
        }
        assertTrue(raised);
    }

    @Test
    public void getSimulator() throws ServiceRegistry.NoServiceException {
        String host = "simulator-201";
        int port = 6000;
        zkClient.createEphemeral(String.format("/simulators/%s:%d", host, port));

        RemoteSimulatorService svc = registry.getSimulator();
        assertEquals(host, svc.getHost());
        assertEquals(port, svc.getPort());

        boolean raised = false;
        try {
            registry.getSimulator();
        }catch (ServiceRegistry.NoServiceException e){
            raised = true;
        }
        assertTrue(raised);
    }

    @Test
    public void releaseAll() throws ServiceRegistry.NoServiceException {
        String simulators = "/simulators/simulator-201:6000";
        zkClient.createEphemeral(simulators);
        String autopilot = RemoteAutopilotService.BASE_PATH + "/autopilot-50:10101";
        zkClient.createEphemeral(autopilot);

        registry.getAutopilot();
        registry.getSimulator();

        assertTrue(zkClient.exists( "/simulators/occupied/simulator-201:6000"));
        assertEquals(1, zkClient.getChildren(RemoteAutopilotService.ROOT_PATH + "/busy/autopilot-50:10101").size());

        registry.releaseAll();

        assertFalse(zkClient.exists( "/simulators/occupied/simulator-201:6000"));
        assertEquals(0, zkClient.getChildren(RemoteAutopilotService.ROOT_PATH + "/busy/autopilot-50:10101").size());
    }
}
