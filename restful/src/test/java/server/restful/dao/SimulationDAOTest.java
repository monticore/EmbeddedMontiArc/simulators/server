/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.dao;

import org.junit.Before;
import org.junit.Test;
import server.restful.model.Dataframe;
import server.restful.model.SimulationModel;
import server.restful.model.VehicleModel;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class SimulationDAOTest {
    @Before
    public void setUp(){
        Connection c = BaseDAO.getConnection();
        try {
            c.createStatement().executeUpdate("DELETE FROM scenario");
            c.commit();
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void create() {
        int numRecords = SimulationDAO.getAll().size();

        int simID = SimulationDAO.create(-1, 100);
        SimulationModel simulation = SimulationDAO.getByID(simID);
        assertEquals(simID, simulation.getId());
        assertEquals(-1, simulation.getScenarioID());
        assertEquals(100, simulation.getNumSectors());

        // create 4 new records in this test
        SimulationDAO.create(-1, 100);
        SimulationDAO.create(-1, 100);
        SimulationDAO.create(-1, 100);

        assertEquals(4, SimulationDAO.getAll().size() - numRecords);
    }

    @Test
    public void storeSimulationResult() throws IOException {
        // construct a simulation result
        List<VehicleModel> simResult = new ArrayList<>();
        VehicleModel vehicle = new VehicleModel();
        Dataframe frame = new Dataframe(){{
            setBrake(3);
            setEngine(0.1);
            setSteering(0.5);
            setPosX(100);
            setPosY(200);
            setPosZ(0);
            setDeltaTime(33);
            setTotalTime(1000);
        }};
        vehicle.setFrames(new ArrayList<Dataframe>(){{add(frame);}});
        vehicle.setId("vid");
        simResult.add(vehicle);

        int simID = SimulationDAO.create(-1, 1);
        assertNull(SimulationDAO.getByID(simID).getResultPath());

        // store it to disk
        SimulationDAO.storeSimulationResult(simID, simResult);
        assertNotEquals("", SimulationDAO.getByID(simID).getResultPath());

        // read the simulation result from disk and compare with the original one
        VehicleModel deserializedResult = SimulationDAO.getSimulationResultByID(simID).get(0);

        assertEquals(vehicle.getId(), deserializedResult.getId());
        assertEquals(frame.getBrake(), deserializedResult.getFrames().get(0).getBrake(), 1e-5);
        assertEquals(frame.getEngine(), deserializedResult.getFrames().get(0).getEngine(), 1e-5);
        assertEquals(frame.getSteering(), deserializedResult.getFrames().get(0).getSteering(), 1e-5);
        assertEquals(frame.getPosX(), deserializedResult.getFrames().get(0).getPosX(), 1e-5);
        assertEquals(frame.getPosY(), deserializedResult.getFrames().get(0).getPosY(), 1e-5);
        assertEquals(frame.getPosZ(), deserializedResult.getFrames().get(0).getPosZ(), 1e-5);
        assertEquals(frame.getDeltaTime(), deserializedResult.getFrames().get(0).getDeltaTime(), 1e-5);
        assertEquals(frame.getTotalTime(), deserializedResult.getFrames().get(0).getTotalTime(), 1e-5);
    }

}
