/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.dao;

import org.junit.Before;
import org.junit.Test;
import server.restful.model.ScenarioModel;

import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

public class ScenarioDAOTest {
    @Before
    public void setUp(){
        Connection c = BaseDAO.getConnection();
        try {
            c.createStatement().executeUpdate("DELETE FROM scenario");
            c.commit();
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCRUD() {
        int id = ScenarioDAO.create("sample.sim", "/path/to/sample.sim");
        ScenarioModel scenario = ScenarioDAO.getByID(id);
        assertEquals("sample.sim", scenario.getName());
        assertEquals("/path/to/sample.sim", scenario.getPath());

        // create 3 new scenarios in these test
        ScenarioDAO.create("sample1.sim", "/path/to/sample.sim");
        ScenarioDAO.create("sample2.sim", "/path/to/sample.sim");
        assertEquals(3, ScenarioDAO.getAll().size());
    }
}
