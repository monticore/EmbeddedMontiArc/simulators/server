/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.dao;

import org.junit.Before;
import org.junit.Test;
import server.restful.model.MapModel;

import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

public class MapDAOTest {
    @Before
    public void setUp(){
        Connection c = BaseDAO.getConnection();
        try {
            c.createStatement().executeUpdate("DELETE FROM map");
            c.commit();
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCRUD() {
        int mapID = MapDAO.create("map.osm", "/path/to/map.osm");
        MapModel map = MapDAO.getByID(mapID);
        assertEquals("map.osm", map.getName());
        assertEquals("/path/to/map.osm", map.getPath());

        map = MapDAO.getByName("map.osm");
        assertEquals(mapID, map.getId());

        // create a map with a name that already exist
        mapID = MapDAO.create("map.osm", "/new/path/to/map.osm");
        map = MapDAO.getByName("map.osm");
        assertEquals(mapID, map.getId());
        assertEquals("/new/path/to/map.osm", map.getPath());

        // create 3 new maps in these test
        MapDAO.create("map1.osm", "/new/path/to/map.osm");
        MapDAO.create("map2.osm", "/new/path/to/map.osm");
        assertEquals(3, MapDAO.getAll().size());
    }
}
