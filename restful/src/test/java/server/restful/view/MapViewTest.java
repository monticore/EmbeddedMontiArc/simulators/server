/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.view;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


public class MapViewTest extends AbstractViewTest{
    @Test
    public void testMap() throws Exception{
        MockMultipartFile file = new MockMultipartFile("file", "map.osm", null, "content".getBytes());
        RequestBuilder builder = MockMvcRequestBuilders.multipart("/map").file(file);
        MvcResult result = null;
        result = mvc.perform(builder).andReturn();
        Assert.assertEquals(200, result.getResponse().getStatus());

        builder = MockMvcRequestBuilders.get("/map/-1");
        result = mvc.perform(builder).andReturn();
        Assert.assertEquals(404, result.getResponse().getStatus());

        builder = MockMvcRequestBuilders.get("/map/1");
        result = mvc.perform(builder).andReturn();
        Assert.assertEquals(200, result.getResponse().getStatus());
        Assert.assertTrue(result.getResponse().getContentAsString().length() > 10);


        builder = MockMvcRequestBuilders.get("/map");
        result = mvc.perform(builder).andReturn();
        Assert.assertEquals(200, result.getResponse().getStatus());
        Assert.assertTrue(result.getResponse().getContentAsString().length() > 20);
    }
}
