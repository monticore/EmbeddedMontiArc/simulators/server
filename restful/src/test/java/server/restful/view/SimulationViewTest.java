/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.view;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


public class SimulationViewTest extends AbstractViewTest{
    @Test
    public void testSimulationCreateAndGetAll() throws Exception{
        RequestBuilder builder = MockMvcRequestBuilders.post("/simulation")
                .param("scenario_id", "1")
                .param("num_sectors", "1");
        MvcResult result = null;
        result = mvc.perform(builder).andReturn();
        Assert.assertEquals(200, result.getResponse().getStatus());

        builder = MockMvcRequestBuilders.get("/simulation");
        result = mvc.perform(builder).andReturn();
        Assert.assertEquals(200, result.getResponse().getStatus());
        Assert.assertTrue(result.getResponse().getContentAsString().length() > 10);
    }
}
