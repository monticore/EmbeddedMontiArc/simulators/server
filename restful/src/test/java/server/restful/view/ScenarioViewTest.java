/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.view;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


public class ScenarioViewTest extends AbstractViewTest{
    @Test
    public void testScenario() throws Exception{
        MockMultipartFile file = new MockMultipartFile("file", "scenario.sim", null, "content".getBytes());
        RequestBuilder builder = MockMvcRequestBuilders.multipart("/scenario").file(file);
        MvcResult result = null;
        result = mvc.perform(builder).andReturn();
        Assert.assertEquals(200, result.getResponse().getStatus());

        builder = MockMvcRequestBuilders.get("/scenario");
        result = mvc.perform(builder).andReturn();
        Assert.assertEquals(200, result.getResponse().getStatus());
        Assert.assertTrue(result.getResponse().getContentAsString().length() > 10);
    }
}
