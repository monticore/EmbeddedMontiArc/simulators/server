/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.graph;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;
import org.jgrapht.graph.DefaultUndirectedGraph;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;
import server.restful.graph.structures.OverlayEdge;
import server.restful.util.Util;

import java.io.File;
import java.io.FileNotFoundException;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class OverlayUtilTest {

    @Before
    public void skipOnWindows() {
        // TODO: may work on windows, if you can get metis to work
        //Assume.assumeFalse(Util.isWindows());
    }

    @Test
    public void createOverlayMap() throws Exception {
        String basePath = getClass().getResource("/").getPath();
        String mapPath = basePath + "/straight.osm";
        int numSectors = 3;

        new File(basePath + "straight.part.3").delete();
        new File(basePath + "straight.part.3-0.osm").delete();
        new File(basePath + "straight.part.3-1.osm").delete();
        new File(basePath + "straight.part.3.overlay.dot").delete();

        MetisGraph graph = new MetisGraph(mapPath, numSectors);
        graph.sectorize();
        OverlayUtil.createOverlayMap(graph.getOriginalGraph(), graph.getOsmIdSectorMapping(), basePath + "/straight.part.3.overlay.dot");

        // check file existence
        assertTrue(new File(basePath + "straight.part.3").exists());
        assertTrue(new File(basePath + "straight.part.3-0.osm").exists());
        assertTrue(new File(basePath + "straight.part.3-1.osm").exists());
        assertTrue(new File(basePath + "straight.part.3.overlay.dot").exists());
    }

    @Test
    public void readOverlay() throws FileNotFoundException {
        DefaultUndirectedGraph<Vertex, OverlayEdge> graph = OverlayUtil.readOverlay(getClass().getResource("/overlay.dot").getPath());
        assertEquals(14, graph.vertexSet().size());
        assertEquals(27, graph.edgeSet().size());

        // check if vertices has bee deserialized correctly
        for(Vertex v: graph.vertexSet()){
            assertTrue(v.getOsmId() > 0);
            assertTrue(v.getId() > 0);
            assertNotNull(v.getPosition());
        }

        // check if edges has bee deserialized correctly
        for(OverlayEdge edge: graph.edgeSet()){
            assertTrue(edge.getWayPoints().size() >= 2);
            assertTrue(edge.getSectorIdxs().size() >= 2);
        }

    }

    @Test
    public void createOverlayMap1() throws FileNotFoundException {
        String basePath = getClass().getResource("/").getPath();
        String mapPath = basePath + "Aachen.osm";
        int numSectors = 2;
        MetisGraph graph = new MetisGraph(mapPath, numSectors);
        graph.sectorize();
        OverlayUtil.createOverlayMap(graph.getOriginalGraph(), graph.getOsmIdSectorMapping(), basePath + "Aachen_2_test.overlay.dot");
    }
}
