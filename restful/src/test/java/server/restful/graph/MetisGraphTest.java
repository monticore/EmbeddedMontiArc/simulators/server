/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.graph;

import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;
import server.restful.util.Util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static junit.framework.TestCase.assertTrue;

public class MetisGraphTest {
    String mapPath = getClass().getResource("/Aachen.osm").getPath();
    int numSectors = 6;

    @Before
    public void skipOnWindows() {
        // TODO: may work on windows, if you can get metis to work
        //Assume.assumeFalse(Util.isWindows());
    }

    @Before
    public void setUp() throws Exception {
        MetisGraph graph = new MetisGraph(mapPath, numSectors);
        new File(graph.getSectorPath()).delete();
        for(int i=0; i<numSectors; i++){
            new File(graph.getMapPathBySectorIdx(i)).delete();
        }
    }


    @Test
    public void sectorize() throws Exception {
        MetisGraph mg = new MetisGraph(mapPath, numSectors);
        mg.sectorize();
        List<Long> expectedNodes = mg.getOriginalGraph().getVertices().stream().map(x -> x.getOsmId()).collect(Collectors.toList());

        // check existence of outputs
        assertTrue(new File(mg.getSectorPath()).exists());
        Set<Long> actualNodes = new HashSet<>();
        for(int i=0; i<numSectors; i++){
            assertTrue(new File(mg.getMapPathBySectorIdx(i)).exists());
            String path = mg.getMapPathBySectorIdx(i);
            actualNodes.addAll(
                    new MetisGraph(path, 1).getOriginalGraph().getVertices()
                    .stream()
                    .map(x -> x.getOsmId())
                    .collect(Collectors.toList())
            );
        }

        for (Long v : expectedNodes) {
            //System.out.println("checking " + v);
            assertTrue(actualNodes.contains(v));
        }
    }

    @Test
    public void formOsmFileFromVertices() throws FileNotFoundException {
        String mapPath = getClass().getResource("/Aachen.osm").getPath();
        int numSectors = 6;
        MetisGraph graph = new MetisGraph(mapPath, numSectors);
        graph.sectorize();

        // first sectorized the map and then try to parse all sector maps using the simulation project parser2D
        for (int i = 0; i < numSectors; i++) {
            new MetisGraph(graph.getMapPathBySectorIdx(i), -1).init();
        }
    }
}
