#
# (c) https://github.com/MontiCore/monticore
#
# The license generally applicable for this project
# can be found under https://github.com/MontiCore/monticore.
#

from peewee import *

db = SqliteDatabase("app.db")


class Map(Model):
    name = CharField(null=False)
    path = CharField(null=False)

    class Meta:
        database = db


class Simulation(Model):
    num_sectors = IntegerField(null=True)
    scenario_id = IntegerField(null=True)
    result_path = CharField(null=True)

    class Meta:
        database = db


class Scenario(Model):
    name = CharField(null=False)
    path = CharField(null=False)

    class Meta:
        database = db

class Car(Model):
    name = CharField(null=False)
    path = CharField(null=False)

    class Meta:
        database = db


Map.drop_table(safe=True)
Map.create_table(safe=True)

Simulation.drop_table(safe=True)
Simulation.create_table(safe=True)

Scenario.drop_table(safe=True)
Scenario.create_table(safe=True)

Car.drop_table(safe=True)
Car.create_table(safe=True)
