/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.graph;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.structures.Edge;
import org.apache.commons.io.FileUtils;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;
import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.Graphs;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultUndirectedGraph;
import org.jgrapht.io.*;
import org.jgrapht.util.SupplierUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import server.restful.graph.structures.OverlayEdge;

import java.io.*;
import java.nio.charset.Charset;
import java.util.*;

//import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.structures.Graph;

/**
 * OverlayUtil provide methods to create, serialize and de-serialize an overlay graph.
 */
public class OverlayUtil {
    private static Logger logger = LoggerFactory.getLogger(OverlayUtil.class);

    /**
     * Create an overlay map of the original one, the map must be already sectorized and the result can be read
     * from sectorResultPath.
     *
     * @param baseMap where the overlay map should be based on
     * @param osmIdToSectorIdxMapping the partition result of the baseMap
     * @param output path to store the overlay map
     */
    public static void createOverlayMap(
            de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.structures.Graph baseMap,
            Map<Long, Integer> osmIdToSectorIdxMapping, String output) {
        logger.info("creating overlay map...");

        // find cut edges and cut vertices
        // if 2 nodes of an edge are in different sector(partition), the sector is cut edge, nodes are cut vertices
        List<Vertex[]> cutEdges = new ArrayList<>();
        Set<Vertex> cutVertices = new HashSet<>();
        for(Vertex vertex: baseMap.getVertices()){
            // iterate over all neighbours of `vertex`
            for(Vertex neighbour: baseMap.getEdges().get(vertex).keySet()){
                if(vertex.equals(neighbour)){
                    continue;
                }
                if(!osmIdToSectorIdxMapping.get(vertex.getOsmId()).equals(osmIdToSectorIdxMapping.get(neighbour.getOsmId()))){
                    cutEdges.add(new Vertex[]{vertex, neighbour});
                    cutVertices.add(vertex);
                    cutVertices.add(neighbour);
                }
            }
        }

        // group cut vertices by sector idx
        Map<Integer, Set<Vertex>> vertexGroups = new HashMap<>();
        for (Vertex vertex: cutVertices){
            int sector = osmIdToSectorIdxMapping.get(vertex.getOsmId());
            if (!vertexGroups.containsKey(sector)){
                vertexGroups.put(sector, new HashSet<>());
            }
            vertexGroups.get(sector).add(vertex);
        }

        // create an sub-overlay graph from each cut vertices group. i.e from all vertices
        // that locates in the same sector
        List<DefaultUndirectedGraph<Vertex, OverlayEdge>> subGraphs = new ArrayList<>();
        DefaultUndirectedGraph<Vertex, Edge> jgrapht = convertToJGraphT(baseMap);
        for(Integer sectorIdx: vertexGroups.keySet()){
            subGraphs.add(createSubGraph(jgrapht, vertexGroups.get(sectorIdx), osmIdToSectorIdxMapping));
        }

        // join all sub-overlays into a same graph
        DefaultUndirectedGraph<Vertex, OverlayEdge> overlay = subGraphs.get(0);
        for (int i = 1; i < subGraphs.size(); i++) {
            Graphs.addGraph(overlay, subGraphs.get(i));
        }

        // connect all sub-overlays with cut edges
        for(Vertex[] edge: cutEdges){
            Edge e = baseMap.getEdge(edge[0], edge[1]);
            OverlayEdge oe = new OverlayEdge(e.getWeight(), e.getCapacity());
            oe.addWayPoint(edge[0].getOsmId());
            oe.addWayPoint(edge[1].getOsmId());
            oe.addSectorIdx(osmIdToSectorIdxMapping.get(edge[0].getOsmId()));
            oe.addSectorIdx(osmIdToSectorIdxMapping.get(edge[1].getOsmId()));
            overlay.addEdge(edge[0], edge[1], oe);
        }

        // serialize the generated overlay graph
        writeOverlayMap(output, overlay);
    }


    /**
     * Convert a montisim-controller.structures.Graph into a JGrapht graph
     *
     * @param graph
     * @return
     */
    public static DefaultUndirectedGraph<Vertex, Edge> convertToJGraphT(
            de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.structures.Graph graph) {
        DefaultUndirectedGraph<Vertex, Edge> result = new DefaultUndirectedGraph<>(
                SupplierUtil.createSupplier(Vertex.class),
                SupplierUtil.createSupplier(OverlayEdge.class),
                true
        );
        for (Vertex v: graph.getVertices()){
            // add the vertex if not already exist
            if (!result.containsVertex(v)){
                result.addVertex(v);
            }

            // iterate over all neighbour vertices
            Map<Vertex, Edge> neighbours = graph.getEdges().get(v);
            for(Vertex neighbour: neighbours.keySet()){
                // add the neighbour if not already exist
                if (!result.containsVertex(neighbour)){
                    result.addVertex(neighbour);
                }
                // add the edge if not already exist
                if(!result.containsEdge(neighbours.get(neighbour))){
                    result.addEdge(v, neighbour, neighbours.get(neighbour));
                    result.setEdgeWeight(v, neighbour, neighbours.get(neighbour).getWeight());
                }
            }
        }
        return result;
    }

    /**
     * Create an sub graph that removed all vertices that not contains in the `verticesToKeep` set.
     * The edges of the original graph are those edge, which connect every pair of vertices from `verticesToKeep`.
     *
     * The edges also contains information of to navigate from one vertex to the other, which osmNode/sector
     * will the vehicle drive by.
     *
     * The result graph is actually an overlay graph of the original one, which contains vertices from `verticesToKeep`
     *
     * @param original
     * @param verticesToKeep
     * @param osmIDtoSectorIdxMapping the partition result of the original graph
     * @return
     */
    private static DefaultUndirectedGraph<Vertex, OverlayEdge> createSubGraph(Graph original, Set<Vertex> verticesToKeep, Map<Long, Integer> osmIDtoSectorIdxMapping){
        DefaultUndirectedGraph<Vertex, OverlayEdge> result = new DefaultUndirectedGraph<>(OverlayEdge.class);
        for(Vertex vertex: verticesToKeep){
            result.addVertex(vertex);
        }

        DijkstraShortestPath<Vertex, Edge> dijkstra = new DijkstraShortestPath<Vertex, Edge>(original);
        for(Vertex v: result.vertexSet()){
            for(Vertex neighbour: result.vertexSet()){
                if (v.equals(neighbour)){
                    continue;
                }

                // calc shortest path
                GraphPath<Vertex, Edge> path = dijkstra.getPath(v, neighbour);
                if (path == null){
                    continue;
                }

                OverlayEdge edge = new OverlayEdge(path.getWeight(), 0.0);
                for(Vertex wayPoint: path.getVertexList()){
                    edge.addWayPoint(wayPoint.getOsmId());
                    edge.addSectorIdx(osmIDtoSectorIdxMapping.get(wayPoint.getOsmId()));
                }

                result.addEdge(v, neighbour, edge);
            }
        }
        return result;
    }

    /**
     * Write an overlay graph to disk
     *
     * @param output
     * @param overlay
     */
    private static void writeOverlayMap(String output, DefaultUndirectedGraph<Vertex, OverlayEdge> overlay){
        ComponentNameProvider<Vertex> vertexIDProvider = vertex -> vertex.getOsmId().toString();
        // defines what additional information should be stored
        ComponentAttributeProvider<Vertex> vertexAttrProvider = vertex -> {
            Map<String, Attribute> attrs = new HashMap<>();
            attrs.put("position", new Attribute() {
                @Override
                public String getValue() {
                    return positionToAttribute(vertex.getPosition());
                }

                @Override
                public AttributeType getType() {
                    return AttributeType.STRING;
                }
            });
            return attrs;
        };

        // defines what additional information should be stored
        ComponentAttributeProvider<OverlayEdge> edgeAttrProvider = overlayEdge -> {
            Map<String, Attribute> attrs = new HashMap<>();
            attrs.put("wayPoints", new Attribute() {
                @Override
                public String getValue() {
                    return String.join(",", overlayEdge.getWayPoints());
                }

                @Override
                public AttributeType getType() {
                    return AttributeType.STRING;
                }
            });
            attrs.put("sectorIdxs", new Attribute() {
                @Override
                public String getValue() {
                    return String.join(",", overlayEdge.getSectorIdxs());
                }

                @Override
                public AttributeType getType() {
                    return AttributeType.STRING;
                }
            });
            attrs.put("weight", new Attribute() {
                @Override
                public String getValue() {
                    return String.valueOf(overlayEdge.getWeight());
                }

                @Override
                public AttributeType getType() {
                    return AttributeType.STRING;
                }
            });
            return attrs;
        };

        GraphExporter<Vertex, OverlayEdge> exporter = new DOTExporter<>(
                vertexIDProvider, null, null, vertexAttrProvider, edgeAttrProvider);
        Writer writer = new StringWriter();
        try {
            exporter.exportGraph(overlay, writer);
            FileUtils.writeStringToFile(new File(output), writer.toString(), Charset.defaultCharset());
        } catch (ExportException | IOException e) {
            logger.warn(e.getMessage(), e);
        }

    }

    /**
     * Read an overlay map from disk
     *
     * @param path
     * @return
     * @throws FileNotFoundException
     */
    public static DefaultUndirectedGraph<Vertex, OverlayEdge> readOverlay(String path) throws FileNotFoundException {
        // how to parse the vertex with additional information
        VertexProvider<Vertex> vertexProvider = (s, map) -> {
            long osmID = Long.parseLong(s);
            return new Vertex(osmID, osmID, attributeToPosition(map.get("position").getValue()), 0.0);
        };
        // how to parse the edge with additional information
        EdgeProvider<Vertex, OverlayEdge> edgeProvider = (v1, v2, s, map) -> {
            OverlayEdge edge = new OverlayEdge(Double.parseDouble(map.get("weight").getValue()), 0.0);
            for(String wayPointOsmId: map.get("wayPoints").getValue().split(",")){
                edge.addWayPoint(Long.parseLong(wayPointOsmId));
            }
            for(String sectorIdx: map.get("sectorIdxs").getValue().split(",")){
                edge.addSectorIdx(Integer.parseInt(sectorIdx));
            }
            return edge;
        };

        // read and parse the file
        DOTImporter importer = new DOTImporter(vertexProvider, edgeProvider);
        DefaultUndirectedGraph<Vertex, OverlayEdge> graph = new DefaultUndirectedGraph<Vertex, OverlayEdge>(
                SupplierUtil.createSupplier(Vertex.class),
                SupplierUtil.createSupplier(OverlayEdge.class),
                true);
        try {
            importer.importGraph(graph, new FileInputStream(new File(path)));
            for(OverlayEdge e: graph.edgeSet()){
                graph.setEdgeWeight(e, e.getWeight());
            }
            return graph;
        } catch (ImportException e) {
            e.printStackTrace();
        }
        return null;
    };

    private static String positionToAttribute(RealVector pos){
        double[] position = pos.toArray();
        String[] coord = new String[position.length];
        for (int i = 0; i < position.length; i++) {
            coord[i] = String.valueOf(position[i]);
        }
        return String.join(",", coord);
    }

    private static RealVector attributeToPosition(String label){
        RealVector position = new ArrayRealVector();
        String[] coords = label.split(",");
        for (int i = 0; i < coords.length; i++) {
            position = position.append(Double.parseDouble(coords[i]));
        }
        return position;
    }
}
