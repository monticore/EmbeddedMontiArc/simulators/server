/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.graph;

import ch.qos.logback.classic.Level;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.structures.Graph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import server.restful.util.Util;
import simulation.environment.World;
import simulation.environment.WorldModel;
import simulation.environment.osm.ParserSettings;
import simulation.environment.weather.WeatherSettings;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * MetisGraph provide a method to sectorize a openstreetmap file into multiple smaller
 * ones using metis graph partition algorithm.
 *
 * By calling sectorize, it will generate:
 *  1. A graph file in metis format represents the original openstreetmap file.
 *  2. a\A file that contains partition result of metis. This is used to create an overlay graph of
 *  the original openstreetmap data.
 *  3. Several openstreetmaps split using the partition result. They can be transferred to sub-simulators
 *  to execute simulation tasks.
*  in the same directory as the mapPath specified in the constructor.
 *
 */
public class MetisGraph {
    private Logger logger = LoggerFactory.getLogger(MetisGraph.class);
    private String mapPath; // where the openstreetmap can be read from
    private String sectorPath; // where the result from metis should be stored
    private String metisGraphPath; // where to store the originalGraph that converted into metis' format
    private int numSectors;


    private Graph originalGraph; // stores graph representation of the original map
    private Map<Vertex, Integer> vertexIdxMapping; // a mapping from vertex to idx(start from 0)
    private Map<Integer, Vertex> idxVertexMapping; // a mapping from idx(starts from 0) to vertex
    private Map<Long, Integer> osmIdSectorMapping; // a mapping from vertex to sectorIdx it belongs to

    private boolean initialized = false;

    // simulation.environment.osm.Parser2D.java@198
    final List<String> notWay = Arrays.asList("footway", "bridleway", "steps", "path", "cycleway", "pedestrian", "track", "elevator");

    /**
     * @param mapPath abs path to the map
     */
    public MetisGraph(String mapPath, int numSectors) throws FileNotFoundException {
        // set root logger level to info
        ((ch.qos.logback.classic.Logger) logger).getLoggerContext().getLoggerList().get(0).setLevel(Level.INFO);

        this.mapPath = mapPath;
        this.numSectors = numSectors;

        this.metisGraphPath = mapPath.replace(".osm", "");

        if (Util.isWindows()) {
            this.metisGraphPath = this.metisGraphPath.substring(1);
        }

        this.sectorPath = String.format("%s.part.%d", this.metisGraphPath, numSectors);

        vertexIdxMapping = new HashMap<>();
        idxVertexMapping = new HashMap<>();
        osmIdSectorMapping = new HashMap<>();

        if(!new File(mapPath).exists()){
            throw new FileNotFoundException(mapPath + " not exist");
        }
    }

    public void init(){
        if (initialized){
            return;
        }

        logger.info("initializing metis graph...");
        try {
            this.originalGraph = getGraphFromMap();
        } catch (Exception e) {
            logger.warn(e.getMessage(), e);
        }
        List<Vertex> vertices = originalGraph.getVertices();
        vertices.sort((v1, v2) -> {
            if (v1.getOsmId() > v2.getOsmId()){
                return -1;
            }else if (v1.getOsmId() < v2.getOsmId()){
                return 1;
            }
            return 0;
        });
        for(int i = 0; i < vertices.size(); i++){
            Vertex v = vertices.get(i);

            // The idx in the mapping starts from 1. Afforded by metis.
            vertexIdxMapping.put(v, i+1);
            idxVertexMapping.put(i+1, v);
        }

        initialized = true;
    }

    /**
     * Split the original openstreetmap into `numSectors` smaller .osm files.
     *
     * The results are stored in the same directory as mapPath.
     */
    public void sectorize(){
        init();
        try {
            convertMapToMetisGraph();
        } catch (IOException e) {
            logger.warn(e.getMessage(), e);
        }

        // call metis to partition the originalGraph
        callMetis();

        // read the result into osmIdSectorMapping
        try {
            readMetisResult();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        splitOsmFile();
    }

    private Graph getGraphFromMap() throws Exception {
        logger.info("reading OpenStreetMap from {}...", mapPath);
        World world;
        world = WorldModel.init(
                new ParserSettings(new FileInputStream(mapPath), ParserSettings.ZCoordinates.ALLZERO),
                new WeatherSettings()
        );
        Graph g = new Graph(world.getControllerMap().getAdjacencies(), false);
        logger.info("parsed map {}", mapPath);
        return g;
    }

    /**
     * Convert the graph parsed from .osm file into the format that accepted by metis.
     * The converted Graph is stored under specified path.
     *
     * For more details about the format:
     * http://glaros.dtc.umn.edu/gkhome/fetch/sw/metis/manual.pdf
     *
     * The result will be stored under `metisGraphPath`
     *
     * @throws IOException
     */
    private void convertMapToMetisGraph() throws IOException {
        Map<String, Object> seenEdges = new HashMap<>();
        List<String> lines = new ArrayList<>();

        // iterate over all vertices to generate a graph accepted by metis software
        for (Vertex v: originalGraph.getVertices()) {
            List<String> adjVertices = new ArrayList<>();

            for (Vertex adj : originalGraph.getEdges().get(v).keySet()) {
                String edge;
                // form a representation for the edge
                if (vertexIdxMapping.get(v) < vertexIdxMapping.get(adj)){
                    edge = v.getOsmId() + "-" + adj.getOsmId();
                }else{
                    edge = adj.getOsmId() + "-" + v.getOsmId();
                }

                if (!seenEdges.containsKey(edge)){
                    seenEdges.put(edge, null);
                }
                adjVertices.add(vertexIdxMapping.get(adj).toString());
            }

            lines.add(String.join(" ", adjVertices));
        }

        lines.add(0, String.format("%d %d 000",
                originalGraph.getVertices().size(),
                seenEdges.keySet().size())
        );

        Files.write(Paths.get(metisGraphPath), lines, Charset.defaultCharset());
    }

    /**
     * Call metis to partition the graph
     */
    public void callMetis(){
        logger.info("calling metis to partition originalGraph {} with {} partitions...", metisGraphPath, numSectors);
        String[] cmd = new String[]{"gpmetis", "-seed=1", metisGraphPath, String.valueOf(numSectors)};
        try {
            ProcessBuilder pb = new ProcessBuilder(cmd);
            pb.redirectErrorStream(true);
            Process proc = pb.start();

            // print process outputs
            BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            List<String> lines = new ArrayList<>();
            String line;
            while ((line = reader.readLine()) != null) {
                lines.add(line);
                logger.debug(line);
            } while (line != null);
        } catch (IOException e) {
            logger.warn(e.getMessage(), e);
        }
    }

    /**
     * Read metis result into osmIdSectorMapping
     *
     * @throws FileNotFoundException
     */
    private void readMetisResult() throws FileNotFoundException {
        if(!initialized){
            init();
        }
        BufferedReader reader;
        reader = new BufferedReader(new FileReader(sectorPath));

        String line = null;
        try {
            line = reader.readLine();

            // the value of i-th line contains sector idx for the i-th vertex
            // line number start from 1
            int lineNum = 1;

            while (line != null){
                int sectorIdx = Integer.valueOf(line);
                Vertex v = idxVertexMapping.get(lineNum);
                osmIdSectorMapping.put(v.getOsmId(), sectorIdx);
                line = reader.readLine();
                lineNum += 1;
            }
            reader.close();
        } catch (IOException e) {
            logger.warn(e.getMessage(), e);
        }
    }

    /**
     * Split the original openstreetmap into several smaller .osm files using osmIdSectorMapping
     */
    private void splitOsmFile(){
        // vertices group by their sector idx
        Map<Integer, Set<Long>> sectors = new HashMap<>();
        for (long osmId: osmIdSectorMapping.keySet()){
            int sector = osmIdSectorMapping.get(osmId);
            if (!sectors.containsKey(sector)){
                sectors.put(sector, new HashSet<>());
            }
            sectors.get(sector).add(osmId);
        }

        int i = 0;
        for(Set<Long> vertices: sectors.values()){

            // the split map should also contains cut edges that connect itself to neighbouring sectors
            Set<Long> cutVertices = new HashSet<>();
            for(Long osmId: vertices){
                // iterate over all vertices in this sector to find all cut edges and the other vertex of the cut edge(cut vertex)
                Vertex s = originalGraph.getVertex(osmId);
                for(Vertex t: originalGraph.getEdges().get(s).keySet()){
                    if(!osmIdSectorMapping.get(s.getOsmId()).equals(osmIdSectorMapping.get(t.getOsmId()))){
                        cutVertices.add(t.getOsmId());
                    }
                }
            }
            vertices.addAll(cutVertices);

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware(true);
            DocumentBuilder builder = null;
            Document doc;
            try {
                builder = factory.newDocumentBuilder();
                doc = builder.parse(mapPath);
            } catch (ParserConfigurationException | SAXException | IOException e) {
                e.printStackTrace();
                return;
            }

            cleanUpDOM(doc);

            // output has the form:
            // /originalmap.part.x-i.osm
            String output = String.format("%s-%d.osm", sectorPath, i++);
            formOsmFileFromVertices(copyDOM(doc), vertices, output);
        }
    }

    private void cleanUpDOM(Document dom){
        logger.info("cleaning up ways...");
        List<Node> nodesToRemove = new ArrayList<>();

        NodeList ways = dom.getElementsByTagName("way");
        for (int i=0; i<ways.getLength(); i++) {
            Node way = ways.item(i);

            // remove way if it is not drivable
            if (!isWay(way)) {
                nodesToRemove.add(way);
                logger.debug("remove way {}, it is not drivable", way.getAttributes().getNamedItem("id").getNodeValue());
            }
        }

        removeNodesFromDoc(nodesToRemove);
    }

    private Document copyDOM(Document source){
        logger.info("copying DOM object...");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        Document result = null;
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            result = builder.newDocument();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        Node node = result.importNode(source.getDocumentElement(), true);
        result.appendChild(node);
        return result;
    }

    /**
     * Create a new openstreetmap file from the original one(read from mapPath), where all nodes
     * that are not in `shouldKeep` are removed.
     *
     * @param output  path where the new .osm file is stored
     * @param shouldKeep osmNodes that should exist in the new map file
     */
    void formOsmFileFromVertices(Document dom, Set<Long> shouldKeep, String output){
        try {
            // only keep information of nodes in `shouldKeep`
            // other nodes will be removed
            removeNodes(dom, shouldKeep);
            removeWays(dom, shouldKeep);
            removeRelations(dom, shouldKeep);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(dom);
            File file = new File(output);
            file.delete();
            file.createNewFile();
            StreamResult result = new StreamResult(file);
            transformer.transform(source, result);
        } catch (TransformerException | IOException e) {
            logger.warn(e.getMessage(), e);
        }
    }

    /**
     * Remove <node id=[osmID].../>
     *
     * @param dom
     * @param nodesToKeep
     */
    private void removeNodes(Document dom, Set<Long> nodesToKeep){
        logger.info("remove nodes...");
        List<Node> nodesToRemove = new ArrayList<>();

        NodeList nodes = dom.getElementsByTagName("node");
        for (int i=0; i<nodes.getLength(); i++){
            Node node = nodes.item(i);
            long nodeID = Long.valueOf(node.getAttributes().getNamedItem("id").getNodeValue());
            if (!nodesToKeep.contains(nodeID)) {
                nodesToRemove.add(node);
                logger.debug("remove node {}", nodeID);
            }
        }

        removeNodesFromDoc(nodesToRemove);
    }

    /**
     * Remove <nd ref=[osmID].../> from <way .../>
     *
     * @param dom
     * @param nodesToKeep
     */
    private void removeWays(Document dom, Set<Long> nodesToKeep){
        logger.info("remove ways...");
        List<Node> nodesToRemove = new ArrayList<>();

        NodeList ways = dom.getElementsByTagName("way");
        for (int i=0; i<ways.getLength(); i++){
            Node way = ways.item(i);

            NodeList wayPoints = way.getChildNodes();
            boolean isAllWayPointsRemoved = true;

            for (int j=0; j<wayPoints.getLength(); j++){
                Node wayPoint = wayPoints.item(j);
                if (!wayPoint.getNodeName().equals("nd")){
                    continue;
                }

                long nodeID = Long.valueOf(wayPoint.getAttributes().getNamedItem("ref").getNodeValue());
                if (!nodesToKeep.contains(nodeID)) {
                    nodesToRemove.add(wayPoint);
                    logger.debug("remove node {} from way {}", nodeID, way.getAttributes().getNamedItem("id").getNodeValue());
                } else {
                    isAllWayPointsRemoved = false;
                }
            }

            if (isAllWayPointsRemoved){
                logger.debug("way {} has no node, remove it from map", way.getAttributes().getNamedItem("id").getNodeValue());
                nodesToRemove.add(way);
            }
        }

        removeNodesFromDoc(nodesToRemove);
    }

    private boolean isWay(Node way){
        NodeList wayPoints = way.getChildNodes();
        boolean isHighway = false; // "highway"s are all walkable, drivable ways in osm map
        for (int i = 0; i < wayPoints.getLength(); i++) {
            Node wayPoint = wayPoints.item(i);
            if (wayPoint.getNodeName().equals("tag") && wayPoint.getAttributes().getNamedItem("k").getNodeValue().equals("highway")){
                isHighway = true;
                String wayType = wayPoint.getAttributes().getNamedItem("v").getNodeValue();
                if (notWay.contains(wayType)){
                    return false;
                }
            }
        }

        return isHighway;
    }

    /**
     * Remove <member ref=[osmID].../> from <relation .../>
     *
     * @param dom
     * @param nodesToKeep
     */
    private void removeRelations(Document dom, Set<Long> nodesToKeep){
        logger.info("remove relations...");
        List<Node> nodesToRemove = new ArrayList<>();

        NodeList relations = dom.getElementsByTagName("relation");
        for (int i=0; i<relations.getLength(); i++){
            Node relation = relations.item(i);
            NodeList members = relation.getChildNodes();
            boolean isAllMembersRemoved = true;

            for (int j=0; j<members.getLength(); j++){
                Node member = members.item(j);
                if (!member.getNodeName().equals("member")){
                    continue;
                }
                long nodeID = Long.valueOf(member.getAttributes().getNamedItem("ref").getNodeValue());
                if (!nodesToKeep.contains(nodeID)) {
                    nodesToRemove.add(member);
                    logger.debug("remove node {} from relation {}", nodeID, relation.getAttributes().getNamedItem("id").getNodeValue());
                } else {
                    isAllMembersRemoved = false;
                }
            }

            if (isAllMembersRemoved){
                logger.debug("relation {} has no node, remove it from map", relation.getAttributes().getNamedItem("id").getNodeValue());
                nodesToRemove.add(relation);
            }
        }

        removeNodesFromDoc(nodesToRemove);
    }

    private void removeNodesFromDoc(List<Node> nodes){
        for (Node node: nodes){
            node.getParentNode().removeChild(node);
        }
    }


    public String getSectorPath() {
        return sectorPath;
    }

    public String getMapPathBySectorIdx(int sectorIdx) {
        return getSectorPath() + "-" + sectorIdx + ".osm";
    }

    public Map<Long, Integer> getOsmIdSectorMapping() {
        // if not in memory, then load if from file
        if (osmIdSectorMapping.values().size() == 0){
            try {
                readMetisResult();
            } catch (FileNotFoundException e) {
                logger.warn(e.getMessage(), e);
                return null;
            }
        }
        return osmIdSectorMapping;
    }

    public Graph getOriginalGraph() {
        if (!initialized){
            init();
        }
        return originalGraph;
    }
}
