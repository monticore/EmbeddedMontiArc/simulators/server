/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.graph.structures;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.structures.Edge;

import java.util.ArrayList;
import java.util.List;


public class OverlayEdge extends Edge {
    List<String> wayPoints;
    List<String> sectorIdxs;

    public OverlayEdge(double weight, Double capacity) {
        super(weight, capacity);
        this.wayPoints = new ArrayList<>();
        this.sectorIdxs = new ArrayList<>();
    }

    public void addWayPoint(long osmID){
        this.wayPoints.add(String.valueOf(osmID));
    }

    public void addSectorIdx(int sectorIdx){
        this.sectorIdxs.add(String.valueOf(sectorIdx));
    }

    public List<String> getWayPoints() {
        return wayPoints;
    }

    public List<Long> getWayPointsAsLong() {
        List<Long> result = new ArrayList<>();
        wayPoints.stream().mapToLong(x -> Long.parseLong(x)).forEach(result::add);
        return result;
    }

    public List<String> getSectorIdxs() {
        return sectorIdxs;
    }

    public List<Integer> getSectorIdxsAsInt() {
        List<Integer> result = new ArrayList<>();
        sectorIdxs.stream().mapToInt(x -> Integer.parseInt(x)).forEach(result::add);
        return result;
    }

    public String getSource(){
        return wayPoints.get(0);
    }

    public String getTarget(){
        return wayPoints.get(wayPoints.size() - 1);
    }
}


