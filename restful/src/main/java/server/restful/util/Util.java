/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.util;

import de.monticore.lang.montisim.carlang.CarContainer;
import de.monticore.lang.montisim.carlang.CarLangTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import server.restful.service.MapService;

import java.io.*;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.Properties;

public class Util {
    static Properties properties = null;
    private static Logger logger = LoggerFactory.getLogger(Util.class);

    private static CarContainer defaultCarContainer;

    public static void writeToFile(InputStream inputStream, String path) {
        try {
            OutputStream out;
            int read = 0;
            byte[] bytes = new byte[1024];

            out = new FileOutputStream(new File(path));
            while ((read = inputStream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getWorkDir() {
        // get paths like:
        // file:/app/app.jar!/BOOT-INF/classes!/server/restful/service/
        // as: /app
        String jarPath = MapService.class.getResource("/").getPath()
                .replaceAll("^.*file:", "")
                .replaceAll("jar!.*", "jar");
        if (isWindows() && jarPath.indexOf("/") == 0) {
            jarPath = jarPath.substring(1);
        }
        if (jarPath.contains("test-classes")) {
            return Paths.get(Paths.get(jarPath).getParent().toString(), "test-classes").toString();
        }
        return Paths.get(Paths.get(jarPath).getParent().toString(), "classes").toString();
    }

    public static String getDatabasePath() {
        return "jdbc:sqlite:" + Paths.get(getWorkDir(), "app.db").toString();
    }

    public static Properties getProperties() {
        if (properties != null) {
            return properties;
        }
        properties = new Properties();

        try {
            properties.load(Util.class.getResourceAsStream("/config.properties"));
            return properties;
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }

        return properties;
    }

    public static boolean isWindows(){
        // TODO: we could use System.getProperty("os.name") - https://stackoverflow.com/questions/228477/how-do-i-programmatically-determine-operating-system-in-java
//        return Boolean.valueOf(Util.getProperties().getProperty("windows_dev_mode", "false"));
        String os = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH);
        return os.contains("win");
    }

    public static CarContainer getDefaultCarContainer() {
        if (defaultCarContainer == null) {
            defaultCarContainer = CarLangTool.parse(Paths.get("src/main/resources/default"), "default").get().getCarContainer();
        }
        return defaultCarContainer;
    }
}
