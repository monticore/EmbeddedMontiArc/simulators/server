/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.dao;

import server.restful.model.ScenarioModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ScenarioDAO extends BaseDAO{
    public static int create(String name, String path){
        int scenarioID = -1;
        Connection c = getConnection();
        PreparedStatement stmt;

        try {
            stmt = c.prepareStatement("INSERT INTO scenario (name, path) VALUES (?, ?)");
            stmt.setString(1, name);
            stmt.setString(2, path);
            scenarioID = executeInsert(c, stmt);
            c.commit();
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return scenarioID;
    }


    public static ScenarioModel getByID(int id) {
        ScenarioModel result = null;
        Connection c = getConnection();
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("SELECT name, path FROM scenario where id=(?)");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()){
                result = new ScenarioModel(
                        id,
                        rs.getString("name"),
                        rs.getString("path")
                );
            }

            stmt.close();
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static List<ScenarioModel> getAll(){
        List<ScenarioModel> result = new ArrayList<>();
        Connection c = getConnection();
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("SELECT id, name, path FROM scenario");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()){
                result.add(new ScenarioModel(
                        rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("path")
                ));
            }

            stmt.close();
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
