/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.dao;

import server.restful.model.MapModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MapDAO extends BaseDAO{
    public static int create(String name, String path){
        int mapID = -1;
        Connection c = getConnection();
        PreparedStatement stmt;
        try {
            // insert if not exist otherwise update
            stmt = c.prepareStatement("SELECT * FROM map where name = (?)");
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()){
                mapID = rs.getInt("id");
                stmt = c.prepareStatement("UPDATE map SET path=(?) where name = (?)");
                stmt.setString(1, path);
                stmt.setString(2, name);
                stmt.executeUpdate();
            }else{
                stmt = c.prepareStatement("INSERT INTO map (name, path) VALUES (?, ?)");
                stmt.setString(1, name);
                stmt.setString(2, path);
                mapID = executeInsert(c, stmt);
            }

            c.commit();
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return mapID;
    }

    public static MapModel getByID(int id) {
        MapModel result = null;
        Connection c = getConnection();
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("SELECT name, path FROM map where id=(?)");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()){
                result = new MapModel();
                result.setId(id);
                result.setName(rs.getString("name"));
                result.setPath(rs.getString("path"));
            }

            stmt.close();
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static MapModel getByName(String name) {
        MapModel result = null;
        Connection c = getConnection();
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("SELECT id, name, path FROM map where name=(?) LIMIT 1");
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()){
                result = new MapModel();
                result.setId(rs.getInt("id"));
                result.setName(rs.getString("name"));
                result.setPath(rs.getString("path"));
            }

            stmt.close();
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static List<MapModel> getAll() {
        List<MapModel> result = new ArrayList<>();
        Connection c = getConnection();
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("SELECT id, name FROM map");
            ResultSet rs = stmt.executeQuery();

            MapModel map;
            while (rs.next()){
                map = new MapModel();
                map.setId(rs.getInt("id"));
                map.setName(rs.getString("name"));
                result.add(map);
            }

            stmt.close();
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
