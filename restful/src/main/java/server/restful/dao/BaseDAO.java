/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import server.restful.util.Util;

import java.sql.*;

class BaseDAO {
    static Logger logger = LoggerFactory.getLogger(BaseDAO.class);

    static Connection getConnection() {
        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection(Util.getDatabasePath());
            c.setAutoCommit(false);
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
        return c;
    }

    static int executeInsert(Connection conn, PreparedStatement stmt) throws SQLException {
        stmt.executeUpdate();
        ResultSet rs = stmt.getGeneratedKeys();
        rs.next();
        stmt.close();
        return rs.getInt(1);
    }
}
