/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import server.restful.model.SimulationModel;
import server.restful.model.VehicleModel;
import server.restful.util.Util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SimulationDAO extends BaseDAO{
    /**
     * Create a simulation record in db
     * @param scenarioID the id of scenario this simulation will simulate
     * @param numSection the number of sections(sub-simulators) needed
     * @return the id of created simulation record
     */
    public static int create(int scenarioID, int numSection){
        int simID = -1;
        Connection c = getConnection();
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement(
                    "INSERT INTO simulation (id, scenario_id, num_sectors) VALUES (NULL, ?, ?)");
            stmt.setInt(1, scenarioID);
            stmt.setInt(2, numSection);
            simID = executeInsert(c, stmt);
            c.commit();
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return simID;
    }

    /**
     * @param id simulation id
     * @return the simulation record specify by id
     */
    public static SimulationModel getByID(int id){
        SimulationModel result = null;
        Connection c = getConnection();
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("SELECT * FROM simulation where id=(?)");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()){
                result = new SimulationModel();
                result.setId(id);
                result.setScenarioID(rs.getInt("scenario_id"));
                result.setNumSectors(rs.getInt("num_sectors"));
                result.setResultPath(rs.getString("result_path"));
            }

            stmt.close();
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @return returns all simulations
     */
    public static List<SimulationModel> getAll(){
        List<SimulationModel> result = new ArrayList<>();
        Connection c = getConnection();
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("SELECT * FROM simulation");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()){
                result.add(new SimulationModel(){{
                    setId(rs.getInt("id"));
                    setScenarioID(rs.getInt("scenario_id"));
                    setNumSectors(rs.getInt("num_sectors"));
                    setResultPath(rs.getString("result_path"));
                }});
            }

            stmt.close();
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Store simulation result to disk and its path into db record
     *
     * @param simulationID the simulation id used to locate the record in db
     * @param result results received from remote simulators
     */
    public static void storeSimulationResult(int simulationID, List<VehicleModel> result){
        String path = String.format("%s/sim_result_%d.json", Util.getWorkDir(), simulationID);
        storeSimulationResultToFile(path, result);

        Connection c = getConnection();
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("UPDATE simulation set result_path=? where id=?");
            stmt.setString(1, path);
            stmt.setInt(2, simulationID);
            stmt.executeUpdate();
            c.commit();
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Store simulation result to disk in json format
     *
     * @param path  path to write to
     * @param result simulation result
     */
    private static void storeSimulationResultToFile(String path, List<VehicleModel> result){
        ObjectMapper mapper = new ObjectMapper();
        try {
            String json = mapper.writeValueAsString(result);
            FileUtils.writeStringToFile(
                    new File(path),
                    json,
                    Charset.forName("UTF-8"),
                    false
            );
        } catch (IOException e) {
            logger.warn(e.getMessage(), e);
        }
    }

    /**
     * Read simulation result from disk
     *
     * @param id  simulation id, it is used to locate the path to read the result from
     * @return
     */
    public static List<VehicleModel> getSimulationResultByID(int id){
        ObjectMapper mapper = new ObjectMapper();
        try {
            Path path = Paths.get(SimulationDAO.getByID(id).getResultPath());
            VehicleModel[] json = mapper.readValue(
                    new String(Files.readAllBytes(path)),
                    VehicleModel[].class
            );
            return Arrays.asList(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
