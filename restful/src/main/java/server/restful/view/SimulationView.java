/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.view;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import server.restful.dao.SimulationDAO;
import server.restful.model.MapModel;
import server.restful.model.SimulationModel;
import server.restful.model.VehicleModel;
import server.restful.registry.RegistryFactory;
import server.restful.registry.ServiceRegistry;
import server.restful.service.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/simulation")
public class SimulationView {
    @PostMapping("")
    public SimulationModel createSimulation(
            @RequestParam("scenario_id") int scenarioID,
            @RequestParam("num_sectors") int numSectors
    ) {
        int simID = SimulationDAO.create(scenarioID, numSectors);
        return new SimulationModel() {{
            setId(simID);
            setScenarioID(scenarioID);
            setNumSectors(numSectors);
            setMapID(-1);
        }};
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<SimulationModel> getSimulation() {
        return SimulationDAO.getAll();
    }

    @PostMapping("/{id}/start")
    public String startSimulation(@PathVariable("id") int simulationID) {
        ServiceRegistry registry = RegistryFactory.getZookeeperRegistry();

        // if you are using mac and running a stand alone RMIServer with docker on localhost:10101,
        // then please use following code
//        ServiceRegistry registry = RegistryFactory.getDefaultRegistry();

        try {
            Thread thread = new Thread(
                    new SimulationService(simulationID, registry)
            );
            thread.start();
            return "ok";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "simulation failed";
    }

    @PostMapping("/vehicle/{id}/navigate-to-charging-station")
    public String navigateToChargingStation(
            @PathVariable("id") String vehicleID,
            @RequestParam("from") long from) {
        VehicleService svc = VehicleService.getInstanceByID(vehicleID);
        if (svc == null){
            return null;
        }

        long chargingStation = ChargingStationService.getNearestChargingStation(svc.getMapService(), from);

        // no reachable charging station
        long newTarget = -1;

        try {
            newTarget = svc.insertNewRoadPoint(from, chargingStation);
        } catch (NavigationService.PathNotExistException ignored) {
        }
        return String.valueOf(newTarget);
    }

    @GetMapping("/{id}/finished")
    public Map<String, Boolean> getStatus(@PathVariable("id") int simulationID) {
        return new HashMap<String, Boolean>() {{
            put("result", SimulationService.isSimulationFinished(simulationID));
        }};
    }

    @GetMapping("/{id}/result")
    public List<VehicleModel> getResult(@PathVariable("id") int simulationID) {
        return SimulationDAO.getSimulationResultByID(simulationID);
    }

    @GetMapping("/{id}/map-data/{sectorIdx}")
    public MapModel getResult(
            @PathVariable("id") int simulationID,
            @PathVariable("sectorIdx") int sectorIdx
    ) {
        SimulationModel simulationModel = SimulationDAO.getByID(simulationID);
        SimulationService simulationService = new SimulationService(simulationID);
        try {
            MapService mapService = new MapService(simulationService.getSimulationMap().getId(), simulationModel.getNumSectors());
            return mapService.getSectorData(sectorIdx);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Map not found.", e);
        }
    }
}
