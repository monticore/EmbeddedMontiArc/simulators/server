/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.view;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import server.restful.dao.MapDAO;
import server.restful.model.MapModel;
import server.restful.service.MapService;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/map")
public class MapView {
    @PostMapping("")
    public MapModel createMap(@RequestParam("file")MultipartFile file) throws IOException {
        return MapService.saveMap(file.getInputStream(), file.getOriginalFilename());
    }

    @GetMapping("{id}")
    public MapModel createMap(@PathVariable int id){
        MapModel map = MapDAO.getByID(id);
        if (map == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "map not found");
        }
        // not revealing local path to the map file for safety reason. Besides, the path is
        // irrelevant for the client.
        map.setPath(null);
        return map;
    }

    @RequestMapping(value="", method= RequestMethod.GET)
    public List<MapModel> listMaps(){
        return MapDAO.getAll();
    }
}
