/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.view;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import server.restful.dao.ScenarioDAO;
import server.restful.model.ScenarioModel;
import server.restful.service.ScenarioService;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/scenario")
public class ScenarioView {
    @PostMapping("")
    public ScenarioModel createMap(@RequestParam("file") MultipartFile file) throws IOException {
        return ScenarioService.saveScenario(file.getInputStream(), file.getOriginalFilename());
    }

    @RequestMapping(value="", method=RequestMethod.GET)
    public List<ScenarioModel> listMaps(){
        return ScenarioDAO.getAll();
    }
}
