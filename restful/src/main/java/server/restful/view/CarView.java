/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.view;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import server.restful.dao.CarDAO;
import server.restful.model.CarModel;
import server.restful.service.CarService;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/car")
public class CarView {

    @PostMapping("")
    public CarModel createCar(@RequestParam("file") MultipartFile file) throws IOException {
        return CarService.saveCar(file.getInputStream(), file.getOriginalFilename());
    }

    @RequestMapping(value="", method= RequestMethod.GET)
    public List<CarModel> listCars() {
        return CarDAO.getAll();
    }

}
