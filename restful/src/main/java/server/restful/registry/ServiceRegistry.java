/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.registry;

public interface ServiceRegistry {
    /**
     * @return An instance of autopilot service that has been reserved.
     * @throws NoServiceException if no autopilot available
     */
    abstract public RemoteAutopilotService getAutopilot() throws NoServiceException;

    /**
     * @return An instance of simulator service that has been reserved.
     * @throws NoServiceException if no autopilot available
     */
    abstract public RemoteSimulatorService getSimulator() throws NoServiceException;

    /**
     * Release all reserved services.
     */
    abstract public void releaseAll();

    public class NoServiceException extends Exception{

    }
}
