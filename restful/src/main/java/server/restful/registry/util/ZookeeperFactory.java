/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.registry.util;

import org.I0Itec.zkclient.ZkClient;
import org.slf4j.LoggerFactory;

public class ZookeeperFactory {
    public static ZkClient newInstance(){
        String zooServers = System.getenv("ZOO_SERVERS");
        if (zooServers == null){
            zooServers = "localhost:2181";
        }
        LoggerFactory.getLogger(ZookeeperFactory.class).info("Initialize connection to zookeeper {}", zooServers);
        return new ZkClient(zooServers, 10000);
    }
}
