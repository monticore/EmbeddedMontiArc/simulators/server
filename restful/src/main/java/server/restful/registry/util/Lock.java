/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.registry.util;

import org.I0Itec.zkclient.IZkDataListener;
import org.I0Itec.zkclient.ZkClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * The lock register it self as a node under /path/locks/lock-[sequence]
 * At any time snapshot, there can only be one owner of of the distributed lock.
 * More specifically, among all children of /path/locks, only the node with the smallest sequence number, will consider
 * itself as the owner of this distributed lock.
 */
public class Lock {
    private final Logger logger = LoggerFactory.getLogger(Lock.class);
    String basePath;
    String lockPath;
    String sequence; //  lock node = basePath/locks/lockPath-sequence
    ZkClient zkClient;

    /**
     * @param path The base path which the Lock should be acquired. For example given path = /resource,
     *             the lock be acquired in /resource/locks/lock-[sequence]
     * @param zkClient
     */
    public Lock(String path, ZkClient zkClient) {
        this.basePath = path + "/locks";
        this.lockPath = basePath + "/lock-";
        this.zkClient = zkClient;
    }

    /**
     * Acquire a lock.
     * @param timeoutMs max wait time in milliseconds
     * @throws TimeoutException
     * @throws AlreadyAcquired
     */
    public synchronized void acquire(long timeoutMs) throws TimeoutException, AlreadyAcquired {
        if (!zkClient.exists(basePath)){
            zkClient.createPersistent(basePath, true);
        }

        if (sequence != null){
            logger.warn("Lock has been acquired, please initialize another lock");
            throw new AlreadyAcquired();
        }

        logger.info("Acquiring lock...");
        String node = zkClient.createEphemeralSequential(lockPath, "");
        sequence = getLockSequenceFromPath(node);
        logger.info("Created lock, sequence: " + sequence);

        List<String> pendingLocks = getPendingLocks();
        if (isOwner(pendingLocks)) {
            logger.info("Lock acquired");
            return;
        }

        // Wait for the lock that are currently waiting in front of this lock to acquire and release.
        // To avoid the heard effect, we should not wait for the owner to release.
        waitForLockToRelease(getPendingLockSequenceBeforeMe(pendingLocks), timeoutMs);
    }

    /**
     * Try to acquired a lock without waiting.
     * @throws TimeoutException
     * @throws AlreadyAcquired
     */
    public void acquireNoWait() throws TimeoutException, AlreadyAcquired {
        acquire(0);
    }

    /**
     * Release an acquired lock.
     */
    public void release(){
        String path = lockPath + sequence;
        logger.info("Release lock {}", path);
        zkClient.deleteRecursive(path);
        sequence = null;
    }

    /**
     * @return if the distributed lock is owned by this instance
     */
    public boolean acquired(){
        return isOwner(getPendingLocks());
    }

    /**
     * Extract the sequence number of a lock. lock-000001 has sequence string 000001. This number is assigned
     * by zookeeper
     *
     * @param path path of the lock. Example: /resource/locks/lock-00000001
     * @return sequence string of the this lock
     */
    String getLockSequenceFromPath(String path) {
        return path.substring(path.lastIndexOf("-")).split("-", 2)[1];
    }

    /**
     * @return List of lock sequence that are waiting in the line to own the distributed lock.
     */
    private List<String> getPendingLocks(){
        return zkClient.getChildren(basePath).stream()
                .map(this::getLockSequenceFromPath)
                .collect(Collectors.toList());
    }

    /**
     * Return the lock that is waiting one place ahead of this one. More specifically, the lock with maximum sequence
     * number over all locks, which has a smaller sequence number than this.
     *
     * @param pendingLocks Locks that is waiting to own the distributed lock
     * @return
     */
    private String getPendingLockSequenceBeforeMe(List<String> pendingLocks){
        Optional<String> result = pendingLocks.stream()
                .filter(seq -> Integer.valueOf(seq) < Integer.valueOf(sequence))
                .max(Comparator.naturalOrder());
        return result.orElse("99999999999");
    }

    /**
     * Check if the distributed lock is currently owned by this instance.
     * It is consider to "own" the distributed lock if the current sequence is the smallest one among all
     * pending locks.
     *
     * @param pendingLocks
     * @return
     */
    private boolean isOwner(List<String> pendingLocks){
        return getOwner(pendingLocks).equals(sequence);
    }

    /**
     * Return the owner of the lock
     *
     * @param pendingLocks
     * @return
     */
    private String getOwner(List<String> pendingLocks){
        String owner = String.valueOf(Integer.MAX_VALUE);
        for (String lock: pendingLocks){
            if (Integer.valueOf(lock) < Integer.valueOf(owner)){
                owner = lock;
            }
        }
        return owner;
    }

    public void waitForLockToRelease(String lockSequence, long timeoutMs) throws TimeoutException {
        logger.info("Waiting for lock-{} to release", lockSequence);
        String path = lockPath + lockSequence;

        LockWatcher watcher = new LockWatcher();
        zkClient.subscribeDataChanges(lockPath + lockSequence, watcher);

        // In case the node is deleted before the watcher is registered, this call ensures
        // the watcher will get notified with a NodeDeleted event, hence will be able to discover that the lock
        // is available. Otherwise a TimeoutException will falsely be thrown.
        zkClient.exists(path);

        final long start = System.currentTimeMillis();
        while (!watcher.isFree() && System.currentTimeMillis() - start < timeoutMs){
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                logger.warn(e.getMessage(), e);
            }
        }

        if (!watcher.isFree()) {
            release();
            throw new TimeoutException();
        }else{
            logger.info("Lock acquired");
        }
    }


    private class LockWatcher implements IZkDataListener{
        private boolean isFree;

        public LockWatcher() {
            this.isFree = false;
        }

        public boolean isFree() {
            return isFree;
        }

        @Override
        public void handleDataChange(String s, Object o) throws Exception {

        }

        @Override
        public void handleDataDeleted(String s) throws Exception {
            isFree = true;
        }
    }

    public class TimeoutException extends Exception{

    }
    
    public class AlreadyAcquired extends Exception{

    }
}
