/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.registry;

public class RemoteAutopilotService extends RemoteService{

    public static final String ROOT_PATH = "/rmi-servers";
    public static final String BASE_PATH = ROOT_PATH + "/all";
    public final String busyPath;
    public static final int CAPACITY = 1;

    private String node;


    public RemoteAutopilotService(String path) {
        super(path);
        busyPath = String.format("%s/busy/%s", ROOT_PATH, super.toString());
    }

    @Override
    public boolean isAvailable() {
        if (!zkClient.exists(path)){
            return false;
        }

        if(!zkClient.exists(busyPath)) {
            zkClient.createPersistent(busyPath, true);
            return true;
        }

        return zkClient.getChildren(busyPath).size() < CAPACITY;
    }

    @Override
    public boolean reserve() {
        if(!isAvailable()){
            return false;
        }

        node = zkClient.createEphemeralSequential(busyPath + "/client-", null);
        logger.info("Reserved {}", path);
        return true;
    }

    @Override
    public void release() {
        zkClient.delete(node);
        logger.info("Released {}", path);
    }
}
