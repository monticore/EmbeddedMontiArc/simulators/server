/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.registry;

import org.I0Itec.zkclient.ZkClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import server.restful.registry.util.Lock;
import server.restful.registry.util.ZookeeperFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * ZookeeperRegistry implements the client side service discovery through zookeeper servers.
 * It supports the discovery of 2 kinds of service: autopilot and simulator.
 */
public class ZookeeperRegistry implements ServiceRegistry {
    Logger logger = LoggerFactory.getLogger(ServiceRegistry.class);
    ZkClient zkClient;

    // TODO logger
    private List<RemoteService> services;

    public ZookeeperRegistry(){
        zkClient = ZookeeperFactory.newInstance();
        services = new ArrayList<>();
    }

    @Override
    public RemoteAutopilotService getAutopilot() throws NoServiceException {
        return (RemoteAutopilotService) reserveService(RemoteAutopilotService.BASE_PATH, RemoteAutopilotService.class);
    }

    @Override
    public RemoteSimulatorService getSimulator() throws NoServiceException {
        return (RemoteSimulatorService) reserveService(RemoteSimulatorService.BASE_PATH, RemoteSimulatorService.class);
    }

    /**
     * Try to reserve a service instance under given basePath.
     *
     * @param basePath base path which all instance of a specific type of service register themself under.
     *                 Example: simulator at ip:port register as /simulators/ip:port, base path is /simulator.
     * @param service what type of service should be reserved
     * @return an instance that has been successfully reserved
     * @throws NoServiceException if no service available
     */
    private RemoteService reserveService(String basePath, Class service) throws NoServiceException {
        Lock lock = new Lock(basePath, ZookeeperFactory.newInstance());
        try {
            lock.acquireNoWait();
        } catch (Lock.TimeoutException | Lock.AlreadyAcquired e) {
            throw new NoServiceException();
        }

        RemoteService result = null;
        for(String node: zkClient.getChildren(basePath)){
            if (!isHostPortFormat(node)){
                continue;
            }

            try {
                RemoteService svc = (RemoteService) service.getConstructor(String.class).newInstance(basePath + "/" + node);
                if (svc.reserve()){
                    this.services.add(svc);
                    result = svc;
                    break;
                }
            } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
                logger.warn(e.getMessage(), e);
            }
        }

        lock.release();
        if (result != null){
            return result;
        }else{
            throw new NoServiceException();
        }
    }

    @Override
    public void releaseAll() {
        logger.info("Releasing all services...");

        for (RemoteService svc: services){
            svc.release();
        }
    }

    private boolean isHostPortFormat(String str){
        return Pattern.compile("\\S+:\\d+").matcher(str).matches();
    }
}
