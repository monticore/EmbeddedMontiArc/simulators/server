/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.registry;

/**
 * DefaultRegistry a registry for the convenient of development. If you don't feel like to setup zookeeper,
 * just fire up a RMIServer at localhost:10101 and a simulator at localhost:6000. DefaultRegistry will ignore
 * the availability of all services and always return them accordingly.
 */
public class DefaultRegistry implements ServiceRegistry {
    @Override
    public RemoteAutopilotService getAutopilot() {
        return new RemoteAutopilotService("/localhost:10101");
    }

    @Override
    public RemoteSimulatorService getSimulator() {
        return new RemoteSimulatorService("/localhost:6000");
    }

    @Override
    public void releaseAll() {
        // nothing need to be done
    }
}
