/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.registry;

import java.util.List;

public class RemoteSimulatorService extends RemoteService{
    public static final String BASE_PATH = "/simulators";
    public static final String OCCUPATION_PATH = BASE_PATH + "/occupied";

    public RemoteSimulatorService(String path) {
        super(path);
    }

    @Override
    public boolean isAvailable() {
        // Check if service exist
        if(!zkClient.exists(path)){
            return false;
        }

        // Check if there is any occupied service
        if (!zkClient.exists(OCCUPATION_PATH)){
            zkClient.createPersistent(OCCUPATION_PATH, true);
            return true;
        }

        List<String> occupiedSimulators = zkClient.getChildren(OCCUPATION_PATH);
        for (String hostPort: occupiedSimulators){
            if (hostPort.equals(host + ":" + port)){
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean reserve() {
        if (isAvailable()){
            zkClient.createEphemeral(String.format("%s/%s:%d", OCCUPATION_PATH, host, port));
            logger.info("Reserved {}", path);
            return true;
        }
        return false;
    }

    @Override
    public void release() {
        zkClient.delete(String.format("%s/%s:%d", OCCUPATION_PATH, host, port));
        logger.info("Released {}", path);
    }

    public String toString(){
        return "simulator " + super.toString();
    }
}
