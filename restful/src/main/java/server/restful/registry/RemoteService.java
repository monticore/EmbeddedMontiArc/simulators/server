/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.registry;

import org.I0Itec.zkclient.ZkClient;
import org.I0Itec.zkclient.exception.ZkTimeoutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import server.restful.registry.util.ZookeeperFactory;
import server.restful.util.Util;

/**
 * RemoteService is a zookeeper ephemeral node under the hood. When a service goes online, it creates an ephemeral
 * node in zookeeper, indicates there ip, port and other information if necessary, the node will be removed by zookeeper
 * or the service itself when it goes offline for any reason.
 *
 * For example, a node /hamburger/instanceHost:port with node data called "data" means that there are a instance
 * that providing hamburger service at instanceHost:port, the "data" could store information such as version,
 * size, price, calorie etc.
 */
public abstract class RemoteService {
    Logger logger = LoggerFactory.getLogger(RemoteService.class);

    final String path;
    final String host;
    final int port;
    ZkClient zkClient;


    /**
     * @param path full path to the ephemeral node created by the service instance.
     *             Format: /path/to/service/host:port
     */
    RemoteService(String path) {
        this.path = path;
        try{
            zkClient = ZookeeperFactory.newInstance();
        } catch (ZkTimeoutException e){
            logger.warn(e.getMessage());
        }

        String[] connectionStr = path.substring(path.lastIndexOf("/") + 1).split(":");
        host = connectionStr[0];
        port = Integer.valueOf(connectionStr[1]);
    }

    abstract public boolean isAvailable();

    abstract public boolean reserve();

    abstract public void release();

    public String getPath() {
        return path;
    }

    public String getHost() {
        // windows platform has problem connecting to local service with hostname other than localhost and "127.0.0.1"
        // in this case we hardcode the host name to "localhost"
        if (Util.isWindows()){
            return "localhost";
        }
        return host;
    }

    public int getPort() {
        return port;
    }

    public String toString(){
        return String.format("%s:%d", host, port);
    }

}
