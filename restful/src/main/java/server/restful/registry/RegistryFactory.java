/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.registry;

import org.I0Itec.zkclient.exception.ZkTimeoutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * ZOO_SERVERS are connection string for connecting zookeeper servers.
 * Example: server.1=0.0.0.0:2888:3888 server.2=zoo2:2888:3888 server.3=zoo3:2888:3888
 * If it does not exist, registry factor will try localhost:2181
 * If it also cant establic connection, a defaultRegistry will be returned, which
 */
public class RegistryFactory {
    private static Logger logger = LoggerFactory.getLogger(RegistryFactory.class);

    /**
     * This factory method first try to return instance of ZookeeperRegistry, if the connection to zookeeper failed,
     * it then returns an instance of DefaultRegistry.
     *
     * @return An instance of ServiceRegistry
     */
    public static ServiceRegistry newInstance(){
        ServiceRegistry result;
        try {
            result = getZookeeperRegistry();
        }catch (ZkTimeoutException e){
            logger.error(e.getMessage());
            result = getDefaultRegistry();
        }

        return result;
    }

    public static ServiceRegistry getDefaultRegistry(){
        logger.info("Initializing default registry");
        return new DefaultRegistry();
    }


    public static ServiceRegistry getZookeeperRegistry(){
        logger.info("Initializing default zookeeper registry with zookeeper");
        return new ZookeeperRegistry();
    }
}
