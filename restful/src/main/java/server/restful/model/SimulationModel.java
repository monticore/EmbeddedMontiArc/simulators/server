/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.model;


import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SimulationModel {
    private int id;
    private int scenarioID;
    private int numSectors;
    private int mapID;
    private String resultPath;

//    private String resultPath;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getScenarioID() {
        return scenarioID;
    }

    public void setScenarioID(int scenarioID) {
        this.scenarioID = scenarioID;
    }

    public int getNumSectors() {
        return numSectors;
    }

    public void setNumSectors(int numSectors) {
        this.numSectors = numSectors;
    }

    public int getMapID() {
        return mapID;
    }

    public void setMapID(int mapID) {
        this.mapID = mapID;
    }

    public void setResultPath(String resultPath) {
        this.resultPath = resultPath;
    }

    public String getResultPath() {
        return resultPath;
    }
//    public String getResultPath() {
//        return resultPath;
//    }

//    public void setResultPath(String resultPath) {
//        this.resultPath = resultPath;
//    }
}
