/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.model;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;
import org.jgrapht.graph.DefaultWeightedEdge;

public class OSMEdge extends DefaultWeightedEdge {
        Vertex source;
        Vertex target;
        public double weight;
        public OSMEdge(Vertex source, Vertex target, double weight){
            this.source = source;
            this.target = target;
            this.weight = weight;
        }
    }
