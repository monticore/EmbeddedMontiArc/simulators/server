/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.model;


import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MapModel {
    public static class Node{
        private long id;
        private double longitude;
        private double latitude;
        private double altitude;
        private StreetSign streetSign;

        public Node(long id, double longitude, double latitude, double altitude, StreetSign streetSign) {
            this.id = id;
            this.longitude = longitude;
            this.latitude = latitude;
            this.altitude = altitude;
            this.streetSign = streetSign;
        }

        public Node() { }

        public long getId() {
            return id;
        }

        public double getLongitude() {
            return longitude;
        }

        public double getLatitude() {
            return latitude;
        }

        public double getAltitude() {
            return altitude;
        }

        public StreetSign getStreetSign() {
            return streetSign;
        }

        public void setId(long id) {
            this.id = id;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public void setAltitude(double altitude) {
            this.altitude = altitude;
        }

        public void setStreetSign(StreetSign streetSign) {
            this.streetSign = streetSign;
        }
    }

    public static class StreetSign{
        private long id;
        private String type;
        private boolean one;
        private boolean two;
        private double x1;
        private double x2;
        private double y1;
        private double y2;
        private double z1;
        private double z2;

        public StreetSign(long id, String type, boolean one, boolean two, double x1, double x2, double y1, double y2, double z1, double z2) {
            this.id = id;
            this.type = type;
            this.one = one;
            this.two = two;
            this.x1 = x1;
            this.x2 = x2;
            this.y1 = y1;
            this.y2 = y2;
            this.z1 = z1;
            this.z2 = z2;
        }

        public StreetSign() {        }

        public long getId() {
            return id;
        }

        public String getType() {
            return type;
        }

        public boolean isOne() {
            return one;
        }

        public boolean isTwo() {
            return two;
        }

        public double getX1() {
            return x1;
        }

        public double getX2() {
            return x2;
        }

        public double getY1() {
            return y1;
        }

        public double getY2() {
            return y2;
        }

        public double getZ1() {
            return z1;
        }

        public double getZ2() {
            return z2;
        }

        public void setId(long id) {
            this.id = id;
        }

        public void setType(String type) {
            this.type = type;
        }

        public void setOne(boolean one) {
            this.one = one;
        }

        public void setTwo(boolean two) {
            this.two = two;
        }

        public void setX1(double x1) {
            this.x1 = x1;
        }

        public void setX2(double x2) {
            this.x2 = x2;
        }

        public void setY1(double y1) {
            this.y1 = y1;
        }

        public void setY2(double y2) {
            this.y2 = y2;
        }

        public void setZ1(double z1) {
            this.z1 = z1;
        }

        public void setZ2(double z2) {
            this.z2 = z2;
        }
    }

    public static class Street{
        private long id;
        private double width;
        private List<Node> nodes;
        private List<Node> intersections;
        private String type;
        boolean isOneWay;
        private String streetPavements;
        private double speedLimit;

        public Street(long id, double width, List<Node> nodes, List<Node> intersections, String type, boolean isOneWay, String streetPavements, double speedLimit) {
            this.id = id;
            this.width = width;
            this.nodes = nodes;
            this.intersections = intersections;
            this.type = type;
            this.isOneWay = isOneWay;
            this.streetPavements = streetPavements;
            this.speedLimit = speedLimit;
        }

        public Street() {
        }

        public long getId() {
            return id;
        }

        public double getWidth() {
            return width;
        }

        public List<Node> getNodes() {
            return nodes;
        }

        public List<Node> getIntersections() {
            return intersections;
        }

        public String getType() {
            return type;
        }

        public boolean isOneWay() {
            return isOneWay;
        }

        public String getStreetPavements() {
            return streetPavements;
        }

        public double getSpeedLimit() {
            return speedLimit;
        }

        public void setId(long id) {
            this.id = id;
        }

        public void setWidth(double width) {
            this.width = width;
        }

        public void setNodes(List<Node> nodes) {
            this.nodes = nodes;
        }

        public void setIntersections(List<Node> intersections) {
            this.intersections = intersections;
        }

        public void setType(String type) {
            this.type = type;
        }

        public void setOneWay(boolean oneWay) {
            isOneWay = oneWay;
        }

        public void setStreetPavements(String streetPavements) {
            this.streetPavements = streetPavements;
        }

        public void setSpeedLimit(double speedLimit) {
            this.speedLimit = speedLimit;
        }
    }

    public static class Bound{
        private double minX;
        private double minY;
        private double minZ;
        private double maxX;
        private double maxY;
        private double maxZ;

        public Bound(double minX, double minY, double minZ, double maxX, double maxY, double maxZ) {
            this.minX = minX;
            this.minY = minY;
            this.minZ = minZ;
            this.maxX = maxX;
            this.maxY = maxY;
            this.maxZ = maxZ;
        }

        public Bound() {
        }

        public double getMinX() {
            return minX;
        }

        public double getMinY() {
            return minY;
        }

        public double getMinZ() {
            return minZ;
        }

        public double getMaxX() {
            return maxX;
        }

        public double getMaxY() {
            return maxY;
        }

        public double getMaxZ() {
            return maxZ;
        }

        public void setMinX(double minX) {
            this.minX = minX;
        }

        public void setMinY(double minY) {
            this.minY = minY;
        }

        public void setMinZ(double minZ) {
            this.minZ = minZ;
        }

        public void setMaxX(double maxX) {
            this.maxX = maxX;
        }

        public void setMaxY(double maxY) {
            this.maxY = maxY;
        }

        public void setMaxZ(double maxZ) {
            this.maxZ = maxZ;
        }
    }

    public static class EnvironmentObject{
        private List<Node> nodes;
        private String type;

        public EnvironmentObject(List<Node> nodes, String type) {
            this.nodes = nodes;
            this.type = type;
        }

        public EnvironmentObject() {
        }

        public List<Node> getNodes() {
            return nodes;
        }

        public String getType() {
            return type;
        }

        public void setNodes(List<Node> nodes) {
            this.nodes = nodes;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public static class Building extends EnvironmentObject {
        public Building(List<Node> nodes, String type) {
            super(nodes, type);
        }

        public Building() {
            super();
        }
    }

    public static class Waterway extends Building{
        public Waterway(List<Node> nodes, String type) {
            super(nodes, type);
        }

        public Waterway() {
            super();
        }
    }

    public static class HeightMap {
        private double[][] data;
        private double heightMapDeltaX;
        private double heightMapDeltaY;
        private double heightMapMinX;
        private double heightMapMinY;
        private double heightMapMaxX;
        private double heightMapMaxY;

        public HeightMap(double[][] data, double heightMapDeltaX, double heightMapDeltaY, double heightMapMinX, double heightMapMinY, double heightMapMaxX, double heightMapMaxY) {
            this.data = data;
            this.heightMapDeltaX = heightMapDeltaX;
            this.heightMapDeltaY = heightMapDeltaY;
            this.heightMapMinX = heightMapMinX;
            this.heightMapMinY = heightMapMinY;
            this.heightMapMaxX = heightMapMaxX;
            this.heightMapMaxY = heightMapMaxY;
        }

        public HeightMap() {
        }

        public double[][] getData() {
            return data;
        }

        public double getHeightMapDeltaX() {
            return heightMapDeltaX;
        }

        public double getHeightMapDeltaY() {
            return heightMapDeltaY;
        }

        public double getHeightMapMinX() {
            return heightMapMinX;
        }

        public double getHeightMapMinY() {
            return heightMapMinY;
        }

        public double getHeightMapMaxX() {
            return heightMapMaxX;
        }

        public double getHeightMapMaxY() {
            return heightMapMaxY;
        }

        public void setData(double[][] data) {
            this.data = data;
        }

        public void setHeightMapDeltaX(double heightMapDeltaX) {
            this.heightMapDeltaX = heightMapDeltaX;
        }

        public void setHeightMapDeltaY(double heightMapDeltaY) {
            this.heightMapDeltaY = heightMapDeltaY;
        }

        public void setHeightMapMinX(double heightMapMinX) {
            this.heightMapMinX = heightMapMinX;
        }

        public void setHeightMapMinY(double heightMapMinY) {
            this.heightMapMinY = heightMapMinY;
        }

        public void setHeightMapMaxX(double heightMapMaxX) {
            this.heightMapMaxX = heightMapMaxX;
        }

        public void setHeightMapMaxY(double heightMapMaxY) {
            this.heightMapMaxY = heightMapMaxY;
        }
    }

    public static class ChargingStation{
        private long id;
        private double longitude;
        private double latitude;
        private double altitude;
        private String name;
        private int capacity;

        public ChargingStation(long id, double longitude, double latitude, double altitude, String name, int capacity) {
            this.id = id;
            this.longitude = longitude;
            this.latitude = latitude;
            this.altitude = altitude;
            this.name = name;
            this.capacity = capacity;
        }
        public long getId() {
            return id;
        }

        public double getLongitude() {
            return longitude;
        }

        public double getLatitude() {
            return latitude;
        }

        public double getAltitude() {
            return altitude;
        }

        public String getName(){ return name; }

        public int getCapacity(){ return capacity; }
    }

    private int id;
    private String name;
    private String path;
    private List<Street> streets;
    private List<Building> buildings;
    private List<Waterway> waterways;
    private List<ChargingStation> chargingStations;
    private Bound bounds;

    private HeightMap heightMap;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<Street> getStreets() {
        return streets;
    }

    public void setStreets(List<Street> streets) {
        this.streets = streets;
    }

    public List<Building> getBuildings() {
        return buildings;
    }

    public void setBuildings(List<Building> buildings) {
        this.buildings = buildings;
    }

    public List<Waterway> getWaterways() {
        return waterways;
    }

    public void setWaterways(List<Waterway> waterways) {
        this.waterways = waterways;
    }

    public List<ChargingStation> getChargingStations() {
        return chargingStations;
    }

    public void setChargingStations(List<ChargingStation> chargingStations) { this.chargingStations = chargingStations; }

    public Bound getBounds() {
        return bounds;
    }

    public void setBounds(Bound bounds) {
        this.bounds = bounds;
    }

    public HeightMap getHeightMap() {
        return heightMap;
    }

    public void setHeightMap(HeightMap heightMap) {
        this.heightMap = heightMap;
    }
}
