/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class VehicleModel {
    private String id;
    private List<Dataframe> frames;

    private double distanceToTargetInMeter;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Dataframe> getFrames() {
        return frames;
    }

    public void setFrames(List<Dataframe> frames) {
        this.frames = frames;
    }

    public double getDistanceToTargetInMeter() {
        return distanceToTargetInMeter;
    }

    public void setDistanceToTargetInMeter(double distanceToTargetInMeter) {
        this.distanceToTargetInMeter = distanceToTargetInMeter;
    }
}
