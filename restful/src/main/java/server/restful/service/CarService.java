/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.service;

import server.restful.dao.CarDAO;
import server.restful.model.CarModel;
import server.restful.util.Util;

import java.io.InputStream;

public class CarService {

    public static String CAR_BASE_DIR = ScenarioService.SCENARIO_BASE_DIR;

    public static CarModel saveCar(InputStream inputStream, String fileName) {
        String path = CAR_BASE_DIR + "/" + fileName;
        String name = fileName.replace(".car", "");
        int id = CarDAO.create(name, path);
        Util.writeToFile(inputStream, path);
        return new CarModel(id, name, path);
    }

}
