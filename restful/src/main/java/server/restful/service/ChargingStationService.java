/* (c) https://github.com/MontiCore/monticore */
package server.restful.service;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChargingStationService {
    /**
     * Search for the charging station nearest to the given reference osm node.
     * The result will a osm node on a road that is nearest to the charging station (because we cant directly
     * navigate to a charging station, the navigation system only consider vertices that are a part of a drivable road)
     * <p>
     * The result will be very close to the real charging station, usually within meters.
     *
     * @param mapService
     * @param from       reference point
     * @return osm id
     */
    public static long getNearestChargingStation(MapService mapService, long from) {
        NavigationService navSvc = new NavigationService();
        List<Map<String, String>> chargingStations = listChargingStationOsmID(mapService.getMap().getPath());

        long nearest = 0;
        double shortestDistance = Double.MAX_VALUE;

        for (Map<String, String> station : chargingStations) {
            List<Vertex> path = null;
            long target = MapService.getNearestVertex(
                    mapService.getMap().getPath(),
                    Double.valueOf(station.get("lon")),
                    Double.valueOf(station.get("lat"))
            ).getOsmId();

            try {
                path = navSvc.findShortestPath(mapService, from, target);
            } catch (FileNotFoundException | NavigationService.PathNotExistException e) {
                e.printStackTrace();
                continue;
            }

            double distance = getDistanceOfPath(path);
            if (distance < shortestDistance) {
                shortestDistance = distance;
                nearest = target;
            }
        }

        return nearest;
    }

    private static double getDistanceOfPath(List<Vertex> vertices) {
        double result = 0;
        for (int i = 0; i < vertices.size(); i++) {
            if (i == 0) {
                continue;
            }

            result += vertices.get(i).getPosition().getDistance(vertices.get(i - 1).getPosition());
        }
        return result;
    }


    /**
     * @param mapPath path to the map file
     * @return a list of charging station OSM IDs from a given map
     */
    static List<Map<String, String>> listChargingStationOsmID(String mapPath) {
        List<Map<String, String>> result = new ArrayList<>();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = null;
        Document doc;

        XPathFactory xpfactory = XPathFactory.newInstance();
        XPath xpath = xpfactory.newXPath();

        try {
            builder = factory.newDocumentBuilder();
            doc = builder.parse(mapPath);

            XPathExpression xp = xpath.compile("//tag[@v=\"charging_station\"]/..");
            NodeList nodes = (NodeList) xp.evaluate(doc, XPathConstants.NODESET);
            for (int i = 0; i < nodes.getLength(); i++) {
                NamedNodeMap attrs = nodes.item(i).getAttributes();
                if (attrs == null) {
                    continue;
                }

                Node id = attrs.getNamedItem("id");
                Node lon = attrs.getNamedItem("lon");
                Node lat = attrs.getNamedItem("lat");
                if (id == null || lon == null || lat == null) {
                    continue;
                }

                result.add(new HashMap<String, String>() {{
                    put("id", id.getNodeValue());
                    put("lon", lon.getNodeValue());
                    put("lat", lat.getNodeValue());
                }});
            }
        } catch (ParserConfigurationException | SAXException | IOException | XPathExpressionException e) {
            e.printStackTrace();
        }

        return result;
    }
}
