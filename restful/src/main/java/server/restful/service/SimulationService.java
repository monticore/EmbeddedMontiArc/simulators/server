/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.service;

import de.monticore.lang.montisim.carlang.CarContainer;
import de.monticore.lang.montisim.carlang.CarLangTool;
import de.monticore.lang.montisim.simlang.SimLangTool;
import de.monticore.lang.montisim.simlang.adapter.SLMontiSimAdapter;
import de.monticore.lang.montisim.simlang.adapter.SimLangContainer;
import de.monticore.lang.montisim.util.types.ExplicitVehicle;
import de.monticore.lang.montisim.util.types.NumberUnit;
import de.monticore.lang.montisim.util.types.Path2D;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import server.restful.dao.MapDAO;
import server.restful.dao.ScenarioDAO;
import server.restful.dao.SimulationDAO;
import server.restful.model.MapModel;
import server.restful.model.ScenarioModel;
import server.restful.model.SimulationModel;
import server.restful.model.VehicleModel;
import server.restful.registry.RemoteService;
import server.restful.registry.ServiceRegistry;
import server.restful.util.Util;

import javax.measure.unit.SI;
import javax.measure.unit.Unit;
import java.io.File;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SimulationService implements Runnable {
    private SimulationModel simulation;
    private String uuid;
    private SimLangContainer simLangContainer;
    private SLMontiSimAdapter adapter;
    private Map<Integer, SimulatorService> simulators;
    private List<VehicleService> vehicles;
    private ServiceRegistry registry;

    private Properties prop;
    private Logger logger = LoggerFactory.getLogger(SimulationService.class);

    // a map of {simID: result}
    Map<Integer, List<VehicleModel>> results;

    public SimulationService(int simulationID) {
        if (results == null) {
            results = new HashMap<>();
        }

        uuid = UUID.randomUUID().toString();
        simulation = SimulationDAO.getByID(simulationID);
    }

    public SimulationService(int simulationID, ServiceRegistry registry) {
        this(simulationID);
        this.registry = registry;
    }

    /**
     * Assemble and start the simulation task. This includes
     * 1. Read and parse the scenario file.
     * 2. Find all the map files.
     * 3. Reserve a sub-simulator for each map.
     * 6. Transfer map data to all sub-simulators.
     * 4. Reserve rmi-server(autopilot) for each vehicle.
     * 5. Create vehicles in sub-simulators.
     * 6. Run and sync all sub-simulators.
     *
     * @param registry
     * @throws Exception
     */
    synchronized public void startSimulation(ServiceRegistry registry) throws Exception {
        //logger.info("startSimulation()");
        // prepare map, read scenario
        SimLangContainer scenario = getScenario();

        MapModel map = getSimulationMap();
        if (map == null) {
            return;
        }

        // sectorize map if necessary
        MapService mapService = new MapService(map.getId(), simulation.getNumSectors());
        if (!mapService.isSectorized() && simulation.getNumSectors() > 1) {
            mapService.sectorizeMap();
        }

        vehicles = new ArrayList<>();
        simulators = new HashMap<>();
        // create vehicles from scenario file
        for (ExplicitVehicle v : adapter.getVehicles().get()) {
            Path2D sourceTarget = v.getPath();
            VehicleService vehicle = new VehicleService(
                    mapService,
                    sourceTarget.getStartLat(), sourceTarget.getStartLong(),
                    sourceTarget.getDestLat(), sourceTarget.getDestLong(),
                    this
            );
            NumberUnit numberUnit = getScenario().getSimulationDuration().get().getNUnit().get();
            double totalDurationMs = Unit.valueOf(numberUnit.getUnit()).getConverterTo(SI.MILLI(SI.SECOND)).convert(numberUnit.getNumber());
            vehicle.setMaxDuration((int) totalDurationMs);

            CarContainer cc = null;

            if (v.getCarContainer() == null) {
                // logger.info(Paths.get("").toAbsolutePath().toString());
                String defaultCarModelPath = Util.getProperties().getProperty("default_car_model_path", "");
                if (defaultCarModelPath.equals("")){
                    // use built in car model
                    defaultCarModelPath = Util.getWorkDir();
                }
                String defaultCarModelName = Util.getProperties().getProperty("default_car_model_name");
                cc = CarLangTool.parse(Paths.get(defaultCarModelPath), defaultCarModelName)
                        .orElseThrow(() -> new Exception("Default car could not be found or parsed."))
                        .getCarContainer();
            } else {
                // logger.info(v.getCarContainer().getController());
                cc = v.getCarContainer();
            }
            vehicle.setCarContainer(cc);
            vehicle.setName(v.getName());

            // set battery here if needed
//            vehicle.setBatteryCapacity(10000000);
//            vehicle.setBatteryPercentage(19.9);

            // construct one simulator per sector by iterating over all vehicles for the sectors they need
            for (Integer sectorIdx : vehicle.getSectors()) {
                if (!simulators.containsKey(sectorIdx)) {
                    // find and reserve a simulator from service registry
                    RemoteService remoteSimulator = registry.getSimulator();
                    simulators.put(sectorIdx, new SimulatorService(
                            uuid, remoteSimulator.getHost(),
                            remoteSimulator.getPort(),
                            mapService.getSectorMapByIdx(sectorIdx))
                    );
                }
            }

            vehicles.add(vehicle);
        }

        // reset setup all sub simulators and transfer map data to them
        for (SimulatorService svc : simulators.values()) {
            svc.reset();
            svc.setMap();
        }

        // preparing all vehicles, they will be created in the remote simulators
        for (VehicleService svc : vehicles) {
            svc.init(registry);
        }

        // run simulation in each simulators until finish
        int stepDurationMs = Integer.parseInt(Util.getProperties().getProperty("simulation_step_ms", "500"));
        ExecutorService pool = Executors.newCachedThreadPool();
        while (!isSimulationFinished()) {
            // start all simulators
            runStep(pool, stepDurationMs);
            for (VehicleService v : vehicles) {
                // collect result, switch simulator if necessary
                v.runStep();
            }
        }

        // collect results
        results.put(simulation.getId(), new ArrayList<>());
        for (VehicleService svc : vehicles) {
            results.get(simulation.getId()).add(svc.getSimulationResult());
        }
        SimulationDAO.storeSimulationResult(simulation.getId(), results.get(simulation.getId()));
    }

    // TODO replace by using the getScenario() method
    public MapModel getSimulationMap() {
        SimLangContainer simLangContainer = getScenario();
        MapModel map = MapDAO.getByName(adapter.getMapName() + ".osm");
        return map;
    }

    public SimLangContainer getScenario() {
        ScenarioModel scenario = ScenarioDAO.getByID(simulation.getScenarioID());
        if (simLangContainer == null) {
            simLangContainer = SimLangTool.parseIntoContainer(Paths.get(scenario.getPath(), scenario.getName() + ".sim").toString());
        }
        if (adapter == null) {
            adapter = new SLMontiSimAdapter(simLangContainer);
        }
        return simLangContainer;
    }

    /**
     * A simulation is finished if all vehicles have finished to run their trajectories
     *
     * @return true if all simulator has finished their job
     */
    public boolean isSimulationFinished() {
        for (VehicleService vehicle : vehicles) {
            if (!vehicle.isAllTrajectoriesFinished()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Start all simulators to simulator a period of time
     *
     * @param pool
     */
    private void runStep(ExecutorService pool, int stepDurationMs) {
        List<Runnable> tasks = new ArrayList<>();
        for (SimulatorService sim : simulators.values()) {
            tasks.add(new SimulationRunner(sim, stepDurationMs));
        }
        CompletableFuture<?>[] futures = tasks.stream().map(task -> CompletableFuture.runAsync(task, pool)).toArray(CompletableFuture[]::new);
        CompletableFuture.allOf(futures).join();
    }

    class SimulationRunner implements Runnable {
        private SimulatorService simulator;
        private int duration;

        SimulationRunner(SimulatorService simulator, int duration) {
            this.simulator = simulator;
            this.duration = duration;
        }

        @Override
        public void run() {
            NumberUnit numberUnit = getScenario().getSimulationDuration().get().getNUnit().get();
            double totalDurationMs = Unit.valueOf(numberUnit.getUnit()).getConverterTo(SI.MILLI(SI.SECOND)).convert(numberUnit.getNumber());
            simulator.start(duration, (int)totalDurationMs);
        }
    }

    @Override
    public void run() {
        try {
            startSimulation(registry);
        } catch (ServiceRegistry.NoServiceException e) {
            logger.error("Resource not enough, please make sure there are enough simulators and rmi servers online.", e);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            registry.releaseAll();
        }
    }

    /**
     * @param id id of simulation to check(databaase id)
     * @return true if simulation is finished and the result is available in storage
     */
    public static boolean isSimulationFinished(int id) {
        SimulationModel simulationModel = SimulationDAO.getByID(id);
        if (simulationModel == null || simulationModel.getResultPath() == null) {
            return false;
        }
        return new File(simulationModel.getResultPath()).exists();
    }

    public SimulatorService getSimulatorBySectorIdx(int idx) {
        return simulators.get(idx);
    }

    public List<VehicleModel> getResult() {
        return results.get(simulation.getId());
    }
}

