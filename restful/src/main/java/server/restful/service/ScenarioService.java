/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.service;

import server.restful.dao.ScenarioDAO;
import server.restful.model.ScenarioModel;
import server.restful.util.Util;

import java.io.InputStream;

public class ScenarioService {
    public static String SCENARIO_BASE_DIR = Util.getWorkDir();

    public static ScenarioModel saveScenario(InputStream inputStream, String fileName){
        String path = SCENARIO_BASE_DIR;// + "/" + fileName;
        String name = fileName.replace(".sim", "");
        int id = ScenarioDAO.create(name, path);
        Util.writeToFile(inputStream, path + "/" + fileName);
        return new ScenarioModel(id, name, path);
    }
}
