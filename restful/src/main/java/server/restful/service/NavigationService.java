/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.service;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.navigationBlock.components.FindPath;
import org.apache.commons.math3.linear.RealVector;
import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import server.restful.graph.structures.OverlayEdge;
import simulation.environment.WorldModel;
import simulation.environment.osm.ParserSettings;
import simulation.environment.weather.WeatherSettings;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;



public class NavigationService {
    static Map<String, de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.structures.Graph> osmGraphs;
    private static Logger logger = LoggerFactory.getLogger(NavigationService.class);

    public static GraphPath<Vertex, OverlayEdge> findShortestOverlayPath(Graph<Vertex, OverlayEdge> g, Vertex source, Vertex target) {
        DijkstraShortestPath<Vertex, OverlayEdge> d = new DijkstraShortestPath<>(g);
        GraphPath<Vertex, OverlayEdge> path = d.getPath(source, target);
        return path;
    }

    public List<Vertex> findShortestOSMPath(String mapPath, long source, long target) throws PathNotExistException {
        if (osmGraphs == null || NavigationService.osmGraphs.get(mapPath) == null) {
            initGraph(mapPath);
        }

        de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.structures.Graph graph = osmGraphs.get(mapPath);
        FunctionBlock findPath = new FindPath();
        Map<String, Object> inputs = new HashMap<>();
        inputs.put(ConnectionEntry.FIND_PATH_graph.toString(), graph);
        inputs.put(ConnectionEntry.FIND_PATH_start_vertex.toString(), getVertexByOsmId(graph, source));
        inputs.put(ConnectionEntry.FIND_PATH_target_vertex.toString(), getVertexByOsmId(graph, target));

        findPath.setInputs(inputs);
        try {
            findPath.execute(0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<Vertex> trajectory = (List<Vertex>) findPath.getOutputs().get(ConnectionEntry.FIND_PATH_path.toString());
        if (trajectory.size() == 0) {
            throw new PathNotExistException(String.format("No path between %d and %d in %s",
                    source, target, mapPath
            ));
        }
        return trajectory;
    }

    public double getDistance(List<Vertex> trajectory) {
        double totalDistance = 0;
        for (int i = 1; i < trajectory.size(); i++) {
            RealVector s = trajectory.get(i - 1).getPosition();
            RealVector t = trajectory.get(i).getPosition();
            totalDistance += s.getDistance(t);
        }
        return totalDistance;
    }

    public void initGraph(String mapPath) {
        try {
            WorldModel.init(
                    new ParserSettings(new FileInputStream(mapPath), ParserSettings.ZCoordinates.ALLZERO),
                    new WeatherSettings()
            );
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.structures.Graph graph =
                new de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.structures.Graph(
                        WorldModel.getInstance().getControllerMap().getAdjacencies(), true);

        if (osmGraphs == null) {
            osmGraphs = new HashMap<>();
        }

        osmGraphs.put(mapPath, graph);
    }

    static Vertex getVertexByOsmId(
            de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.structures.Graph graph, long osmID) {
        for (Vertex v : graph.getVertices())
            if (v.getOsmId() == osmID) return v;
        return null;
    }

    /**
     * Find shortest path between given source and target using the overlay map and some parts of the original map.
     * The result will contain detailed trajectory both from source to the edge node of its sector, and target to its
     * edge node.
     * The path between two 2 edge nodes are just overlay path, it only contains information of how to navigate between
     * sectors to go from source edge node to the target edge node.
     * The server uses this path to track vehicles and transfer them between sectors (sub-simulators).
     *
     * @param mapService
     * @param source
     * @param target
     * @return
     * @throws FileNotFoundException
     * @throws PathNotExistException
     */
    public List<Vertex> findShortestPath(MapService mapService, long source, long target) throws FileNotFoundException, PathNotExistException {
        // read the overlay map
        Graph<Vertex, OverlayEdge> overlay = mapService.getOverlayMap();

        int sourceSectorIdx = mapService.getSectorIdxByOsmID(source);
        int targetSectorIdx = mapService.getSectorIdxByOsmID(target);
        String sourceMap = mapService.getSectorMapByIdx(sourceSectorIdx);
        String targetMap = mapService.getSectorMapByIdx(targetSectorIdx);

        // collect edge vertices both from source sector and target sector
        List<Vertex> sourceEdgeVertices = mapService.getGraphVerticesBySectorIdx(overlay, sourceSectorIdx);
        List<Vertex> targetEdgeVertices = mapService.getGraphVerticesBySectorIdx(overlay, targetSectorIdx);

        double shortestDist = Double.MAX_VALUE;
        List<Vertex> bestPath = null;

        if (sourceSectorIdx == targetSectorIdx) {
            bestPath = findShortestOSMPath(mapService.getMap().getPath(), source, target);
            shortestDist = getDistance(bestPath);
        }

        // trajectory planning
        GraphPath<Vertex, OverlayEdge> overlayPath;
        for (Vertex se : sourceEdgeVertices) {
            List<Vertex> pathStoSe;
            try {
                pathStoSe = findShortestOSMPath(sourceMap, source, se.getOsmId());
            } catch (PathNotExistException e) {
                continue;
            }
            double sourceSectorDist = getDistance(pathStoSe);

            for (Vertex te : targetEdgeVertices) {
                List<Vertex> pathTetoT;
                try {
                    pathTetoT = findShortestOSMPath(targetMap, te.getOsmId(), target);
                } catch (PathNotExistException e) {
                    continue;
                }
                double targetSectorDist = getDistance(pathTetoT);

                overlayPath = findShortestOverlayPath(overlay, se, te);
                double overlayDist = overlayPath.getWeight();

                double totalDist = sourceSectorDist + overlayDist + targetSectorDist;

                if (totalDist < shortestDist) {
                    shortestDist = totalDist;
                    bestPath = new ArrayList<>();
                    bestPath.addAll(pathStoSe);
                    List<Vertex> overlayVertices = new ArrayList<>(overlayPath.getVertexList());
                    if (overlayVertices.get(0).getOsmId().equals(bestPath.get(bestPath.size() - 1).getOsmId()))
                        overlayVertices.remove(0);
                    bestPath.addAll(overlayVertices);
                    if (pathTetoT.get(0).getOsmId().equals(bestPath.get(bestPath.size() - 1).getOsmId()))
                        pathTetoT.remove(0);
                    bestPath.addAll(pathTetoT);
                }
            }
        }

        if (bestPath == null || bestPath.size() == 0) {
            String msg = String.format(
                    "no path found between %d(sector: %d) and %d(sector: %d)",
                    source, sourceSectorIdx, target, targetSectorIdx
            );
            //logger.error(msg);
            throw new PathNotExistException(msg);
        }
        return bestPath;
    }

    /**
     * Cut trajectory into several segments, each segment is the vehicle's trajectory inside a simulator.
     * <p>
     * A segment belong to a sector n, where n is the smallest sector number over all nodes from the same segment.
     * This way we guarantee all the vehicle transfer between different sectors will be performed by simulator(sector)
     * with smaller sector number.
     * <p>
     * For each segment, the server only need to look for the first/last node as a vehicle's source/destination in that
     * sector. Once the vehicle reaches its destination in a segment, it will be scheduled by the server for its next
     * journey in the simulation world.
     *
     * <p>
     * For example:
     * Assume the trajectory: [osm1(sector 1), osm2(sector 1), osm3(sector 2), osm4(sector 5), osm5(sector 5), osm6(sector 1)]
     * It Will be grouped into:
     * [
     *  [osm1(sector 1), osm2(sector 1), osm3(sector 2)], (simulated by simulator 1)
     *  [osm3(sector 2)], osm4(sector 5)],                (simulated by simulator 2)
     *  [osm4(sector 5), osm5(sector 5)],                 (simulated by simulator 5)
     *  [osm5(sector 5), osm6(sector 1)]                  (simulated by simulator 1)
     * ]
     *
     * @param trajectory represents by a list of osm ids
     * @return list of segments
     */
    public List<List<Long>> computeTrajectorySegments(MapService mapService, List<Long> trajectory) {
        List<List<Long>> result = new ArrayList<>();
        // reduce size
        List<Long> toRemove = new ArrayList<>();
        for (int i = 1; i < trajectory.size() - 1; i++) {
            int prev = mapService.getSectorIdxByOsmID(trajectory.get(i - 1));
            int sector = mapService.getSectorIdxByOsmID(trajectory.get(i));
            int next = mapService.getSectorIdxByOsmID(trajectory.get(i + 1));
            if (sector == prev && sector == next) {
                toRemove.add(trajectory.get(i));
            }
        }
        trajectory.removeAll(toRemove);

        // find out indices of nodes, where vehicles could be transferred between sectors
        List<Integer> sectors = trajectory.stream().map(mapService::getSectorIdxByOsmID).collect(Collectors.toList());
        List<Integer> splitters = new ArrayList<>();
        for (int i = 1; i < trajectory.size(); i++) {
            int prev = sectors.get(i - 1);
            int sector = sectors.get(i);
            if (i == trajectory.size() - 1) {
                if (sector > prev) {
                    splitters.add(i);
                }
                continue;
            }

            int next = sectors.get(i + 1);
            if (prev != next && (sector > prev || sector > next)) {
                splitters.add(i);
            }
        }

        // in case all all node are in the same sector
        if (splitters.size() == 0) {
            result.add(trajectory);
        } else {
            // form the segments
            int lastIdx = 0;
            for (int idx : splitters) {
                List<Long> segment = new ArrayList<>(trajectory.subList(lastIdx, idx));
                segment.add(trajectory.get(idx));
                result.add(segment);

                lastIdx = idx;
            }

            if (lastIdx != trajectory.size() - 1) {
                result.add(trajectory.subList(lastIdx, trajectory.size()));
            }
        }

        // log it
        // StringBuilder sb = new StringBuilder();
        // for (List<Long> segment : result) {
        //     sb.append(String.format("[%s]", segmentToString(mapService, segment)));
        // }
        // logger.info(String.format("Trajectory segments:\n[%s]", sb.toString()));

        return result;
    }

    private String segmentToString(MapService mapService, List<Long> segment) {
        return String.join(
                "->",
                segment.stream()
                        .map(x -> String.format("%d(%d)", x, mapService.getSectorIdxByOsmID(x)))
                        .collect(Collectors.toList())
        );

    }

    public static class PathNotExistException extends Exception {
        public PathNotExistException(String message) {
            super(message);
        }
    }

}
