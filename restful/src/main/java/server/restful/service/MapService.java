/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.service;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleWeightedGraph;
import org.jgrapht.io.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import server.restful.dao.MapDAO;
import server.restful.graph.MetisGraph;
import server.restful.graph.OverlayUtil;
import server.restful.graph.structures.OverlayEdge;
import server.restful.model.MapModel;
import server.restful.model.OSMEdge;
import server.restful.util.Util;
import simulation.environment.World;
import simulation.environment.WorldModel;
import simulation.environment.osm.ApproximateConverter;
import simulation.environment.osm.ParserSettings;
import simulation.environment.weather.WeatherSettings;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MapService {
    private static Logger logger = LoggerFactory.getLogger(MapService.class);
    private MapModel map;
    private int mapID;
    private int numSectors;
    private MetisGraph metis;
    private Map<Long, Integer> osmIDtoSectorIdxMapping;

    public static String MAP_BASE_DIR = Util.getWorkDir();

    public MapService(int mapID, int numSectors) throws FileNotFoundException {
        this.mapID = mapID;
        this.numSectors = numSectors;
        this.map = MapDAO.getByID(mapID);
        this.metis = new MetisGraph(map.getPath(), numSectors);
    }

    public static MapModel saveMap(InputStream inputStream, String fileName){
        String path = Paths.get(MAP_BASE_DIR,  fileName).toString();
        Util.writeToFile(inputStream, path);
        int map_id = MapDAO.create(fileName, path);
        MapModel mapModel = new MapModel();
        mapModel.setId(map_id);
        return mapModel;
    }

    /**
     * Check if given map is sectorized into `numSectors` smaller maps
     *
     * @return
     */
    public boolean isSectorized() {
        // check if graph partition result exist
        if (!new File(metis.getSectorPath()).exists()) {
            return false;
        }

        // check if map is split into several smaller ones
        for (int i = 0; i < numSectors; i++) {
            if (!new File(metis.getMapPathBySectorIdx(i)).exists()) {
                return false;
            }
        }

        // check if overlay map exist
        if (!new File(metis.getSectorPath() + ".overlay.dot").exists()){
            return false;
        }

        return true;
    }

    public void sectorizeMap() throws Exception {
        if (numSectors <= 1){
            return;
        }
        metis.sectorize();
        OverlayUtil.createOverlayMap(
                metis.getOriginalGraph(),
                metis.getOsmIdSectorMapping(),
                metis.getSectorPath() + ".overlay.dot"
        );
    }


    public Map<Integer, String> getSectorFiles() throws Exception {
        HashMap<Integer, String> result = new HashMap<>();

        if (numSectors == 1){
            result.put(0, map.getPath());
            return result;
        }

        //  /path/to/originmap.osm
        String basePath = map.getPath().substring(0, map.getPath().lastIndexOf(".osm"));
        // /path/to/originmap.part.[numSectors]
        String baseName = basePath.substring(basePath.lastIndexOf("/") + 1) + ".part." + numSectors;
        // matches originmap.part.[numSectors]-[sectorIdx].osm
        String sectorPattern = String.format(baseName + "-(\\d+).osm");
        Pattern regex = Pattern.compile(sectorPattern);
        for(File f: new File(map.getPath()).getParentFile().listFiles()){
            Matcher m = regex.matcher(f.getName());
            if (f.isFile() && m.find()){
                int sectorID = Integer.valueOf(m.group(1));
                result.put(sectorID, f.getAbsolutePath());
            }
        }

        if (result.size() != numSectors){
            throw new Exception(String.format("Map %s, numSectors=%d has not been sectorized, do it first.", map.getPath(), numSectors));
        }
        return result;
    }

    public MapModel getSectorData(int sectorIdx) throws Exception {
        return new WorldBuilderService(getSectorMapByIdx(sectorIdx)).buildWorld();
    }

    public Graph<Vertex, OverlayEdge> getOverlayMap() throws FileNotFoundException {
        return OverlayUtil.readOverlay(metis.getSectorPath() + ".overlay.dot");
    }

    public List<Vertex> getGraphVerticesBySectorIdx(Graph<Vertex, OverlayEdge> graph, int sectorIdx) {
        List<Vertex> result = new ArrayList<>();
        for (Vertex node: graph.vertexSet()){
            if (getSectorIdxByOsmID(node.getOsmId()) == sectorIdx){
                result.add(node);
            }
        }
        return result;
    }

    public int getSectorIdxByOsmID(long osmID){
        if (numSectors == 1){
            return 0;
        }
        Map<Long, Integer> mapping = this.metis.getOsmIdSectorMapping();
        if (!mapping.containsKey(osmID)){
            String msg = osmID + " does not exist in map: " + this.getMap().getPath();
            logger.warn(msg);
            return -1;
        }
        return mapping.get(osmID);
    }

//    private void initOsmIDtoSectorIdxMapping(){
//        String filepath = String.format(
//                "%s/%s_%d/node_sector_mapping.txt",
//                MAP_BASE_DIR,
//                MapDAO.getByID(mapID).getName().replace(".osm", ""),
//                numSectors);
//        initOsmIDtoSectorIdxMapping(filepath);
//    }

//    public void initOsmIDtoSectorIdxMapping(String filepath){
//        osmIDtoSectorIdxMapping = new HashMap<>();
//        File file = new File(filepath);
//        Scanner sc = null;
//        try {
//            sc = new Scanner(file);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//
//        String line = "";
//        while (sc.hasNextLine()) {
//            line = sc.nextLine();
//            String[] split = line.split(",");
//            osmIDtoSectorIdxMapping.put(Long.valueOf(split[0]), Integer.valueOf(split[1]));
//        }
//    }

    static Graph<Vertex, DefaultWeightedEdge> readOverlayMap(String filepath){
        Graph<Vertex, DefaultWeightedEdge> g=new SimpleWeightedGraph<>(DefaultWeightedEdge.class);

        VertexProvider<Vertex> vp = new VertexProvider<Vertex>() {
            public Vertex buildVertex(String s, Map<String, Attribute> map) {
                return new Vertex(
                        Long.valueOf(map.get("label").toString()),
                        Long.valueOf(map.get("label").toString()),
                        new ArrayRealVector(new double[]{
                                Double.valueOf(map.get("lon").toString()),
                                Double.valueOf(map.get("lat").toString())
                        }),
                        0.0
                );
            }
        };
        EdgeProvider<Vertex, DefaultWeightedEdge> ep = new EdgeProvider<Vertex, DefaultWeightedEdge>() {
            @Override
            public DefaultWeightedEdge buildEdge(Vertex source, Vertex target, String s, Map<String, Attribute> map) {
                return new OSMEdge(source, target, Double.valueOf(map.get("weightStr").toString()));
            }
        };

        GmlImporter<Vertex, DefaultWeightedEdge> importer = new GmlImporter<>(vp, ep);
        try {
            importer.importGraph(g, new File(filepath));
        } catch (ImportException e) {
            e.printStackTrace();
        }

        return g;
    }

    public Vertex getVertexByOsmID(long osmID){
        return metis.getOriginalGraph().getVertex(osmID);
    }

    public static Vertex getNearestVertex(String mapPath, double longitude, double latitude){
        try {
            World world = WorldModel.init(
                    new ParserSettings(new FileInputStream(mapPath), ParserSettings.ZCoordinates.ALLZERO), new WeatherSettings()
            );
            de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.structures.Graph g =
                    new de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.structures.Graph(world.getControllerMap().getAdjacencies(), true);
            ApproximateConverter cvt = world.getParser().getConverter().getApproximateConverter();

            RealVector target = new ArrayRealVector(new double[]{
                    cvt.convertLongToMeters(longitude, latitude),
                    cvt.convertLatToMeters(latitude),
                    0
            }, false);
            double bestHeuristic = Double.MAX_VALUE;
            Vertex nearestNode = null;

            for (Vertex v: g.getVertices()){
                double heuristic = v.getPosition().getDistance(target);
                if (heuristic < bestHeuristic){
                    bestHeuristic = heuristic;
                    nearestNode = v;
                }
            }
            return nearestNode;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public String getSectorMapByIdx(int idx){
        if (numSectors == 1){
            return map.getPath();
        }
        return metis.getMapPathBySectorIdx(idx);
    }

    public int getMapID() {
        return mapID;
    }

    public int getNumSectors() {
        return numSectors;
    }

    public MapModel getMap() {
        return map;
    }


}
