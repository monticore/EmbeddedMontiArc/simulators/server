/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import server.restful.model.MapModel;
import simulation.environment.World;
import simulation.environment.WorldModel;
import simulation.environment.osm.ApproximateConverter;
import simulation.environment.osm.ParserSettings;
import simulation.environment.visualisationadapter.interfaces.*;
import simulation.environment.weather.WeatherSettings;
import simulation.environment.object.ChargingStation;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

public class WorldBuilderService {
    World world;
    MapModel mapModel;
    ApproximateConverter converter;
    private Logger logger = LoggerFactory.getLogger(WorldBuilderService.class);

    public WorldBuilderService(String mapPath) throws Exception {
        mapModel = new MapModel();
        world = WorldModel.init(
                new ParserSettings(new FileInputStream(mapPath), ParserSettings.ZCoordinates.ALLZERO),
                new WeatherSettings()
        );
        converter = world.getParser().getConverter().getApproximateConverter();
    }

    public MapModel buildWorld() {
        try {
            addStreets();
            addBuildings();
            addBound();
            addWaterWays();
            addHeightMap();
            addChargingStations();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapModel;
    }

    private void addStreets() throws Exception {
        List<MapModel.Street> streets = new ArrayList<>();
        for (EnvStreet street : world.getContainer().getStreets()) {
            List<MapModel.Node> nodes = getLonLatNodesFromXYNodes(street.getNodes());
            List<MapModel.Node> intersections = getLonLatNodesFromXYNodes((List<EnvNode>) street.getIntersections());
            streets.add(new MapModel.Street(
                    street.getOsmId(),
                    street.getStreetWidth().doubleValue(),
                    nodes,
                    intersections,
                    street.getStreetType().toString(),
                    street.isOneWay(),
                    street.getStreetPavement().toString(),
                    street.getSpeedLimit().doubleValue()
            ));
        }
        mapModel.setStreets(streets);
    }

    private List<MapModel.Node> getLonLatNodesFromXYNodes(List<EnvNode> nodes) {
        List<MapModel.Node> result = new ArrayList<>();
        for (EnvNode node : nodes) {
            double x = node.getX().doubleValue(), y = node.getY().doubleValue();
            StreetSign sign = node.getStreetSign();

            result.add(new MapModel.Node(
                    node.getOsmId(),
//                    x, y,
// uncomment blowing code to get longitude/latitude coordinate
                    converter.convertXToLong(x, y),
                    converter.convertYToLat(y),
                    node.getZ().floatValue(),
                    sign == null ? null : new MapModel.StreetSign(
                            sign.getId(), sign.getType().toString(), sign.isOne(), sign.isTwo(),
                            sign.getX1(), sign.getX2(),
                            sign.getY1(), sign.getY2(),
                            sign.getZ1(), sign.getZ2()
                    )
            ));
        }
        return result;
    }

    private void addBound() throws Exception {
        EnvBounds bound = world.getContainer().getBounds();
        double minX = bound.getMinX(), minY = bound.getMinY(),
                maxX = bound.getMaxX(), maxY = bound.getMaxY();
        mapModel.setBounds(new MapModel.Bound(
//                minX, minY, bound.getMinZ(), maxX, maxY, bound.getMaxZ()
// uncomment blowing code to get longitude/latitude coordinate
                converter.convertXToLong(minX, minY),
                converter.convertYToLat(minY),
                bound.getMinZ(),
                converter.convertXToLong(maxX, maxY),
                converter.convertYToLat(maxY),
                bound.getMaxZ()
        ));
    }

    private void addBuildings() throws Exception {
        List<MapModel.Building> buildings = new ArrayList<>();
        for (Building building : world.getContainer().getBuildings()) {
            Building.BuildingTypes buildingTypes = building.getBuildingTypes();
            buildings.add(new MapModel.Building(
                    getLonLatNodesFromXYNodes(building.getNodes()),
                    buildingTypes == null ? "" : buildingTypes.toString()
            ));
        }
        mapModel.setBuildings(buildings);
    }

    private void addWaterWays() throws Exception {
        List<MapModel.Waterway> waterways = new ArrayList<>();
        for (Waterway waterway : world.getContainer().getWaterway()) {
            Waterway.WaterTypes waterwayTypes = waterway.getWaterType();
            waterways.add(new MapModel.Waterway(
                    getLonLatNodesFromXYNodes(waterway.getNodes()),
                    waterwayTypes == null ? "" : waterwayTypes.toString()
            ));
        }
        mapModel.setWaterways(waterways);
    }

    private void addHeightMap() {
        VisualisationEnvironmentContainer container;
        try {
            container = world.getContainer();
        } catch (Exception e) {
            logger.warn(e.getMessage(), e);
            return;
        }

        mapModel.setHeightMap(new MapModel.HeightMap(
                container.getHeightMap(),
                container.getHeightMapDeltaX(),
                container.getHeightMapDeltaY(),
                container.getHeightMapMinPoint().getX(),
                container.getHeightMapMinPoint().getY(),
                container.getHeightMapMaxPoint().getX(),
                container.getHeightMapMaxPoint().getY()
        ));
    }

    private void addChargingStations() throws Exception {
        List<MapModel.ChargingStation> chargingStations = new ArrayList<>();
        for (ChargingStation chargingStation : world.getContainer().getChargingStations()) {
            chargingStations.add(new MapModel.ChargingStation(
                    chargingStation.getOsmId(),
                    chargingStation.getLocation().getEntry(0),
                    chargingStation.getLocation().getEntry(1),
                    chargingStation.getLocation().getEntry(2),
                    chargingStation.getName(),
                    chargingStation.getCapacity()
            ));
        }
        mapModel.setChargingStations(chargingStations);
    }
}
