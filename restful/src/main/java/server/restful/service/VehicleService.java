/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.service;

import de.monticore.lang.montisim.carlang.CarContainer;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import server.restful.model.Dataframe;
import server.restful.model.VehicleModel;
import server.restful.registry.RemoteAutopilotService;
import server.restful.registry.ServiceRegistry;
import server.restful.util.Util;

import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

/**
 *
 */
public class VehicleService {
    private static Logger logger = LoggerFactory.getLogger(VehicleService.class);
    private MapService mapService;
    private Vertex source;
    private Vertex target;

    private double batteryCapacity = 0;
    private double batteryPercentage = 0;

    private List<List<Long>> trajectories;
    private String id;
    private SimulationService simulation;

    private VehicleModel simulationResult;

    private CarContainer carContainer;


    private String rmiHost;
    private int rmiPort;

    private SimulatorService currentSimulator;
    private List<Long> currentTrajectory;
    private double minDistToTarget;
    private String name;

    private int maxDuration;

    private static Map<String, VehicleService> instances;

    public VehicleService(MapService mapService, float sourceLong, float sourceLat, float targetLong, float targetLat, SimulationService simulation) throws NavigationService.PathNotExistException {
        this(
                mapService,
                MapService.getNearestVertex(mapService.getMap().getPath(), sourceLong, sourceLat).getOsmId(),
                MapService.getNearestVertex(mapService.getMap().getPath(), targetLong, targetLat).getOsmId(),
                simulation
        );
    }

    public VehicleService(MapService mapService, long source, long target, SimulationService simulation) throws NavigationService.PathNotExistException {
        this.mapService = mapService;
        this.source = mapService.getVertexByOsmID(source);
        this.target = mapService.getVertexByOsmID(target);
        this.simulation = simulation;

        this.id = UUID.randomUUID().toString();
        simulationResult = new VehicleModel();
        simulationResult.setId(id);

        try {
            trajectories = calcTrajectorySegments(this.source, this.target);
        } catch (FileNotFoundException e) {
            logger.error(e.getMessage(), e);
        }

        if (instances == null) {
            instances = new HashMap<>();
        }
        instances.put(this.id, this);
    }

    /**
     * Prepare the vehicle, including
     * 1. reserve autopilot rmi-server resource
     * 2. prepare first trajectory segment which the vehicle will drive
     * 3. join the first simulator which is responsible for simulating the first segment
     *
     * @param registry service registry
     * @throws ServiceRegistry.NoServiceException if no autopilot rmi-server is available
     */
    public void init(ServiceRegistry registry) throws ServiceRegistry.NoServiceException, SimulatorService.NoVehicleCreatedException {
        RemoteAutopilotService svc = null;
        try {
            svc = registry.getAutopilot();
        } catch (ServiceRegistry.NoServiceException e) {
            e.printStackTrace();
            throw e;
        }
        rmiHost = svc.getHost();
        rmiPort = svc.getPort();
        currentTrajectory = trajectories.remove(0);
        int firstSectorIdx = mapService.getSectorIdxByOsmID(currentTrajectory.get(0));
        SimulatorService firstSimulator = simulation.getSimulatorBySectorIdx(firstSectorIdx);
        joinSimulator(firstSimulator);
    }

    /**
     * Collect simulation result from simulator
     */
    private void collectResultFromCurrentSimulator() {
        if (currentTrajectory == null) {
            return;
        }

        if (simulationResult.getFrames() == null) {
            simulationResult.setFrames(new ArrayList<>());
        }

        VehicleModel result = currentSimulator.getResultByVehicleID(id);
        simulationResult.setDistanceToTargetInMeter(result.getDistanceToTargetInMeter());
        simulationResult.getFrames().addAll(result.getFrames());
        simulationResult.setId(getName());

    }

    /**
     * Join the simulator by creating a vehicle in the simulator
     *
     * @param simulator
     */
    public void joinSimulator(SimulatorService simulator) throws SimulatorService.NoVehicleCreatedException {
        if (simulator.equals(currentSimulator)) {
            return;
        }
        if (currentSimulator != null) {
            currentSimulator.removeVehicle(id);
        }
        currentSimulator = simulator;

        if (carContainer == null) carContainer = Util.getDefaultCarContainer();

        if (batteryCapacity > 0){
            // create an EV if the vehicle has battery

            // get latest battery status
            double percentage = batteryPercentage;
            List<Dataframe> frames = simulationResult.getFrames();
            if (frames != null && frames.size() > 0) {
                percentage = frames.get(frames.size() - 1).getBattery();
            }

            currentSimulator.addVehicle(
                    id,
                    currentTrajectory.get(0),
                    currentTrajectory.get(currentTrajectory.size() - 1),
                    rmiHost, rmiPort,
                    carContainer.toJson(),
                    Boolean.valueOf(Util.getProperties().getProperty("use_modelica", "false")),
                    batteryCapacity, percentage
            );
        } else {
            currentSimulator.addVehicle(
                    id,
                    currentTrajectory.get(0),
                    currentTrajectory.get(currentTrajectory.size() - 1),
                    rmiHost, rmiPort,
                    carContainer.toJson(),
                    Boolean.valueOf(Util.getProperties().getProperty("use_modelica", "false"))
            );
        }
    }


    /**
     * Plan the vehicle's trajectory. A trajectory consists of segments. Each segment are a group of nodes
     * that belong to the same sector. The server only cares about the first and the last node of each segment,
     * they are source/destinations for the vehicle in each sector.
     *
     * @return
     * @throws NavigationService.PathNotExistException if no path could be found between source and target vertex
     * @throws FileNotFoundException                   if the overlay map file does not exist
     */
    public List<List<Long>> calcTrajectorySegments(Vertex source, Vertex target) throws FileNotFoundException, NavigationService.PathNotExistException {
        NavigationService navSvc = new NavigationService();

        // if the simulation only needs one sector, then compute the trajectory directly
        if (mapService.getNumSectors() == 1) {
            List<Vertex> trajectory = navSvc.findShortestOSMPath(mapService.getMap().getPath(), source.getOsmId(), target.getOsmId());
            List<Long> osmRoadPoints = new ArrayList<>();
            for (Vertex v : trajectory) {
                osmRoadPoints.add(Long.valueOf(v.getOsmId().toString()));
            }
            ArrayList<List<Long>> result = new ArrayList<List<Long>>() {{
                add(osmRoadPoints);
            }};
            if (result.size() == 0) {
                throw new NavigationService.PathNotExistException(
                        String.format("Can't find trajectory between osm node %d and %d",
                                source.getOsmId(), target.getOsmId())
                );
            }
            return result;
        }

        List<Vertex> shortestPath = navSvc.findShortestPath(mapService, source.getOsmId(), target.getOsmId());
        return navSvc.computeTrajectorySegments(
                mapService,
                shortestPath.stream().map(Vertex::getOsmId).collect(Collectors.toList())
        );
    }

    /**
     * @return number of sectors this vehicle drives through
     */
    public Set<Integer> getSectors() {
        HashMap<Integer, String> result = new HashMap<>();
        for (List<Long> trajectorySection : trajectories) {
            for (long osmID : trajectorySection) {
                result.put(mapService.getSectorIdxByOsmID(osmID), "");
            }
        }
        return result.keySet();
    }

    public void runStep() throws SimulatorService.NoVehicleCreatedException {
        if (isCurrentTrajectoryFinished()) {
            collectResultFromCurrentSimulator();

            if (!isAllTrajectoriesFinished()) {
                currentTrajectory = trajectories.remove(0);
                SimulatorService nextSimulator = simulation.getSimulatorBySectorIdx(mapService.getSectorIdxByOsmID(currentTrajectory.get(0)));
                joinSimulator(nextSimulator);
            }
        }
    }

    public boolean isCurrentTrajectoryFinished() {
        double distToTarget = currentSimulator.getVehicleByID(id).getDistanceToTargetInMeter();
        logger.info(String.format("Vehicle %s distance to target: %.2fm", id, distToTarget));
        if (distToTarget < minDistToTarget) {
            minDistToTarget = distToTarget;
            return false;
        }

        // if vehicle is driving away from target while it very close to it, consider it has reached its target.
        // this avoids the vehicle circling around at the destination without stopping, due to some bug in the simulator.
        List<Dataframe> frames = currentSimulator.getResultByVehicleID(id).getFrames();

        // instead of next line, we use the duration specified in the scenario file
        // maxDuration = Integer.parseInt(Util.getProperties().getProperty("max_simulation_time_ms", "10000"));
        if (distToTarget < 2 || (frames != null && frames.size() > 0 && frames.get(frames.size()-1).getTotalTime() > maxDuration)) {
            return true;
        }


        return false;
    }

    /**
     * Update vehicle's trajectory in current simulator.
     * This method ultimately change the value of PLANNED_TRAJECTORY_X/Y sensor from the vehicle in the simulator.
     *
     * @param source
     * @param destination
     */
    public void updateCurrentTrajectory(long source, long destination) {
        currentSimulator.updateVehicleTrajectory(id, source, destination);
    }

    public boolean isAllTrajectoriesFinished() {
        return isCurrentTrajectoryFinished() && trajectories.size() == 0;
    }

    public VehicleModel getSimulationResult() {
        return simulationResult;
    }

    public List<List<Long>> getTrajectories() {
        return trajectories;
    }


    public MapService getMapService() {
        return mapService;
    }


    /**
     * Insert a new destination between vehicle's current location and its final destination.
     * To do so, we compute 2 trajectories both from vehicle's current location to the given destination,
     * and then the trajectory from given destination to vehicle's original destination.
     *
     * It is useful for navigating to charging station. For example, if a vehicle's battery is low,
     * it should first take a detour to the nearest charging station, charge its battery,  and then go from the charging
     * station to its original destination.
     *
     * @param currentNode
     * @param newNode
     * @return the vehicle's next destination in its current sector. It should go to this returned destination and then goto
     * its original one.
     */
    public long insertNewRoadPoint(long currentNode, long newNode) throws NavigationService.PathNotExistException {
        if (currentTrajectory == null) {
            currentTrajectory = trajectories.get(0);
        }

        int currentSector = mapService.getSectorIdxByOsmID(currentTrajectory.get(0));
        int targetSector = mapService.getSectorIdxByOsmID(newNode);
        if (currentSector == targetSector) {
            return newNode;
        }

        try {
            List<List<Long>> pathToChargingStation = calcTrajectorySegments(
                    mapService.getVertexByOsmID(currentNode),
                    mapService.getVertexByOsmID(newNode)
            );
            List<List<Long>> pathChargingStationToDestination = calcTrajectorySegments(
                    mapService.getVertexByOsmID(newNode),
                    this.target
            );

            this.trajectories = mergeTrajectory(pathToChargingStation, pathChargingStationToDestination);

            List<Long> firstSegment = trajectories.get(0);
            if (firstSegment.size() > 0
                    && !firstSegment.get(firstSegment.size() - 1).equals(currentTrajectory.get(currentTrajectory.size() - 1))) {

                // update vehicle's trajectory in the simulator
                // this ultimately change the value of PLANNED_TRAJECTORY_X/Y sensor from the vehicle in the simulator.
                if (currentSimulator != null){
                    currentSimulator.updateVehicleTrajectory(
                            id,
                            firstSegment.get(0),
                            firstSegment.get(firstSegment.size() - 1)
                    );
                }
            }

            this.currentTrajectory = firstSegment;
            return this.currentTrajectory.get(this.currentTrajectory.size() - 1);
        } catch (FileNotFoundException e){
            e.printStackTrace();
            return -1;
        }
    }

    private List<List<Long>> mergeTrajectory(List<List<Long>> s, List<List<Long>> t) {
        s.addAll(t);
        return s;
    }

    static public VehicleService getInstanceByID(String vehicleID) {
        if (instances == null) {
            return null;
        }
        return instances.get(vehicleID);
    }

    public void setBatteryCapacity(double batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public void setBatteryPercentage(double batteryPercentage) {
        this.batteryPercentage = batteryPercentage;
    }

    public CarContainer getCarContainer() {
        return carContainer;
    }

    public void setCarContainer(CarContainer carContainer) {
        this.carContainer = carContainer;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    public List<Long> getCurrentTrajectory() {
        return currentTrajectory;
    }

    public void setMaxDuration(int duration) {
        maxDuration = duration;
    }

    public class PathNotExistException extends Exception {
        public PathNotExistException(String message) {
            super(message);
        }
    }
}
