/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful.service;

import com.google.protobuf.ByteString;
import io.grpc.ManagedChannelBuilder;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import protobuf.*;
import server.errorflags.ErrorFlag;
import server.restful.model.Dataframe;
import server.restful.model.MapModel;
import server.restful.model.VehicleModel;
import server.restful.util.Util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


/**
 * This class is a wrapper around gRPC APIs defined in the rpc module
 */
public class SimulatorService {
    private static Logger logger = LoggerFactory.getLogger(VehicleService.class);
    private String simulationID;
    private String mapPath;
    private String mapName;
    private SimControllerGrpc.SimControllerBlockingStub simulator;

    public SimulatorService(String simulationID, String host, int port, String mapPath){
        this.simulationID = simulationID;
        this.mapPath = mapPath;

        // TODO: use Path instead of String (then we can also avoid the isWindows() method)
        String filename = "";
        if (Util.isWindows()){
            filename = mapPath.substring(mapPath.lastIndexOf("\\") + 1);
            filename = filename.substring(filename.lastIndexOf("/") + 1);
        }else{
            filename = mapPath.substring(mapPath.lastIndexOf("/") + 1);
        }
        this.mapName = filename.replace(".osm", "");

        ManagedChannelBuilder<?> channel = ManagedChannelBuilder
                .forAddress(host, port)
                .maxInboundMessageSize((int) 30e6)  // allow maximum of 30 MByte message from simulator
                .usePlaintext();
        simulator = SimControllerGrpc.newBlockingStub(channel.build());
    }

    /**
     * Reset the simulator. After reset, the simulator is completely new. No map, no vehicle, time reset to 0 etc.
     */
    public void reset(){
        simulator.resetSimulator(ResetSimulatorRequest.newBuilder().build());
    }

    /**
     * Transfer map file to the simulator in chunks. Each chunk has the max. size of 1MB.
     */
    public void setMap(){
        try {
            InputStream input = new FileInputStream(mapPath);
            byte[] bytes = IOUtils.toByteArray(input);

            int batchSize = 1024 * 1024; // 1M per chunk
            int numChunk = bytes.length / batchSize; // number of chunks needed;
            numChunk += 1;
            for (int i=0; i < numChunk; i ++){
                SetMapRequest.Builder builder = SetMapRequest.newBuilder();
                builder.setIndex(i + 1);
                builder.setTotalChunk(numChunk);
                builder.setMapName(this.mapName);

                int nextChunkSize = i * batchSize + batchSize > bytes.length ? (bytes.length - i * batchSize) : batchSize;
                builder.addData(ByteString.copyFrom(bytes, i * batchSize, nextChunkSize));
                simulator.setMap(builder.build());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create one vehicle in the simulator
     *
     * @param id  global id, if left empty(""), a random UUID will be assigned by the simulator.
     * @param source the OSM node ID of the beginning of the trajectory
     * @param target the OSM node ID of the end of the trajectory
     * @param rmiHost the host of autopliot(RMIModelServer) which the vehicle should use
     * @param rmiPort the port of autopliot(RMIModelServer) which the vehicle should use
     * @param vehicleModel defines which model should autopilot use, i.e AutopilotAdapter
     * @param useModelica if yes the remote simulator will use ModelicaVehicle, otherwise it will use MassPointVehicle
     * @param batteryCapacity  capacity of vehicle's battery
     * @param batteryPercentage initial percentage of vehicle's battery
     * @throws NoVehicleCreatedException
     */
    public void addVehicle(
            String id, long source, long target, String rmiHost,
            int rmiPort, String vehicleModel, boolean useModelica,
            double batteryCapacity, double batteryPercentage) throws NoVehicleCreatedException {
        AddVehicleRequest.Builder builder = getAddVehicleRequestBuilder(
                id, source, target, rmiHost, rmiPort, vehicleModel, useModelica);
        builder.setBatteryCapacity(batteryCapacity);
        builder.setBatteryPercentage(batteryPercentage);
        AddVehicleReply reply =  simulator.addVehicle(builder.build());

        if (reply.getVehicleID().equals("")) {
            logger.error("Failed to create vehicle.");
            throw new NoVehicleCreatedException();
        } else {
            logger.info("Created vehicle " + reply.getVehicleID());
        }
    }

    public void addVehicle(
            String id, long source, long target, String rmiHost,
            int rmiPort, String vehicleModel, boolean useModelica) throws NoVehicleCreatedException {
        AddVehicleRequest.Builder builder = getAddVehicleRequestBuilder(
                id, source, target, rmiHost, rmiPort, vehicleModel, useModelica);
        AddVehicleReply reply =  simulator.addVehicle(builder.build());

        if (reply.getVehicleID().equals("")) {
            logger.error("Failed to create vehicle.");
            throw new NoVehicleCreatedException();
        } else {
            logger.info("Created vehicle " + reply.getVehicleID());
        }
    }

    private AddVehicleRequest.Builder getAddVehicleRequestBuilder(
            String id, long source, long target, String rmiHost,
            int rmiPort, String vehicleModel, boolean useModelica) {
        return AddVehicleRequest.newBuilder()
                .setVehicleID(id)
                .setSourceOsmID(String.valueOf(source))
                .setTargetOsmID(String.valueOf(target))
                .setRmiHost(rmiHost)
                .setRmiPort(rmiPort)
                .setVehicleModel(vehicleModel)
                .setUseModelica(useModelica);
    }

    /**
     * Update vehicle's trajectory on the fly
     *
     * @param id vehicle id
     * @param source
     * @param target
     */
    public void updateVehicleTrajectory(String id, long source, long target){
        UpdateTrajectoryRequest req = UpdateTrajectoryRequest.newBuilder()
                .setVehicleID(id)
                .setSourceOsmID(String.valueOf(source))
                .setTargetOsmID(String.valueOf(target))
                .build();
        simulator.updateTrajectory(req);
    }

    /**
     * Start the simulation for given milliseconds
     * @param durationMs
     */
    public void start(int durationMs, int totalDuration){
        StartSimulationRequest req = StartSimulationRequest.newBuilder().setSimulationID(simulationID).setDuration(durationMs).setTotalDuration(totalDuration).build();
        simulator.startSimulation(req);
    }

    /**
     * Retrieve the simulation result from the simulator. The results are list of vehicles, which contains
     * their planned trajectory, and simulation result for each frame.
     *
     * @return
     */
    public List<VehicleModel> getResult(){
        List<VehicleModel> result = new ArrayList<>();
        GetSimulationResultReply reply = simulator.getSimulationResult(GetSimulationResultRequest.newBuilder().build());

        for(VehicleSimulationDataFrame vframe: reply.getVehiclesList()){
            VehicleModel vm = new VehicleModel();
            List<Dataframe> frames = new ArrayList<>();

            for (SimulationDataFrame f: vframe.getFramesList()){
                double[][] rotation = new double[f.getRotationCount()][f.getRotation(0).getValueCount()];
                for (int i = 0; i < f.getRotationCount(); i++) {
                    rotation[i] = f.getRotation(i).getValueList().stream().mapToDouble(Double::doubleValue).toArray();
                }
                frames.add(new Dataframe(){{
                    setPosX(f.getPosX());
                    setPosY(f.getPosY());
                    setPosZ(f.getPosZ());

                    setSteering(f.getSteering());
                    setEngine(f.getEngine());
                    setBrake(f.getBrake());
                    setRotation(rotation);
                    setVelocity(f.getVelocity().getValueList().stream().mapToDouble(Double::doubleValue).toArray());
                    setAcceleration(f.getAcceleration().getValueList().stream().mapToDouble(Double::doubleValue).toArray());
                    setBattery(f.getBattery());
                    setCharging(f.getIsCharging());

                    setDeltaTime(f.getDeltaTime());
                    setTotalTime(f.getTotalTime());

                    boolean[] errorFlags = new boolean[ErrorFlag.values().length];
                    List<Boolean> valueList = f.getErrorFlags().getValueList();
                    for (int i = 0; i < valueList.size(); i++) {
                        errorFlags[i] = valueList.get(i);
                    }
                    setErrorFlags(errorFlags);
                }});
            }

            vm.setId(vframe.getVehicleID());
            vm.setFrames(frames);
            vm.setDistanceToTargetInMeter(getVehicleByID(vframe.getVehicleID()).getDistanceToTargetInMeter());

            result.add(vm);
        }
        return result;
    }

    /**
     * Get the result of a specific vehicle
     * @param id vehicle id
     * @return
     */
    public VehicleModel getResultByVehicleID(String id){
        List<VehicleModel> results = getResult();
        for (VehicleModel vehicle: results){
            if (vehicle.getId().equals(id)){
                return vehicle;
            }
        }
        return null;
    }

    /**
     * Get vehicles that the simulator is simulating
     * @return
     */
    public List<VehicleModel> getVehicles(){
        List<VehicleModel> result = new ArrayList<>();
        GetVehiclesReply vehicles = simulator.getVehicles(GetVehiclesRequest.newBuilder().build());

        for (GetVehiclesReply.VehicleState v: vehicles.getStatesList()){
            result.add(new VehicleModel(){{
                setId(v.getId());
                setDistanceToTargetInMeter(v.getDistToTargetInMeters());
            }});
        }
        return result;
    }

    /**
     * Get a specific vehicle
     *
     * @param id
     * @return
     */
    public VehicleModel getVehicleByID(String id){
        for(VehicleModel v: getVehicles()){
            if (v.getId().equals(id)){
                return v;
            }
        }
        return null;
    }

    /**
     * Get map data of the map which the simulator is simulating. The response contains data for streets, buildings and
     * bounds of the map.
     *
     * @return
     */
    public MapModel getMap(){
        MapModel result = new MapModel();
        GetMapResponse resp = simulator.getMap(Empty.newBuilder().build());

        List<MapModel.Street> streets = new ArrayList<>();
        for (GetMapResponse.Street street: resp.getStreetsList()){
            List<MapModel.Node> nodes = extractNodesFromProtobuf(street.getNodesList());
            streets.add(new MapModel.Street(
                    street.getId(),
                    street.getWidth(),
                    extractNodesFromProtobuf(street.getNodesList()),
                    extractNodesFromProtobuf(street.getIntersectionsList()),
                    street.getStreetType(),
                    street.getIsOneWay(),
                    street.getStreetPavements(),
                    street.getSpeedLimit()
            ));
        }
        result.setStreets(streets);

        List<MapModel.Building> buildings = new ArrayList<>();
        for (GetMapResponse.Building building: resp.getBuildingsList()){
            // TODO add building id
            buildings.add(new MapModel.Building(
                    extractNodesFromProtobuf(building.getNodesList()), ""));
        }
        result.setBuildings(buildings);

        result.setBounds(new MapModel.Bound(
                resp.getBound().getMinX(),
                resp.getBound().getMinY(),
                resp.getBound().getMinZ(),
                resp.getBound().getMaxX(),
                resp.getBound().getMaxY(),
                resp.getBound().getMaxZ()
        ));

        return result;
    }

    /**
     * A helper function that convert nodes of type GetMapResponse.OsmNode into MapModel.Node
     * @param nodes
     * @return same node of type MapModel.Node
     */
    private List<MapModel.Node> extractNodesFromProtobuf(List<GetMapResponse.OsmNode> nodes){
        List<MapModel.Node> result = new ArrayList<>();
        for(GetMapResponse.OsmNode node: nodes){
            result.add(new MapModel.Node(
                    node.getId(),
                    node.getLonitude(),
                    node.getLatitude(),
                    node.getAltitude(),
                    null
            ));
        }
        return result;
    }

    /**
     * Remove vehicle from the simulator
     * @param id
     */
    public void removeVehicle(String id){
        simulator.removeVehicle(RemoveVehicleRequest.newBuilder().setId(id).build());
    }

    class NoVehicleCreatedException extends Exception {

    }
}
