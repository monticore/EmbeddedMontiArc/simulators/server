/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.restful;

import de.monticore.lang.montisim.simlang.SimLangTool;
import de.monticore.lang.montisim.simlang.adapter.SimLangContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.web.bind.annotation.RestController;
import server.restful.view.CarView;
import server.restful.view.MapView;
import server.restful.view.ScenarioView;
import server.restful.view.SimulationView;

import java.nio.file.Paths;


@SpringBootApplication
@Import({MapView.class, ScenarioView.class, SimulationView.class, CarView.class})
@RestController
public class Server {

    public static void main(String[] args) {
        try {
            SpringApplication.run(Server.class, args);
        }catch (Exception e){
            System.out.println(e);
        }
    }
}
