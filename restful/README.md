<!-- (c) https://github.com/MontiCore/monticore -->
This is the simulation server module. It provides a set of RESTful APIs for setting up, running simulation and also
retrieving the results.

## Install
Install using maven:
```
$ mvn clean install --settings=../settings.xml -DskipTests
```
Run the server:
```
$ java -jar ./target/restful.jar
```

It is also fine to start the server using your IDE, the main class is located at `src/main/java/server/restful/Server.java`.

## Simulation
To start a simulation, the following services must be started at first:
- ZooKeeper
- RMIModelServer
- RPC server

The RMIModelServer and RPC server depends on ZooKeeper, so before starting them, make
sure ZooKeeper is running.

The following instructions enables the server to run a simulation task
with arbitrary maps and scenarios. 

### ZooKeeper
Checkout the installation guide [here](https://zookeeper.apache.org/doc/r3.1.2/zookeeperStarted.html#ch_GettingStarted) and 
[here](https://medium.com/@shaaslam/installing-apache-zookeeper-on-windows-45eda303e835).    
After installation, start the ZooKeeper server. By default, it should be listening at **localhost:2181**. 
Linux users can install and start the ZooKeeper server with:
```bash
$ apt-get install zookeeper
$ sudo bash /usr/share/zookeeper/bin/zkServer.sh start
```
Mac users:
```bash
$ brew install zookeeper
$ brew services start zookeeper
```
Windows users should install the ZooKeeper server manually, which takes a few more steps. Please checkout 
the installation guide above.

### RMIModelServer
Please refer to [RMIModelServer](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/simulators/RMIModelServer) for installation guide.    
After installation, start at least one rmiserver By default, it should be listening at **localhost:10101**.

### RPC Server
Go to the rpc module for installation guid.
Start at least one RPC server at **localhost:6000**.

There are some simple scenarios and maps for testing purpose
available in directory `restful/src/test/resources`.

### METIS
METIS is a graph partitioning software. It is necessary for the simulation to run
in multiple sub-simulators.
 Without METIS the simulation is also available, but only with `numSectors=1`.
 
Install METIS in linux:
```bash
$ apt-get install -y metis
``` 
Install METIS on Mac:
```bash
$ brew install metis
``` 

### Windows 
Windows users should set the flag of `windows_dev_mode` to `true` in the config file.

## Configuration
The configuration file are located at `restful/src/main/resources/config.properties`.
Several options are available in there.

## Docker
Since the server depends on multiple services, it is a good choice to run them all in docker
containers. First built docker images with `rpc/build.sh` and `restful/build.sh` and then
edit the last line in `dev.yml` to add path to your local visualization files. 
You can comment out the nginx service if you don't need visualization, 
but in that case, please also uncomment line 49 of `dev.yml`
Finally, start the project with: 
```
$ docker-compose -f dev.yml up 
```

To scale specific services:
```
$ docker-compose -f dev.yml up --scale rmi-server=10 rmi-server
$ docker-compose -f dev.yml up --scale rpc-server=3 rpc-server
```

## Developing with Docker
The development always involves debugging, with docker, debugging has to be done via remote debugger.
`dev.yml` already configured the restful server to run in debug mode, the JVM listens for debugging request
at **localhost:8787**. 
To debug restful server, create a remote debugger in the IDE and start debugging.

The development workflow with should be:
 1. code
 2. compile
 3. use `dev.yml` to start and scale necessary services
 4. start a remote debugger to debug the server
 5. goto 1
 
If you need to debug the rpc server, add a new service into `dev.yml`:
```yaml
rpc-server-debugger:
  image: rpc-server
  depends_on:
    - zoo1
  ports:
    - 8788:8787
  restart: always
  environment:
    ZOO_SERVERS: zoo1:2181
  entrypoint:
    - java
    - -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=8787
    - -cp
    - "simulator-rpc.jar:."
    - server.rpc.RpcServer
    - "6000"
``` 
It starts a rpc server that listen on port **8788** for debugging.

## Visualization
By default, visualization is not enabled in the server. 
To enable visualization, put the static files under `restful/src/main/resources/resources`.

Aa an example, to use the visualization provided in [visualization repo](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/simulators/visualization),
copy everything from [webapp directory](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/simulators/visualization/tree/master/src/main/webapp) into
`restful/src/main/resources/resources`.
Start the restful server, the visualization client will be available at [http://127.0.0.1:8090/index.html](http://127.0.0.1:8090/index.html)

It is highly recommended for frontend developers to use reverse proxy to serve
the frontend files. Because otherwise, you need to recompile the restful module 
every time you changed something in your code. For example, to use nginx as reverse proxy, 
we provided a configuration(`nginx.conf`) for that. 
You should 
   1. update `root /app` to point it to your static files, such as `root /path/to/visualization/src/webapp`
   2. update `proxy_pass http://server:8090;` to fit your case. If you started the restful server with `java -jar ...jar` command,
   it should be `proxy_pass http://127.0.0.1:8090;`
After you installed nginx, start it with this config file.

If you use docker, you only need to update the last line of `dev.yml` in order to 
use nginx.

## Simulation via RESTful API
After the server started, simulation tasks can be configured via the RESTful APIs.
The sequence should be:
1. Upload the map with `POST /map`
2. Upload the scenario file with `POST /scenario`
3. Upload car files if needed with `POST /car`
4. Create simulation with `POST /simulation`
5. Start the simulation with `POST /simulation/{id}/start`
6. Check the simulation status with `GET /simulation/{id}/status`
7. Once finished, retrieve all results with `GET /simulation/{id}/result`
8. Individual sector map data can downloaded with `GET /simulation/{id}/map-data/{sectorIdx}`. The minimal sector index is 0, maximal is *num_sectors* - 1.

## API Documentation
The APIs are defined using OpenAPI2.0 in `openapi.yml`. 
However, the restful server also provides an auto generated API documentation page at
[http://127.0.0.1:8090/swagger-ui.html](http://127.0.0.1:8090/swagger-ui.html).

## Tips
### Scenarios and Maps
There are couple of scenarios and maps that you can play with. They are located
in `restful/src/test/resources`. Maps have `.osm` suffix, `.sim` suffix are for scenarios.

### Editing scenario files
`https://www.openstreetmap.org/node/[node id]` will help you with finding source and target coordinates for vehicles easily.
