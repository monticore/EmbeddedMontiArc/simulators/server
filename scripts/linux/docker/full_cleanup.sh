#!/usr/bin/env bash
# (c) https://github.com/MontiCore/monticore  
SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
#Import print()
. $SCRIPTS_DIR/../print.sh
NETWORK_NAME="simulation-network"

. $SCRIPTS_DIR/cleanup.sh

print "Full Cleanup"

print "Cleaning up images"
if docker image ls | grep -q montisim-server-builder; then docker rmi montisim-server-builder; fi
if docker image ls | grep -q rpc-server; then docker rmi rpc-server; fi
if docker image ls | grep -q restful-server; then docker rmi restful-server; fi

print "Stopping all docker containers"
docker stop $(docker ps -a -q)
print "Removing all docker images"
docker rm $(docker ps -a -q)
print "Pruning docker"
docker system prune -f
