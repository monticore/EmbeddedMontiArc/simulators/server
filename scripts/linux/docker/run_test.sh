#!/usr/bin/env bash
#
# (c) https://github.com/MontiCore/monticore
#
# The license generally applicable for this project
# can be found under https://github.com/MontiCore/monticore.
#
SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ROOT_DIR=$SCRIPTS_DIR/../../..
#Import print()
. $SCRIPTS_DIR/../print.sh

NETWORK_NAME="simulation-network"

# this script serves the purpose of running unitetests in gitlab-runner.
# It starts all dependent services needed before starting the tests.
# Including:
#   - a zookeeper server at localhost:2181


. $SCRIPTS_DIR/cleanup.sh

print "Creating docker network"
docker network create $NETWORK_NAME

print "Starting zookeeper"
docker run \
    --network="$NETWORK_NAME" \
    -d \
    --name=zookeeper \
    -e "ZOO_MAX_CLIENT_CNXNS=1024" \
    zookeeper:latest

print "Waiting for zookeeper to start..."
sleep 5

#docker build -t server-test-runner -f ./docker/TestDockerfile .
print "Running Unit Tests"
docker run \
    --rm --network="$NETWORK_NAME" \
    -e "ZOO_SERVERS=zookeeper" \
    montisim-server-builder \
    bash -c "source ~/.bashrc && pushd rpc && mvn test -s ../settings.xml && popd && cd restful && mvn test -ntp -Dtest=\!IntegrationTest* -s ../settings.xml"

code=$?

if [ "$code" = "0" ]; then
    print "Unit Tests successful"
else
    print "Error Performing Unit Tests (code: $code)"
    print "Zookeeper Log"
    docker logs -t zookeeper
fi

print "Stopping zookeeper"
docker stop zookeeper

. $SCRIPTS_DIR/cleanup.sh

exit $code
