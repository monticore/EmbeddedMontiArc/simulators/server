#!/usr/bin/env bash
#
# (c) https://github.com/MontiCore/monticore
#
# The license generally applicable for this project
# can be found under https://github.com/MontiCore/monticore.
#
SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ROOT_DIR=$SCRIPTS_DIR/../../..
#Import print()
. $SCRIPTS_DIR/../print.sh

NETWORK_NAME="simulation-network"

. $SCRIPTS_DIR/cleanup.sh

# this script serves the purpose of running integration test in gitlab-runner.
# It starts all dependent services needed before starting the tests.
# Including:
#   - a remote simulator at rpc-server:6000
#   - an autopilot at autopilot:10101
#   - a zookeeper server at zookeeper:2181

print "Creating docker network"
docker network create $NETWORK_NAME

print "Starting zookeeper"
docker run \
    --network="$NETWORK_NAME" \
    -d \
    --name=zookeeper \
    -e "ZOO_MAX_CLIENT_CNXNS=1024" \
    zookeeper:latest


print "Waiting for zookeeper to start..."
sleep 10


print "Starting autopilot server (RMIServer)"
docker run \
    --network="$NETWORK_NAME" \
    --name=autopilot \
    -d \
    -e "ZOO_SERVERS=zookeeper" \
    registry.git.rwth-aachen.de/monticore/embeddedmontiarc/simulators/rmimodelserver:latest \
    bash -c "source ~/.bashrc && java -Djava.rmi.server.useLocalHostname -jar rmi-model-server.jar port=10101 softwares_folder=softwares"

print "Starting RPC-server"
docker run \
    --network="$NETWORK_NAME" \
    -d \
    -e "ZOO_SERVERS=zookeeper" \
    --name=rpc-server \
    rpc-server:latest


echo "waiting for autopilot and simulator to start..."
sleep 5


pushd ${ROOT_DIR}
#docker build -t server-test-runner -f ./docker/TestDockerfile .
print "Running Integration Tests"
docker run \
    --network="$NETWORK_NAME" \
    -e "ZOO_SERVERS=zookeeper" \
    montisim-server-builder \
    bash -c "cd restful && mvn test -ntp -Dtest=IntegrationTest -s ../settings.xml"

code=$?

if [ "$code" = "0" ]; then
    print "Integration Test Successful"
else
    print "Error Performing Integration Tests (code: $code)"
    print "Zookeeper Log"
    docker logs -t zookeeper
    print "Autopilot Log"
    docker logs -t autopilot
    print "RPC-server Log"
    docker logs -t rpc-server
fi

print "Stopping rpc-server"
docker stop rpc-server
print "Stopping autopilot"
docker stop autopilot
print "Stopping zookeeper"
docker stop zookeeper

. $SCRIPTS_DIR/cleanup.sh

exit $code
