#!/usr/bin/env bash
#
# (c) https://github.com/MontiCore/monticore
#
# The license generally applicable for this project
# can be found under https://github.com/MontiCore/monticore.
#
SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ROOT_DIR=$SCRIPTS_DIR/../../..
#Import print()
. $SCRIPTS_DIR/../print.sh

pushd $ROOT_DIR/rpc
print "Building RPC docker image..."
docker build -f "${ROOT_DIR}/docker/Dockerfile_rpc" -t rpc-server .
popd

