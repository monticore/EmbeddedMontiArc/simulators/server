
# (c) https://github.com/MontiCore/monticore  

SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ROOT_DIR=$SCRIPTS_DIR/../../..
#Import print()
. $SCRIPTS_DIR/../print.sh

print "Docker CI script"

$SCRIPTS_DIR/full_cleanup.sh

docker pull registry.git.rwth-aachen.de/monticore/embeddedmontiarc/simulators/rmimodelserver:latest

$SCRIPTS_DIR/build_server.sh
$SCRIPTS_DIR/run_test.sh
$SCRIPTS_DIR/run_integration_test.sh
