#!/usr/bin/env bash
# (c) https://github.com/MontiCore/monticore  
CLEANUP_SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
. $CLEANUP_SCRIPT_DIR/../print.sh

print "Cleaning up containers and network"
if docker container ls -a | grep -q zookeeper; then docker rm -f zookeeper; fi
if docker container ls -a | grep -q autopilot; then docker rm -f autopilot; fi
if docker container ls -a | grep -q rpc-server; then docker rm -f rpc-server; fi
if docker network ls | grep -q $NETWORK_NAME; then docker network rm $NETWORK_NAME; fi
