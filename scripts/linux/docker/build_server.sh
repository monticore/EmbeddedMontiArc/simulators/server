#!/usr/bin/env bash
# (c) https://github.com/MontiCore/monticore  

SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ROOT_DIR=$SCRIPTS_DIR/../../..
#Import print()
. $SCRIPTS_DIR/../print.sh

pushd $ROOT_DIR
print "Building montisim_server_builder docker image..."
docker build -f "${ROOT_DIR}/docker/Dockerfile_montisim_server_builder" -t montisim-server-builder .
popd

$SCRIPTS_DIR/build_rpc.sh
$SCRIPTS_DIR/build_restful.sh
