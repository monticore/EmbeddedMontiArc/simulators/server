@rem (c) https://github.com/MontiCore/monticore  
@echo off
set SCRIPTS_DIR=%~dp0
set ROOT_DIR=%SCRIPTS_DIR%\..\..\..

pushd %ROOT_DIR%
docker build -f "%ROOT_DIR%\docker\Dockerfile_montisim_server_builder" -t montisim-server-builder .
popd

call %SCRIPTS_DIR%\build_rpc.bat
call %SCRIPTS_DIR%\build_restful.bat
