@rem (c) https://github.com/MontiCore/monticore  
for /f %%i in ('docker container ls -a ^| findstr /c:zookeeper') do docker rm -f zookeeper
for /f %%i in ('docker container ls -a ^| findstr /c:autopilot') do docker rm -f autopilot
for /f %%i in ('docker container ls -a ^| findstr /c:rpc-server') do docker rm -f rpc-server
for /f %%i in ('docker network ls ^| findstr /c:%NETWORK_NAME%') do docker network rm %NETWORK_NAME%
