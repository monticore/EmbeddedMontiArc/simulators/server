@rem (c) https://github.com/MontiCore/monticore  
:: TODO simplify / comment output
:: @echo off
set SCRIPTS_DIR=%~dp0
set ROOT_DIR=%SCRIPTS_DIR%\..\..\..

set SIMULATOR_DIR=%ROOT_DIR%\rpc
set NETWORK_NAME="simulation-network"

call %SCRIPTS_DIR%\cleanup.bat

docker network create %NETWORK_NAME%

docker run ^
    --network=%NETWORK_NAME% ^
    --name=zookeeper ^
    -d ^
    -e "ZOO_MAX_CLIENT_CNXNS=1024" ^
    zookeeper:latest

echo Waiting for zookeeper to start...
timeout /t 10

::    registry.git.rwth-aachen.de/monticore/embeddedmontiarc/simulators/rmimodelserver:latest
:: sh -c "java -Djava.rmi.server.useLocalHostname -jar rmi-model-server.jar port=10101 softwares_folder=softwares"
docker run ^
    --network=%NETWORK_NAME% ^
    --name=autopilot ^
    -d ^
    -e "ZOO_SERVERS=zookeeper" ^
    rmi-server ^
    bash -c "source ~/.bashrc && java -Djava.rmi.server.useLocalHostname -jar rmi-model-server.jar port=10101 softwares_folder=softwares"

docker run ^
    --network=%NETWORK_NAME% ^
    --name=rpc-server ^
    -d ^
    -e "ZOO_SERVERS=zookeeper" ^
    rpc-server:latest

echo Waiting for autopilot and simulator to start...
timeout /t 5

docker run ^
    --rm --network=%NETWORK_NAME% ^
    -e "ZOO_SERVERS=zookeeper" ^
    montisim-server-builder sh -c "cd restful && mvn test -Dtest=IntegrationTest -s ../settings.xml"

set FAILURE=%errorlevel%
if %errorlevel% neq 0 (
    echo Error Integration Testing
)
::call %SCRIPTS_DIR%\cleanup.bat
exit /b %FAILURE%
