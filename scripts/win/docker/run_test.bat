@rem (c) https://github.com/MontiCore/monticore  
:: TODO simplify / comment output
@echo off
set SCRIPTS_DIR=%~dp0
set ROOT_DIR=%SCRIPTS_DIR%\..\..\..

set NETWORK_NAME="simulation-network"

call %SCRIPTS_DIR%\cleanup.bat

docker network create %NETWORK_NAME%

docker run ^
    --network=%NETWORK_NAME% ^
    -d ^
    --name=zookeeper ^
    -e "ZOO_MAX_CLIENT_CNXNS=1024" ^
    zookeeper:latest

docker run ^
    --rm --network=%NETWORK_NAME% ^
    -e "ZOO_SERVERS=zookeeper" ^
    montisim-server-builder ^
    bash -c "source ~/.bashrc && pushd rpc && mvn test -s ../settings.xml && popd && cd restful && mvn test -Dtest=\!IntegrationTest* -s ../settings.xml"


set FAILURE=%errorlevel%
if %errorlevel% neq 0 (
    echo Error Unit Testing
)
call %SCRIPTS_DIR%\cleanup.bat
exit /b %FAILURE%
