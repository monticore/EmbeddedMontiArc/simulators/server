@rem (c) https://github.com/MontiCore/monticore  
@echo off
set SCRIPTS_DIR=%~dp0
set ROOT_DIR=%SCRIPTS_DIR%\..\..

pushd %ROOT_DIR%\rpc
java -cp "target/rpc-jar-with-dependencies.jar" server.rpc.RpcServer 6000
popd
