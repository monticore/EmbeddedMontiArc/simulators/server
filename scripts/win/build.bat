@rem (c) https://github.com/MontiCore/monticore  
@echo off
set SCRIPTS_DIR=%~dp0
set ROOT_DIR=%SCRIPTS_DIR%\..\..

pushd %ROOT_DIR%
mvn clean install -DskipTests -s settings.xml
popd
