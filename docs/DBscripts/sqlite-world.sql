--
-- (c) https://github.com/MontiCore/monticore
--
-- The license generally applicable for this project
-- can be found under https://github.com/MontiCore/monticore.
--

CREATE TABLE area (
    id integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    minx double precision NOT NULL,
    miny double precision NOT NULL,
    minz double precision NOT NULL,
    maxx double precision NOT NULL,
    maxy double precision NOT NULL,
    maxz double precision NOT NULL
);


CREATE TABLE entry_node (
    sector_id bigint NOT NULL,
    node_osm_id bigint NOT NULL,
    PRIMARY KEY (sector_id, node_osm_id)
);


CREATE TABLE env_node (
    id integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    x double precision NOT NULL,
    y double precision NOT NULL,
    z double precision NOT NULL,
    osmid bigint NOT NULL,
    env_object_id integer NOT NULL,
    isintersection smallint NOT NULL
);


CREATE TABLE env_object (
    id integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    tag character varying(255) NOT NULL,
    osmid bigint NOT NULL,
    map_id integer NOT NULL
);


CREATE TABLE map (
    id integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    mname character varying(255) NOT NULL,
    src character varying(255) NOT NULL
);

CREATE TABLE height_map (
    id integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    col integer NOT NULL,
    row integer NOT NULL,
    height double precision NOT NULL,
    map_id integer NOT NULL
);

CREATE TABLE height_info (
    id integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    deltaX double precision NOT NULL,
    deltaY double precision NOT NULL,
    min_point_x double precision NOT NULL,
    min_point_y double precision NOT NULL,
    max_point_x double precision NOT NULL,
    max_point_y double precision NOT NULL,
    map_id integer NOT NULL
);

CREATE TABLE overlap_area (
    id integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    overlap_a integer NOT NULL,
    overlap_b integer NOT NULL,
    area_id integer NOT NULL
);


CREATE TABLE path (
    sector_id bigint NOT NULL,
    source bigint NOT NULL,
    target bigint NOT NULL,
    distance double precision,
    PRIMARY KEY (sector_id, source, target)
);


CREATE TABLE scenario (
    id integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    sname character varying(255) NOT NULL,
    map_id integer NOT NULL
);


CREATE TABLE sector (
    id integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    map_id integer NOT NULL,
    area_id integer NOT NULL
);


CREATE TABLE street (
    id integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    speed_limit integer NOT NULL,
    width double precision NOT NULL,
    isoneway smallint NOT NULL,
    env_object_id integer NOT NULL,
    street_type character varying(255),
    street_pavement character varying(255)
);

CREATE TABLE waterway (
    id integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    width double precision NOT NULL,
    env_object_id integer NOT NULL
);


CREATE TABLE street_sign (
    id integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    ss_type character varying(255) NOT NULL,
    ss_state character varying(255) NOT NULL,
    isone smallint NOT NULL,
    istwo smallint NOT NULL,
    x1 double precision NOT NULL,
    y1 double precision NOT NULL,
    z1 double precision NOT NULL,
    x2 double precision NOT NULL,
    y2 double precision NOT NULL,
    z2 double precision NOT NULL,
    env_node_id integer NOT NULL
);


CREATE TABLE track (
    id integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    tname character varying(50) NOT NULL,
    scenario_id integer NOT NULL
);


CREATE TABLE track_section (
    id integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    track_id bigint,
    source bigint NOT NULL,
    target bigint NOT NULL,
    sector_id bigint NOT NULL
);


CREATE TABLE users (
    id integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    uname character varying(255) NOT NULL,
    passwd character varying(255) NOT NULL
);
