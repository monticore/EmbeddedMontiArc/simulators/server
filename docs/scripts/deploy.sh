#!/bin/bash
#
# (c) https://github.com/MontiCore/monticore
#
# The license generally applicable for this project
# can be found under https://github.com/MontiCore/monticore.
#


CONFIG_DIR="main/resources"
CONFIG_FILE="config.properties"
SQLITE_DB="docs/DBscripts/world_linux.db"

SFS_DIR="/path/to/smartfox/SFS2X"
SFS_EXT_DIR="$SFS_DIR/extensions/AllInOne"

#Copy jar to server directory
cp -f $CONFIG_DIR/$CONFIG_FILE $SFS_EXT_DIR/$CONFIG_FILE
cp $CONFIG_DIR/aachen.osm $SFS_EXT_DIR/

#Copy SQLite database to smartfox extension directory
cp -nf $SQLITE_DB $SFS_EXT_DIR/world.db

#Change permissions for all extensions
chmod -R +x $SFS_EXT_DIR

echo Extensions deployed \(Platform Unix\)
