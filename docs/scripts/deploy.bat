@REM
@REM (c) https://github.com/MontiCore/monticore
@REM
@REM The license generally applicable for this project
@REM can be found under https://github.com/MontiCore/monticore.
@REM

REM Batch script

SET CONFIG_DIR=main\resources
SET CONFIG_FILE=config.properties
SET SQLITE_DB_DIR=docs\DBscripts
SET SQLITE_DB_FILE=world.db

SET SFS_DIR=C:\SmartFoxServer_2X\SFS2X
SET SFS_EXT_DIR=%SFS_DIR%\extensions\AllInOne

copy %CONFIG_DIR%\%CONFIG_FILE% %SFS_EXT_DIR%\%CONFIG_FILE%
copy %SQLITE_DB_DIR%\%SQLITE_DB_FILE% %SFS_EXT_DIR%\%SQLITE_DB_FILE%

copy ..\simulation\lib\InputFilter.fmu %SFS_DIR%\lib\InputFilter.fmu
copy ..\simulation\lib\Chassis.fmu %SFS_DIR%\lib\Chassis.fmu
copy ..\simulation\lib\Suspension.fmu %SFS_DIR%\lib\Suspension.fmu
copy ..\simulation\lib\Tires.fmu %SFS_DIR%\lib\Tires.fmu

ECHO Extensions deployed \(Platform windows\)
