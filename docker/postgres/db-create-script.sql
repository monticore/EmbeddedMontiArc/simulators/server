--
-- (c) https://github.com/MontiCore/monticore
--
-- The license generally applicable for this project
-- can be found under https://github.com/MontiCore/monticore.
--

--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: area; Type: TABLE; Schema: public; Owner: server; Tablespace: 
--

CREATE TABLE area (
    id integer NOT NULL,
    minx double precision NOT NULL,
    miny double precision NOT NULL,
    minz double precision NOT NULL,
    maxx double precision NOT NULL,
    maxy double precision NOT NULL,
    maxz double precision NOT NULL
);


ALTER TABLE area OWNER TO server;

--
-- Name: area_id_seq; Type: SEQUENCE; Schema: public; Owner: server
--

CREATE SEQUENCE area_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE area_id_seq OWNER TO server;

--
-- Name: area_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: server
--

ALTER SEQUENCE area_id_seq OWNED BY area.id;


--
-- Name: entry_node; Type: TABLE; Schema: public; Owner: server; Tablespace: 
--

CREATE TABLE entry_node (
    sector_id bigint NOT NULL,
    node_osm_id bigint NOT NULL
);


ALTER TABLE entry_node OWNER TO server;

--
-- Name: env_node; Type: TABLE; Schema: public; Owner: server; Tablespace: 
--

CREATE TABLE env_node (
    id integer NOT NULL,
    x double precision NOT NULL,
    y double precision NOT NULL,
    z double precision NOT NULL,
    osmid bigint NOT NULL,
    env_object_id integer NOT NULL,
    isintersection smallint NOT NULL
);


ALTER TABLE env_node OWNER TO server;

--
-- Name: env_node_id_seq; Type: SEQUENCE; Schema: public; Owner: server
--

CREATE SEQUENCE env_node_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE env_node_id_seq OWNER TO server;

--
-- Name: env_node_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: server
--

ALTER SEQUENCE env_node_id_seq OWNED BY env_node.id;


--
-- Name: env_object; Type: TABLE; Schema: public; Owner: server; Tablespace: 
--

CREATE TABLE env_object (
    id integer NOT NULL,
    tag character varying(255) NOT NULL,
    osmid bigint NOT NULL,
    map_id integer NOT NULL
);


ALTER TABLE env_object OWNER TO server;

--
-- Name: env_object_id_seq; Type: SEQUENCE; Schema: public; Owner: server
--

CREATE SEQUENCE env_object_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE env_object_id_seq OWNER TO server;

--
-- Name: env_object_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: server
--

ALTER SEQUENCE env_object_id_seq OWNED BY env_object.id;


--
-- Name: map; Type: TABLE; Schema: public; Owner: server; Tablespace: 
--

CREATE TABLE map (
    id integer NOT NULL,
    mname character varying(255) NOT NULL,
    src character varying(255) NOT NULL
);


ALTER TABLE map OWNER TO server;

--
-- Name: map_id_seq; Type: SEQUENCE; Schema: public; Owner: server
--

CREATE SEQUENCE map_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE map_id_seq OWNER TO server;

--
-- Name: map_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: server
--

ALTER SEQUENCE map_id_seq OWNED BY map.id;


--
-- Name: overlap_area; Type: TABLE; Schema: public; Owner: server; Tablespace: 
--

CREATE TABLE overlap_area (
    id integer NOT NULL,
    overlap_a integer NOT NULL,
    overlap_b integer NOT NULL,
    area_id integer NOT NULL
);


ALTER TABLE overlap_area OWNER TO server;

--
-- Name: overlap_area_id_seq; Type: SEQUENCE; Schema: public; Owner: server
--

CREATE SEQUENCE overlap_area_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE overlap_area_id_seq OWNER TO server;

--
-- Name: overlap_area_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: server
--

ALTER SEQUENCE overlap_area_id_seq OWNED BY overlap_area.id;


--
-- Name: path; Type: TABLE; Schema: public; Owner: server; Tablespace: 
--

CREATE TABLE path (
    sector_id bigint NOT NULL,
    source bigint NOT NULL,
    target bigint NOT NULL,
    distance double precision
);


ALTER TABLE path OWNER TO server;

--
-- Name: scenario; Type: TABLE; Schema: public; Owner: server; Tablespace: 
--

CREATE TABLE scenario (
    id integer NOT NULL,
    sname character varying(255) NOT NULL,
    map_id integer NOT NULL
);


ALTER TABLE scenario OWNER TO server;

--
-- Name: scenario_id_seq; Type: SEQUENCE; Schema: public; Owner: server
--

CREATE SEQUENCE scenario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE scenario_id_seq OWNER TO server;

--
-- Name: scenario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: server
--

ALTER SEQUENCE scenario_id_seq OWNED BY scenario.id;


--
-- Name: sector; Type: TABLE; Schema: public; Owner: server; Tablespace: 
--

CREATE TABLE sector (
    id integer NOT NULL,
    map_id integer NOT NULL,
    area_id integer NOT NULL
);


ALTER TABLE sector OWNER TO server;

--
-- Name: sector_id_seq; Type: SEQUENCE; Schema: public; Owner: server
--

CREATE SEQUENCE sector_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sector_id_seq OWNER TO server;

--
-- Name: sector_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: server
--

ALTER SEQUENCE sector_id_seq OWNED BY sector.id;


--
-- Name: street; Type: TABLE; Schema: public; Owner: server; Tablespace: 
--

CREATE TABLE street (
    id integer NOT NULL,
    speed_limit integer NOT NULL,
    width double precision NOT NULL,
    isoneway smallint NOT NULL,
    env_object_id integer NOT NULL,
    street_type character varying(255)
);


ALTER TABLE street OWNER TO server;

--
-- Name: street_id_seq; Type: SEQUENCE; Schema: public; Owner: server
--

CREATE SEQUENCE street_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE street_id_seq OWNER TO server;

--
-- Name: street_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: server
--

ALTER SEQUENCE street_id_seq OWNED BY street.id;


--
-- Name: street_sign; Type: TABLE; Schema: public; Owner: server; Tablespace: 
--

CREATE TABLE street_sign (
    id integer NOT NULL,
    ss_type character varying(255) NOT NULL,
    ss_state character varying(255) NOT NULL,
    isone smallint NOT NULL,
    istwo smallint NOT NULL,
    x1 double precision NOT NULL,
    y1 double precision NOT NULL,
    z1 double precision NOT NULL,
    x2 double precision NOT NULL,
    y2 double precision NOT NULL,
    z2 double precision NOT NULL,
    env_node_id integer NOT NULL
);


ALTER TABLE street_sign OWNER TO server;

--
-- Name: street_sign_id_seq; Type: SEQUENCE; Schema: public; Owner: server
--

CREATE SEQUENCE street_sign_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE street_sign_id_seq OWNER TO server;

--
-- Name: street_sign_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: server
--

ALTER SEQUENCE street_sign_id_seq OWNED BY street_sign.id;


--
-- Name: track; Type: TABLE; Schema: public; Owner: server; Tablespace: 
--

CREATE TABLE track (
    id integer NOT NULL,
    tname character varying(50) NOT NULL,
    scenario_id integer NOT NULL
);


ALTER TABLE track OWNER TO server;

--
-- Name: track_id_seq; Type: SEQUENCE; Schema: public; Owner: server
--

CREATE SEQUENCE track_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE track_id_seq OWNER TO server;

--
-- Name: track_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: server
--

ALTER SEQUENCE track_id_seq OWNED BY track.id;


--
-- Name: track_section; Type: TABLE; Schema: public; Owner: server; Tablespace: 
--

CREATE TABLE track_section (
    id integer NOT NULL,
    track_id bigint,
    source bigint NOT NULL,
    target bigint NOT NULL,
    sector_id bigint NOT NULL
);


ALTER TABLE track_section OWNER TO server;

--
-- Name: track_section_id_seq; Type: SEQUENCE; Schema: public; Owner: server
--

CREATE SEQUENCE track_section_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE track_section_id_seq OWNER TO server;

--
-- Name: track_section_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: server
--

ALTER SEQUENCE track_section_id_seq OWNED BY track_section.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: server; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    uname character varying(255) NOT NULL,
    passwd character varying(255) NOT NULL
);


ALTER TABLE users OWNER TO server;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: server
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO server;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: server
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: server
--

ALTER TABLE ONLY area ALTER COLUMN id SET DEFAULT nextval('area_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: server
--

ALTER TABLE ONLY env_node ALTER COLUMN id SET DEFAULT nextval('env_node_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: server
--

ALTER TABLE ONLY env_object ALTER COLUMN id SET DEFAULT nextval('env_object_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: server
--

ALTER TABLE ONLY map ALTER COLUMN id SET DEFAULT nextval('map_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: server
--

ALTER TABLE ONLY overlap_area ALTER COLUMN id SET DEFAULT nextval('overlap_area_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: server
--

ALTER TABLE ONLY scenario ALTER COLUMN id SET DEFAULT nextval('scenario_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: server
--

ALTER TABLE ONLY sector ALTER COLUMN id SET DEFAULT nextval('sector_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: server
--

ALTER TABLE ONLY street ALTER COLUMN id SET DEFAULT nextval('street_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: server
--

ALTER TABLE ONLY street_sign ALTER COLUMN id SET DEFAULT nextval('street_sign_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: server
--

ALTER TABLE ONLY track ALTER COLUMN id SET DEFAULT nextval('track_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: server
--

ALTER TABLE ONLY track_section ALTER COLUMN id SET DEFAULT nextval('track_section_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: server
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: area_pkey; Type: CONSTRAINT; Schema: public; Owner: server; Tablespace: 
--

ALTER TABLE ONLY area
    ADD CONSTRAINT area_pkey PRIMARY KEY (id);


--
-- Name: entry_node_pkey; Type: CONSTRAINT; Schema: public; Owner: server; Tablespace: 
--

ALTER TABLE ONLY entry_node
    ADD CONSTRAINT entry_node_pkey PRIMARY KEY (sector_id, node_osm_id);


--
-- Name: env_node_pkey; Type: CONSTRAINT; Schema: public; Owner: server; Tablespace: 
--

ALTER TABLE ONLY env_node
    ADD CONSTRAINT env_node_pkey PRIMARY KEY (id);


--
-- Name: env_object_pkey; Type: CONSTRAINT; Schema: public; Owner: server; Tablespace: 
--

ALTER TABLE ONLY env_object
    ADD CONSTRAINT env_object_pkey PRIMARY KEY (id);


--
-- Name: map_mname_key; Type: CONSTRAINT; Schema: public; Owner: server; Tablespace: 
--

ALTER TABLE ONLY map
    ADD CONSTRAINT map_mname_key UNIQUE (mname);


--
-- Name: map_pkey; Type: CONSTRAINT; Schema: public; Owner: server; Tablespace: 
--

ALTER TABLE ONLY map
    ADD CONSTRAINT map_pkey PRIMARY KEY (id);


--
-- Name: overlap_area_pkey; Type: CONSTRAINT; Schema: public; Owner: server; Tablespace: 
--

ALTER TABLE ONLY overlap_area
    ADD CONSTRAINT overlap_area_pkey PRIMARY KEY (id);


--
-- Name: path_pkey; Type: CONSTRAINT; Schema: public; Owner: server; Tablespace: 
--

ALTER TABLE ONLY path
    ADD CONSTRAINT path_pkey PRIMARY KEY (sector_id, source, target);


--
-- Name: scenario_pkey; Type: CONSTRAINT; Schema: public; Owner: server; Tablespace: 
--

ALTER TABLE ONLY scenario
    ADD CONSTRAINT scenario_pkey PRIMARY KEY (id);


--
-- Name: sector_pkey; Type: CONSTRAINT; Schema: public; Owner: server; Tablespace: 
--

ALTER TABLE ONLY sector
    ADD CONSTRAINT sector_pkey PRIMARY KEY (id);


--
-- Name: street_pkey; Type: CONSTRAINT; Schema: public; Owner: server; Tablespace: 
--

ALTER TABLE ONLY street
    ADD CONSTRAINT street_pkey PRIMARY KEY (id);


--
-- Name: street_sign_pkey; Type: CONSTRAINT; Schema: public; Owner: server; Tablespace: 
--

ALTER TABLE ONLY street_sign
    ADD CONSTRAINT street_sign_pkey PRIMARY KEY (id);


--
-- Name: track_pkey; Type: CONSTRAINT; Schema: public; Owner: server; Tablespace: 
--

ALTER TABLE ONLY track
    ADD CONSTRAINT track_pkey PRIMARY KEY (id);


--
-- Name: track_section_pkey; Type: CONSTRAINT; Schema: public; Owner: server; Tablespace: 
--

ALTER TABLE ONLY track_section
    ADD CONSTRAINT track_section_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: server; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users_uname_key; Type: CONSTRAINT; Schema: public; Owner: server; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_uname_key UNIQUE (uname);


--
-- Name: map_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: server
--

ALTER TABLE ONLY env_object
    ADD CONSTRAINT map_id_fk FOREIGN KEY (map_id) REFERENCES map(id) MATCH FULL;


--
-- Name: map_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: server
--

ALTER TABLE ONLY sector
    ADD CONSTRAINT map_id_fk FOREIGN KEY (map_id) REFERENCES map(id) MATCH FULL;


--
-- Name: node_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: server
--

ALTER TABLE ONLY street_sign
    ADD CONSTRAINT node_id_fk FOREIGN KEY (env_node_id) REFERENCES env_node(id) MATCH FULL;


--
-- Name: obj_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: server
--

ALTER TABLE ONLY env_node
    ADD CONSTRAINT obj_id_fk FOREIGN KEY (env_object_id) REFERENCES env_object(id) MATCH FULL;


--
-- Name: obj_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: server
--

ALTER TABLE ONLY street
    ADD CONSTRAINT obj_id_fk FOREIGN KEY (env_object_id) REFERENCES env_object(id) MATCH FULL;


--
-- Name: overlap_area_area_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: server
--

ALTER TABLE ONLY overlap_area
    ADD CONSTRAINT overlap_area_area_id_fkey FOREIGN KEY (area_id) REFERENCES area(id);


--
-- Name: overlap_area_overla_b_fkey; Type: FK CONSTRAINT; Schema: public; Owner: server
--

ALTER TABLE ONLY overlap_area
    ADD CONSTRAINT overlap_area_overla_b_fkey FOREIGN KEY (overlap_b) REFERENCES sector(id);


--
-- Name: overlap_area_overlap_a_fkey; Type: FK CONSTRAINT; Schema: public; Owner: server
--

ALTER TABLE ONLY overlap_area
    ADD CONSTRAINT overlap_area_overlap_a_fkey FOREIGN KEY (overlap_a) REFERENCES sector(id);


--
-- Name: scenario_map_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: server
--

ALTER TABLE ONLY scenario
    ADD CONSTRAINT scenario_map_id_fkey FOREIGN KEY (map_id) REFERENCES map(id);


--
-- Name: sector_area_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: server
--

ALTER TABLE ONLY sector
    ADD CONSTRAINT sector_area_id_fkey FOREIGN KEY (area_id) REFERENCES area(id);


--
-- Name: track_scenario_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: server
--

ALTER TABLE ONLY track
    ADD CONSTRAINT track_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES scenario(id);


--
-- Name: track_section_sector_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: server
--

ALTER TABLE ONLY track_section
    ADD CONSTRAINT track_section_sector_id_fkey FOREIGN KEY (sector_id) REFERENCES sector(id);


--
-- Name: track_section_track_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: server
--

ALTER TABLE ONLY track_section
    ADD CONSTRAINT track_section_track_id_fkey FOREIGN KEY (track_id) REFERENCES track(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

