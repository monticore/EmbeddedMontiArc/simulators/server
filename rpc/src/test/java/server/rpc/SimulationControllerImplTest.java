/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.rpc;

import de.monticore.lang.montisim.carlang.CarContainer;
import de.monticore.lang.montisim.carlang.CarLangTool;
import de.monticore.lang.montisim.carlang._symboltable.CarSymbol;
import de.rwth.monticore.EmbeddedMontiArc.simulators.hardware_emulator.interfaces.SoftwareSimulatorManager;
import de.rwth.monticore.EmbeddedMontiArc.simulators.hardware_emulator.DirectSoftwareSimulatorManager;
import de.rwth.monticore.EmbeddedMontiArc.simulators.hardware_emulator.config.SoftwareSimulatorConfig;
import io.grpc.inprocess.InProcessChannelBuilder;
import io.grpc.inprocess.InProcessServerBuilder;
import io.grpc.testing.GrpcCleanupRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;
import protobuf.*;

import java.io.IOException;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.HashMap;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class SimulationControllerImplTest {
    private SimControllerGrpc.SimControllerBlockingStub blockingStub;
    private SimulationControllerImpl controller;
    private String mapPath = getClass().getResource("/straight.osm").getPath();
    private CarContainer defaultCarContainer;

    @Before
    public void setUp() throws Exception {
        String serverName = InProcessServerBuilder.generateName();

        //SoftwareSimulatorManager connectSoftwareSimulatorManager(String host, int port)
        DirectSoftwareSimulatorManager manager = new DirectSoftwareSimulatorManager(new SoftwareSimulatorConfig().set_softwares_folder("src/test/resources/softwares"));
        controller = Mockito.spy(new SimulationControllerImpl());
        Mockito.doReturn(manager).when(controller).connectSoftwareSimulatorManager(Mockito.anyString(), Mockito.anyInt());

        // Create a server, add service, start, and register for automatic graceful shutdown.
        grpcCleanup.register(InProcessServerBuilder
                .forName(serverName).directExecutor().addService(controller).build().start());
        blockingStub = SimControllerGrpc.newBlockingStub(
                // Create a client channel and register for automatic graceful shutdown.
                grpcCleanup.register(InProcessChannelBuilder.forName(serverName).directExecutor().build()));
        blockingStub.resetSimulator(ResetSimulatorRequest.newBuilder().build());

        Optional<CarSymbol> carSymbol = CarLangTool.parse(Paths.get("src/test/resources"), "default");
        defaultCarContainer = carSymbol.get().getCarContainer();
    }

    @Rule
    public final GrpcCleanupRule grpcCleanup = new GrpcCleanupRule();

    @Test
    public void simulation() throws IOException {
        controller.setMap(mapPath);

        blockingStub.addVehicle(AddVehicleRequest.newBuilder()
                .setVehicleID("idid")
                .setSourceOsmID("897050130")
                .setTargetOsmID("897050130")
                .setVehicleModel(defaultCarContainer.toJson())
                .setUseModelica(false)
                .build());
        blockingStub.startSimulation(StartSimulationRequest.newBuilder().setDuration(1000).setTotalDuration(1000).setSimulationID("0").build());

        // check if vehicle has been created and has UUID
        GetVehiclesReply vehicleReply = blockingStub.getVehicles(GetVehiclesRequest.newBuilder().build());
        assertEquals(4, vehicleReply.getStates(0).getId().length());
        assertEquals(1, vehicleReply.getStatesList().size());

        // check if there is any simulation result(dataframes)
        GetSimulationResultReply simReply = blockingStub.getSimulationResult(GetSimulationResultRequest.newBuilder().build());
        assertEquals(1, simReply.getVehiclesCount());
        assertTrue(simReply.getVehicles(0).getFramesCount() > 0);

        // test remove vehicle
        assertEquals(1, vehicleReply.getStatesList().size());
        blockingStub.removeVehicle(RemoveVehicleRequest.newBuilder().setId(vehicleReply.getStates(0).getId()).build());
        vehicleReply = blockingStub.getVehicles(GetVehiclesRequest.newBuilder().build());
        assertEquals(0, vehicleReply.getStatesList().size());
    }

    @Test
    public void globalId() {
        controller.setMap(mapPath);

        blockingStub.addVehicle(AddVehicleRequest.newBuilder()
                .setSourceOsmID("897050130")
                .setTargetOsmID("897050130")
                .setVehicleModel(defaultCarContainer.toJson())
                .setUseModelica(false)
                .build());
        GetVehiclesReply vehicleReply = blockingStub.getVehicles(GetVehiclesRequest.newBuilder().build());
        // no vehicle should be created, because its id was not set.
        assertEquals(0, vehicleReply.getStatesCount());

        String expectedId = "oh-my-id";
        blockingStub.addVehicle(AddVehicleRequest.newBuilder()
                .setVehicleID(expectedId)
                .setSourceOsmID("897050130")
                .setTargetOsmID("897050130")
                .setVehicleModel(defaultCarContainer.toJson())
                .setUseModelica(false)
                .build());
        vehicleReply = blockingStub.getVehicles(GetVehiclesRequest.newBuilder().build());
        assertEquals(expectedId, vehicleReply.getStates(0).getId());
    }

    @Test
    public void simulationID() throws IOException {
        StartSimulationReply reply = blockingStub.startSimulation(StartSimulationRequest.newBuilder().setSimulationID("simID").build());
        assertEquals("simID", reply.getSimulationID());

        // check if simulation has been assigned a UUID
        reply = blockingStub.startSimulation(StartSimulationRequest.newBuilder().build());
        assertEquals(36, reply.getSimulationID().length());
    }


    @Test
    public void pauseContinueSimulation() throws IOException {
        controller.setMap(mapPath);

        blockingStub.addVehicle(AddVehicleRequest.newBuilder()
                .setSourceOsmID("897050130")
                .setTargetOsmID("897050130")
                .setVehicleID("vid")
                .setVehicleModel(defaultCarContainer.toJson())
                .build());

        // simulate 1000ms in virtual world
        blockingStub.startSimulation(StartSimulationRequest.newBuilder().setDuration(1000).setTotalDuration(2000).build());
        GetSimulationResultReply simReply = blockingStub.getSimulationResult(GetSimulationResultRequest.newBuilder().build());
        int numFrames = (simReply.getVehicles(0).getFramesCount());

        // simulate another 1000ms in virtual world and check if there are new dataframes generated
        blockingStub.startSimulation(StartSimulationRequest.newBuilder().setDuration(1000).build());
        simReply = blockingStub.getSimulationResult(GetSimulationResultRequest.newBuilder().build());
        assertTrue(numFrames < simReply.getVehicles(0).getFramesCount());
    }

    @Test
    public void distToTargetInMeters() throws IOException {
        controller.setMap(mapPath);

        blockingStub.addVehicle(AddVehicleRequest.newBuilder()
                .setSourceOsmID("41958919")
                .setTargetOsmID("897050130")
                .setVehicleID("vid")
                .setVehicleModel(defaultCarContainer.toJson())
                .build());

        float distToTargetInMeters = Float.MAX_VALUE;
        for (int i = 0; i < 1; i++) {
            blockingStub.startSimulation(StartSimulationRequest.newBuilder().setDuration(1000).build());
            GetVehiclesReply vehicles = blockingStub.getVehicles(GetVehiclesRequest.newBuilder().build());
            float currDist = vehicles.getStates(0).getDistToTargetInMeters();
            assertTrue(distToTargetInMeters > currDist);
            distToTargetInMeters = currDist;
        }
    }

    @Test
    public void vehicleStop() throws IOException {
        controller.setMap(mapPath);

        blockingStub.addVehicle(AddVehicleRequest.newBuilder()
                .setSourceOsmID("5170132488")
                .setTargetOsmID("5170132492")
                .setVehicleID("vid")
                .setVehicleModel(defaultCarContainer.toJson())
                .build());

        float distToTargetInMeters = Float.MAX_VALUE;
        float currDist;

        // test if vehicle is able to stop
        while (true) {
            blockingStub.startSimulation(StartSimulationRequest.newBuilder().setDuration(1000).build());
            GetVehiclesReply vehicles = blockingStub.getVehicles(GetVehiclesRequest.newBuilder().build());
            currDist = vehicles.getStates(0).getDistToTargetInMeters();
            if (currDist == distToTargetInMeters) {
                break;
            }
            distToTargetInMeters = currDist;
        }
    }

    @Test
    public void getMap() {
        controller.setMap(mapPath);
        GetMapResponse resp = blockingStub.getMap(Empty.newBuilder().build());
        assertTrue(resp.getStreetsCount() > 0);
        assertTrue(resp.getBuildingsCount() > 0);
        for (GetMapResponse.Street street : resp.getStreetsList()) {
            assertTrue(street.getStreetPavements().length() > 0);
        }
    }
}
