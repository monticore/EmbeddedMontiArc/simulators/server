/* (c) https://github.com/MontiCore/monticore */
// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: simulationController.proto

package protobuf;

public interface AddVehicleRequestOrBuilder extends
    // @@protoc_insertion_point(interface_extends:SimulationController.AddVehicleRequest)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>string sourceOsmID = 1;</code>
   */
  java.lang.String getSourceOsmID();
  /**
   * <code>string sourceOsmID = 1;</code>
   */
  com.google.protobuf.ByteString
      getSourceOsmIDBytes();

  /**
   * <code>string targetOsmID = 2;</code>
   */
  java.lang.String getTargetOsmID();
  /**
   * <code>string targetOsmID = 2;</code>
   */
  com.google.protobuf.ByteString
      getTargetOsmIDBytes();

  /**
   * <code>string rmiHost = 3;</code>
   */
  java.lang.String getRmiHost();
  /**
   * <code>string rmiHost = 3;</code>
   */
  com.google.protobuf.ByteString
      getRmiHostBytes();

  /**
   * <code>int64 rmiPort = 4;</code>
   */
  long getRmiPort();

  /**
   * <code>string vehicleModel = 5;</code>
   */
  java.lang.String getVehicleModel();
  /**
   * <code>string vehicleModel = 5;</code>
   */
  com.google.protobuf.ByteString
      getVehicleModelBytes();

  /**
   * <code>bool useModelica = 6;</code>
   */
  boolean getUseModelica();

  /**
   * <code>double batteryCapacity = 7;</code>
   */
  double getBatteryCapacity();

  /**
   * <code>double batteryPercentage = 8;</code>
   */
  double getBatteryPercentage();

  /**
   * <code>string vehicleID = 9;</code>
   */
  java.lang.String getVehicleID();
  /**
   * <code>string vehicleID = 9;</code>
   */
  com.google.protobuf.ByteString
      getVehicleIDBytes();
}
