/* (c) https://github.com/MontiCore/monticore */
// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: simulationController.proto

package protobuf;

public interface PlannedTrajectoryOrBuilder extends
    // @@protoc_insertion_point(interface_extends:SimulationController.PlannedTrajectory)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>string osmID = 1;</code>
   */
  java.lang.String getOsmID();
  /**
   * <code>string osmID = 1;</code>
   */
  com.google.protobuf.ByteString
      getOsmIDBytes();

  /**
   * <code>float posX = 2;</code>
   */
  float getPosX();

  /**
   * <code>float posY = 3;</code>
   */
  float getPosY();

  /**
   * <code>float posZ = 4;</code>
   */
  float getPosZ();
}
