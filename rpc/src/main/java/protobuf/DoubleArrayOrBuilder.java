/* (c) https://github.com/MontiCore/monticore */
// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: simulationController.proto

package protobuf;

public interface DoubleArrayOrBuilder extends
    // @@protoc_insertion_point(interface_extends:SimulationController.DoubleArray)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>repeated double value = 1 [packed = true];</code>
   */
  java.util.List<java.lang.Double> getValueList();
  /**
   * <code>repeated double value = 1 [packed = true];</code>
   */
  int getValueCount();
  /**
   * <code>repeated double value = 1 [packed = true];</code>
   */
  double getValue(int index);
}
