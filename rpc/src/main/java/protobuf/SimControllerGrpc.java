/* (c) https://github.com/MontiCore/monticore */
package protobuf;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.16.1)",
    comments = "Source: simulationController.proto")
public final class SimControllerGrpc {

  private SimControllerGrpc() {}

  public static final String SERVICE_NAME = "SimulationController.SimController";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<protobuf.SetMapRequest,
      protobuf.Empty> getSetMapMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "SetMap",
      requestType = protobuf.SetMapRequest.class,
      responseType = protobuf.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<protobuf.SetMapRequest,
      protobuf.Empty> getSetMapMethod() {
    io.grpc.MethodDescriptor<protobuf.SetMapRequest, protobuf.Empty> getSetMapMethod;
    if ((getSetMapMethod = SimControllerGrpc.getSetMapMethod) == null) {
      synchronized (SimControllerGrpc.class) {
        if ((getSetMapMethod = SimControllerGrpc.getSetMapMethod) == null) {
          SimControllerGrpc.getSetMapMethod = getSetMapMethod = 
              io.grpc.MethodDescriptor.<protobuf.SetMapRequest, protobuf.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "SimulationController.SimController", "SetMap"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  protobuf.SetMapRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  protobuf.Empty.getDefaultInstance()))
                  .setSchemaDescriptor(new SimControllerMethodDescriptorSupplier("SetMap"))
                  .build();
          }
        }
     }
     return getSetMapMethod;
  }

  private static volatile io.grpc.MethodDescriptor<protobuf.SetSectorRequest,
      protobuf.Empty> getSetSectorMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "SetSector",
      requestType = protobuf.SetSectorRequest.class,
      responseType = protobuf.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<protobuf.SetSectorRequest,
      protobuf.Empty> getSetSectorMethod() {
    io.grpc.MethodDescriptor<protobuf.SetSectorRequest, protobuf.Empty> getSetSectorMethod;
    if ((getSetSectorMethod = SimControllerGrpc.getSetSectorMethod) == null) {
      synchronized (SimControllerGrpc.class) {
        if ((getSetSectorMethod = SimControllerGrpc.getSetSectorMethod) == null) {
          SimControllerGrpc.getSetSectorMethod = getSetSectorMethod = 
              io.grpc.MethodDescriptor.<protobuf.SetSectorRequest, protobuf.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "SimulationController.SimController", "SetSector"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  protobuf.SetSectorRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  protobuf.Empty.getDefaultInstance()))
                  .setSchemaDescriptor(new SimControllerMethodDescriptorSupplier("SetSector"))
                  .build();
          }
        }
     }
     return getSetSectorMethod;
  }

  private static volatile io.grpc.MethodDescriptor<protobuf.ConfigureRMIRequest,
      protobuf.Empty> getConfigureRMIMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "configureRMI",
      requestType = protobuf.ConfigureRMIRequest.class,
      responseType = protobuf.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<protobuf.ConfigureRMIRequest,
      protobuf.Empty> getConfigureRMIMethod() {
    io.grpc.MethodDescriptor<protobuf.ConfigureRMIRequest, protobuf.Empty> getConfigureRMIMethod;
    if ((getConfigureRMIMethod = SimControllerGrpc.getConfigureRMIMethod) == null) {
      synchronized (SimControllerGrpc.class) {
        if ((getConfigureRMIMethod = SimControllerGrpc.getConfigureRMIMethod) == null) {
          SimControllerGrpc.getConfigureRMIMethod = getConfigureRMIMethod = 
              io.grpc.MethodDescriptor.<protobuf.ConfigureRMIRequest, protobuf.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "SimulationController.SimController", "configureRMI"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  protobuf.ConfigureRMIRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  protobuf.Empty.getDefaultInstance()))
                  .setSchemaDescriptor(new SimControllerMethodDescriptorSupplier("configureRMI"))
                  .build();
          }
        }
     }
     return getConfigureRMIMethod;
  }

  private static volatile io.grpc.MethodDescriptor<protobuf.AddVehicleRequest,
      protobuf.AddVehicleReply> getAddVehicleMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "AddVehicle",
      requestType = protobuf.AddVehicleRequest.class,
      responseType = protobuf.AddVehicleReply.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<protobuf.AddVehicleRequest,
      protobuf.AddVehicleReply> getAddVehicleMethod() {
    io.grpc.MethodDescriptor<protobuf.AddVehicleRequest, protobuf.AddVehicleReply> getAddVehicleMethod;
    if ((getAddVehicleMethod = SimControllerGrpc.getAddVehicleMethod) == null) {
      synchronized (SimControllerGrpc.class) {
        if ((getAddVehicleMethod = SimControllerGrpc.getAddVehicleMethod) == null) {
          SimControllerGrpc.getAddVehicleMethod = getAddVehicleMethod = 
              io.grpc.MethodDescriptor.<protobuf.AddVehicleRequest, protobuf.AddVehicleReply>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "SimulationController.SimController", "AddVehicle"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  protobuf.AddVehicleRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  protobuf.AddVehicleReply.getDefaultInstance()))
                  .setSchemaDescriptor(new SimControllerMethodDescriptorSupplier("AddVehicle"))
                  .build();
          }
        }
     }
     return getAddVehicleMethod;
  }

  private static volatile io.grpc.MethodDescriptor<protobuf.StartSimulationRequest,
      protobuf.StartSimulationReply> getStartSimulationMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "StartSimulation",
      requestType = protobuf.StartSimulationRequest.class,
      responseType = protobuf.StartSimulationReply.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<protobuf.StartSimulationRequest,
      protobuf.StartSimulationReply> getStartSimulationMethod() {
    io.grpc.MethodDescriptor<protobuf.StartSimulationRequest, protobuf.StartSimulationReply> getStartSimulationMethod;
    if ((getStartSimulationMethod = SimControllerGrpc.getStartSimulationMethod) == null) {
      synchronized (SimControllerGrpc.class) {
        if ((getStartSimulationMethod = SimControllerGrpc.getStartSimulationMethod) == null) {
          SimControllerGrpc.getStartSimulationMethod = getStartSimulationMethod = 
              io.grpc.MethodDescriptor.<protobuf.StartSimulationRequest, protobuf.StartSimulationReply>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "SimulationController.SimController", "StartSimulation"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  protobuf.StartSimulationRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  protobuf.StartSimulationReply.getDefaultInstance()))
                  .setSchemaDescriptor(new SimControllerMethodDescriptorSupplier("StartSimulation"))
                  .build();
          }
        }
     }
     return getStartSimulationMethod;
  }

  private static volatile io.grpc.MethodDescriptor<protobuf.GetVehiclesRequest,
      protobuf.GetVehiclesReply> getGetVehiclesMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetVehicles",
      requestType = protobuf.GetVehiclesRequest.class,
      responseType = protobuf.GetVehiclesReply.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<protobuf.GetVehiclesRequest,
      protobuf.GetVehiclesReply> getGetVehiclesMethod() {
    io.grpc.MethodDescriptor<protobuf.GetVehiclesRequest, protobuf.GetVehiclesReply> getGetVehiclesMethod;
    if ((getGetVehiclesMethod = SimControllerGrpc.getGetVehiclesMethod) == null) {
      synchronized (SimControllerGrpc.class) {
        if ((getGetVehiclesMethod = SimControllerGrpc.getGetVehiclesMethod) == null) {
          SimControllerGrpc.getGetVehiclesMethod = getGetVehiclesMethod = 
              io.grpc.MethodDescriptor.<protobuf.GetVehiclesRequest, protobuf.GetVehiclesReply>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "SimulationController.SimController", "GetVehicles"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  protobuf.GetVehiclesRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  protobuf.GetVehiclesReply.getDefaultInstance()))
                  .setSchemaDescriptor(new SimControllerMethodDescriptorSupplier("GetVehicles"))
                  .build();
          }
        }
     }
     return getGetVehiclesMethod;
  }

  private static volatile io.grpc.MethodDescriptor<protobuf.GetSimulationResultRequest,
      protobuf.GetSimulationResultReply> getGetSimulationResultMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetSimulationResult",
      requestType = protobuf.GetSimulationResultRequest.class,
      responseType = protobuf.GetSimulationResultReply.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<protobuf.GetSimulationResultRequest,
      protobuf.GetSimulationResultReply> getGetSimulationResultMethod() {
    io.grpc.MethodDescriptor<protobuf.GetSimulationResultRequest, protobuf.GetSimulationResultReply> getGetSimulationResultMethod;
    if ((getGetSimulationResultMethod = SimControllerGrpc.getGetSimulationResultMethod) == null) {
      synchronized (SimControllerGrpc.class) {
        if ((getGetSimulationResultMethod = SimControllerGrpc.getGetSimulationResultMethod) == null) {
          SimControllerGrpc.getGetSimulationResultMethod = getGetSimulationResultMethod = 
              io.grpc.MethodDescriptor.<protobuf.GetSimulationResultRequest, protobuf.GetSimulationResultReply>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "SimulationController.SimController", "GetSimulationResult"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  protobuf.GetSimulationResultRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  protobuf.GetSimulationResultReply.getDefaultInstance()))
                  .setSchemaDescriptor(new SimControllerMethodDescriptorSupplier("GetSimulationResult"))
                  .build();
          }
        }
     }
     return getGetSimulationResultMethod;
  }

  private static volatile io.grpc.MethodDescriptor<protobuf.Empty,
      protobuf.GetMapResponse> getGetMapMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetMap",
      requestType = protobuf.Empty.class,
      responseType = protobuf.GetMapResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<protobuf.Empty,
      protobuf.GetMapResponse> getGetMapMethod() {
    io.grpc.MethodDescriptor<protobuf.Empty, protobuf.GetMapResponse> getGetMapMethod;
    if ((getGetMapMethod = SimControllerGrpc.getGetMapMethod) == null) {
      synchronized (SimControllerGrpc.class) {
        if ((getGetMapMethod = SimControllerGrpc.getGetMapMethod) == null) {
          SimControllerGrpc.getGetMapMethod = getGetMapMethod = 
              io.grpc.MethodDescriptor.<protobuf.Empty, protobuf.GetMapResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "SimulationController.SimController", "GetMap"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  protobuf.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  protobuf.GetMapResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new SimControllerMethodDescriptorSupplier("GetMap"))
                  .build();
          }
        }
     }
     return getGetMapMethod;
  }

  private static volatile io.grpc.MethodDescriptor<protobuf.RemoveVehicleRequest,
      protobuf.Empty> getRemoveVehicleMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "RemoveVehicle",
      requestType = protobuf.RemoveVehicleRequest.class,
      responseType = protobuf.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<protobuf.RemoveVehicleRequest,
      protobuf.Empty> getRemoveVehicleMethod() {
    io.grpc.MethodDescriptor<protobuf.RemoveVehicleRequest, protobuf.Empty> getRemoveVehicleMethod;
    if ((getRemoveVehicleMethod = SimControllerGrpc.getRemoveVehicleMethod) == null) {
      synchronized (SimControllerGrpc.class) {
        if ((getRemoveVehicleMethod = SimControllerGrpc.getRemoveVehicleMethod) == null) {
          SimControllerGrpc.getRemoveVehicleMethod = getRemoveVehicleMethod = 
              io.grpc.MethodDescriptor.<protobuf.RemoveVehicleRequest, protobuf.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "SimulationController.SimController", "RemoveVehicle"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  protobuf.RemoveVehicleRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  protobuf.Empty.getDefaultInstance()))
                  .setSchemaDescriptor(new SimControllerMethodDescriptorSupplier("RemoveVehicle"))
                  .build();
          }
        }
     }
     return getRemoveVehicleMethod;
  }

  private static volatile io.grpc.MethodDescriptor<protobuf.ResetSimulatorRequest,
      protobuf.Empty> getResetSimulatorMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ResetSimulator",
      requestType = protobuf.ResetSimulatorRequest.class,
      responseType = protobuf.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<protobuf.ResetSimulatorRequest,
      protobuf.Empty> getResetSimulatorMethod() {
    io.grpc.MethodDescriptor<protobuf.ResetSimulatorRequest, protobuf.Empty> getResetSimulatorMethod;
    if ((getResetSimulatorMethod = SimControllerGrpc.getResetSimulatorMethod) == null) {
      synchronized (SimControllerGrpc.class) {
        if ((getResetSimulatorMethod = SimControllerGrpc.getResetSimulatorMethod) == null) {
          SimControllerGrpc.getResetSimulatorMethod = getResetSimulatorMethod = 
              io.grpc.MethodDescriptor.<protobuf.ResetSimulatorRequest, protobuf.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "SimulationController.SimController", "ResetSimulator"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  protobuf.ResetSimulatorRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  protobuf.Empty.getDefaultInstance()))
                  .setSchemaDescriptor(new SimControllerMethodDescriptorSupplier("ResetSimulator"))
                  .build();
          }
        }
     }
     return getResetSimulatorMethod;
  }

  private static volatile io.grpc.MethodDescriptor<protobuf.UpdateTrajectoryRequest,
      protobuf.Empty> getUpdateTrajectoryMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "UpdateTrajectory",
      requestType = protobuf.UpdateTrajectoryRequest.class,
      responseType = protobuf.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<protobuf.UpdateTrajectoryRequest,
      protobuf.Empty> getUpdateTrajectoryMethod() {
    io.grpc.MethodDescriptor<protobuf.UpdateTrajectoryRequest, protobuf.Empty> getUpdateTrajectoryMethod;
    if ((getUpdateTrajectoryMethod = SimControllerGrpc.getUpdateTrajectoryMethod) == null) {
      synchronized (SimControllerGrpc.class) {
        if ((getUpdateTrajectoryMethod = SimControllerGrpc.getUpdateTrajectoryMethod) == null) {
          SimControllerGrpc.getUpdateTrajectoryMethod = getUpdateTrajectoryMethod = 
              io.grpc.MethodDescriptor.<protobuf.UpdateTrajectoryRequest, protobuf.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "SimulationController.SimController", "UpdateTrajectory"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  protobuf.UpdateTrajectoryRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  protobuf.Empty.getDefaultInstance()))
                  .setSchemaDescriptor(new SimControllerMethodDescriptorSupplier("UpdateTrajectory"))
                  .build();
          }
        }
     }
     return getUpdateTrajectoryMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static SimControllerStub newStub(io.grpc.Channel channel) {
    return new SimControllerStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static SimControllerBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new SimControllerBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static SimControllerFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new SimControllerFutureStub(channel);
  }

  /**
   */
  public static abstract class SimControllerImplBase implements io.grpc.BindableService {

    /**
     */
    public void setMap(protobuf.SetMapRequest request,
        io.grpc.stub.StreamObserver<protobuf.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getSetMapMethod(), responseObserver);
    }

    /**
     */
    public void setSector(protobuf.SetSectorRequest request,
        io.grpc.stub.StreamObserver<protobuf.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getSetSectorMethod(), responseObserver);
    }

    /**
     */
    public void configureRMI(protobuf.ConfigureRMIRequest request,
        io.grpc.stub.StreamObserver<protobuf.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getConfigureRMIMethod(), responseObserver);
    }

    /**
     */
    public void addVehicle(protobuf.AddVehicleRequest request,
        io.grpc.stub.StreamObserver<protobuf.AddVehicleReply> responseObserver) {
      asyncUnimplementedUnaryCall(getAddVehicleMethod(), responseObserver);
    }

    /**
     */
    public void startSimulation(protobuf.StartSimulationRequest request,
        io.grpc.stub.StreamObserver<protobuf.StartSimulationReply> responseObserver) {
      asyncUnimplementedUnaryCall(getStartSimulationMethod(), responseObserver);
    }

    /**
     */
    public void getVehicles(protobuf.GetVehiclesRequest request,
        io.grpc.stub.StreamObserver<protobuf.GetVehiclesReply> responseObserver) {
      asyncUnimplementedUnaryCall(getGetVehiclesMethod(), responseObserver);
    }

    /**
     */
    public void getSimulationResult(protobuf.GetSimulationResultRequest request,
        io.grpc.stub.StreamObserver<protobuf.GetSimulationResultReply> responseObserver) {
      asyncUnimplementedUnaryCall(getGetSimulationResultMethod(), responseObserver);
    }

    /**
     */
    public void getMap(protobuf.Empty request,
        io.grpc.stub.StreamObserver<protobuf.GetMapResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getGetMapMethod(), responseObserver);
    }

    /**
     */
    public void removeVehicle(protobuf.RemoveVehicleRequest request,
        io.grpc.stub.StreamObserver<protobuf.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getRemoveVehicleMethod(), responseObserver);
    }

    /**
     */
    public void resetSimulator(protobuf.ResetSimulatorRequest request,
        io.grpc.stub.StreamObserver<protobuf.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getResetSimulatorMethod(), responseObserver);
    }

    /**
     */
    public void updateTrajectory(protobuf.UpdateTrajectoryRequest request,
        io.grpc.stub.StreamObserver<protobuf.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getUpdateTrajectoryMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getSetMapMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                protobuf.SetMapRequest,
                protobuf.Empty>(
                  this, METHODID_SET_MAP)))
          .addMethod(
            getSetSectorMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                protobuf.SetSectorRequest,
                protobuf.Empty>(
                  this, METHODID_SET_SECTOR)))
          .addMethod(
            getConfigureRMIMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                protobuf.ConfigureRMIRequest,
                protobuf.Empty>(
                  this, METHODID_CONFIGURE_RMI)))
          .addMethod(
            getAddVehicleMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                protobuf.AddVehicleRequest,
                protobuf.AddVehicleReply>(
                  this, METHODID_ADD_VEHICLE)))
          .addMethod(
            getStartSimulationMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                protobuf.StartSimulationRequest,
                protobuf.StartSimulationReply>(
                  this, METHODID_START_SIMULATION)))
          .addMethod(
            getGetVehiclesMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                protobuf.GetVehiclesRequest,
                protobuf.GetVehiclesReply>(
                  this, METHODID_GET_VEHICLES)))
          .addMethod(
            getGetSimulationResultMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                protobuf.GetSimulationResultRequest,
                protobuf.GetSimulationResultReply>(
                  this, METHODID_GET_SIMULATION_RESULT)))
          .addMethod(
            getGetMapMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                protobuf.Empty,
                protobuf.GetMapResponse>(
                  this, METHODID_GET_MAP)))
          .addMethod(
            getRemoveVehicleMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                protobuf.RemoveVehicleRequest,
                protobuf.Empty>(
                  this, METHODID_REMOVE_VEHICLE)))
          .addMethod(
            getResetSimulatorMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                protobuf.ResetSimulatorRequest,
                protobuf.Empty>(
                  this, METHODID_RESET_SIMULATOR)))
          .addMethod(
            getUpdateTrajectoryMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                protobuf.UpdateTrajectoryRequest,
                protobuf.Empty>(
                  this, METHODID_UPDATE_TRAJECTORY)))
          .build();
    }
  }

  /**
   */
  public static final class SimControllerStub extends io.grpc.stub.AbstractStub<SimControllerStub> {
    private SimControllerStub(io.grpc.Channel channel) {
      super(channel);
    }

    private SimControllerStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SimControllerStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new SimControllerStub(channel, callOptions);
    }

    /**
     */
    public void setMap(protobuf.SetMapRequest request,
        io.grpc.stub.StreamObserver<protobuf.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getSetMapMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void setSector(protobuf.SetSectorRequest request,
        io.grpc.stub.StreamObserver<protobuf.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getSetSectorMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void configureRMI(protobuf.ConfigureRMIRequest request,
        io.grpc.stub.StreamObserver<protobuf.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getConfigureRMIMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void addVehicle(protobuf.AddVehicleRequest request,
        io.grpc.stub.StreamObserver<protobuf.AddVehicleReply> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getAddVehicleMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void startSimulation(protobuf.StartSimulationRequest request,
        io.grpc.stub.StreamObserver<protobuf.StartSimulationReply> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getStartSimulationMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getVehicles(protobuf.GetVehiclesRequest request,
        io.grpc.stub.StreamObserver<protobuf.GetVehiclesReply> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetVehiclesMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getSimulationResult(protobuf.GetSimulationResultRequest request,
        io.grpc.stub.StreamObserver<protobuf.GetSimulationResultReply> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetSimulationResultMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getMap(protobuf.Empty request,
        io.grpc.stub.StreamObserver<protobuf.GetMapResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetMapMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void removeVehicle(protobuf.RemoveVehicleRequest request,
        io.grpc.stub.StreamObserver<protobuf.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getRemoveVehicleMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void resetSimulator(protobuf.ResetSimulatorRequest request,
        io.grpc.stub.StreamObserver<protobuf.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getResetSimulatorMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void updateTrajectory(protobuf.UpdateTrajectoryRequest request,
        io.grpc.stub.StreamObserver<protobuf.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getUpdateTrajectoryMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class SimControllerBlockingStub extends io.grpc.stub.AbstractStub<SimControllerBlockingStub> {
    private SimControllerBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private SimControllerBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SimControllerBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new SimControllerBlockingStub(channel, callOptions);
    }

    /**
     */
    public protobuf.Empty setMap(protobuf.SetMapRequest request) {
      return blockingUnaryCall(
          getChannel(), getSetMapMethod(), getCallOptions(), request);
    }

    /**
     */
    public protobuf.Empty setSector(protobuf.SetSectorRequest request) {
      return blockingUnaryCall(
          getChannel(), getSetSectorMethod(), getCallOptions(), request);
    }

    /**
     */
    public protobuf.Empty configureRMI(protobuf.ConfigureRMIRequest request) {
      return blockingUnaryCall(
          getChannel(), getConfigureRMIMethod(), getCallOptions(), request);
    }

    /**
     */
    public protobuf.AddVehicleReply addVehicle(protobuf.AddVehicleRequest request) {
      return blockingUnaryCall(
          getChannel(), getAddVehicleMethod(), getCallOptions(), request);
    }

    /**
     */
    public protobuf.StartSimulationReply startSimulation(protobuf.StartSimulationRequest request) {
      return blockingUnaryCall(
          getChannel(), getStartSimulationMethod(), getCallOptions(), request);
    }

    /**
     */
    public protobuf.GetVehiclesReply getVehicles(protobuf.GetVehiclesRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetVehiclesMethod(), getCallOptions(), request);
    }

    /**
     */
    public protobuf.GetSimulationResultReply getSimulationResult(protobuf.GetSimulationResultRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetSimulationResultMethod(), getCallOptions(), request);
    }

    /**
     */
    public protobuf.GetMapResponse getMap(protobuf.Empty request) {
      return blockingUnaryCall(
          getChannel(), getGetMapMethod(), getCallOptions(), request);
    }

    /**
     */
    public protobuf.Empty removeVehicle(protobuf.RemoveVehicleRequest request) {
      return blockingUnaryCall(
          getChannel(), getRemoveVehicleMethod(), getCallOptions(), request);
    }

    /**
     */
    public protobuf.Empty resetSimulator(protobuf.ResetSimulatorRequest request) {
      return blockingUnaryCall(
          getChannel(), getResetSimulatorMethod(), getCallOptions(), request);
    }

    /**
     */
    public protobuf.Empty updateTrajectory(protobuf.UpdateTrajectoryRequest request) {
      return blockingUnaryCall(
          getChannel(), getUpdateTrajectoryMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class SimControllerFutureStub extends io.grpc.stub.AbstractStub<SimControllerFutureStub> {
    private SimControllerFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private SimControllerFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SimControllerFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new SimControllerFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<protobuf.Empty> setMap(
        protobuf.SetMapRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getSetMapMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<protobuf.Empty> setSector(
        protobuf.SetSectorRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getSetSectorMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<protobuf.Empty> configureRMI(
        protobuf.ConfigureRMIRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getConfigureRMIMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<protobuf.AddVehicleReply> addVehicle(
        protobuf.AddVehicleRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getAddVehicleMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<protobuf.StartSimulationReply> startSimulation(
        protobuf.StartSimulationRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getStartSimulationMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<protobuf.GetVehiclesReply> getVehicles(
        protobuf.GetVehiclesRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetVehiclesMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<protobuf.GetSimulationResultReply> getSimulationResult(
        protobuf.GetSimulationResultRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetSimulationResultMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<protobuf.GetMapResponse> getMap(
        protobuf.Empty request) {
      return futureUnaryCall(
          getChannel().newCall(getGetMapMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<protobuf.Empty> removeVehicle(
        protobuf.RemoveVehicleRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getRemoveVehicleMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<protobuf.Empty> resetSimulator(
        protobuf.ResetSimulatorRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getResetSimulatorMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<protobuf.Empty> updateTrajectory(
        protobuf.UpdateTrajectoryRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getUpdateTrajectoryMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_SET_MAP = 0;
  private static final int METHODID_SET_SECTOR = 1;
  private static final int METHODID_CONFIGURE_RMI = 2;
  private static final int METHODID_ADD_VEHICLE = 3;
  private static final int METHODID_START_SIMULATION = 4;
  private static final int METHODID_GET_VEHICLES = 5;
  private static final int METHODID_GET_SIMULATION_RESULT = 6;
  private static final int METHODID_GET_MAP = 7;
  private static final int METHODID_REMOVE_VEHICLE = 8;
  private static final int METHODID_RESET_SIMULATOR = 9;
  private static final int METHODID_UPDATE_TRAJECTORY = 10;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final SimControllerImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(SimControllerImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_SET_MAP:
          serviceImpl.setMap((protobuf.SetMapRequest) request,
              (io.grpc.stub.StreamObserver<protobuf.Empty>) responseObserver);
          break;
        case METHODID_SET_SECTOR:
          serviceImpl.setSector((protobuf.SetSectorRequest) request,
              (io.grpc.stub.StreamObserver<protobuf.Empty>) responseObserver);
          break;
        case METHODID_CONFIGURE_RMI:
          serviceImpl.configureRMI((protobuf.ConfigureRMIRequest) request,
              (io.grpc.stub.StreamObserver<protobuf.Empty>) responseObserver);
          break;
        case METHODID_ADD_VEHICLE:
          serviceImpl.addVehicle((protobuf.AddVehicleRequest) request,
              (io.grpc.stub.StreamObserver<protobuf.AddVehicleReply>) responseObserver);
          break;
        case METHODID_START_SIMULATION:
          serviceImpl.startSimulation((protobuf.StartSimulationRequest) request,
              (io.grpc.stub.StreamObserver<protobuf.StartSimulationReply>) responseObserver);
          break;
        case METHODID_GET_VEHICLES:
          serviceImpl.getVehicles((protobuf.GetVehiclesRequest) request,
              (io.grpc.stub.StreamObserver<protobuf.GetVehiclesReply>) responseObserver);
          break;
        case METHODID_GET_SIMULATION_RESULT:
          serviceImpl.getSimulationResult((protobuf.GetSimulationResultRequest) request,
              (io.grpc.stub.StreamObserver<protobuf.GetSimulationResultReply>) responseObserver);
          break;
        case METHODID_GET_MAP:
          serviceImpl.getMap((protobuf.Empty) request,
              (io.grpc.stub.StreamObserver<protobuf.GetMapResponse>) responseObserver);
          break;
        case METHODID_REMOVE_VEHICLE:
          serviceImpl.removeVehicle((protobuf.RemoveVehicleRequest) request,
              (io.grpc.stub.StreamObserver<protobuf.Empty>) responseObserver);
          break;
        case METHODID_RESET_SIMULATOR:
          serviceImpl.resetSimulator((protobuf.ResetSimulatorRequest) request,
              (io.grpc.stub.StreamObserver<protobuf.Empty>) responseObserver);
          break;
        case METHODID_UPDATE_TRAJECTORY:
          serviceImpl.updateTrajectory((protobuf.UpdateTrajectoryRequest) request,
              (io.grpc.stub.StreamObserver<protobuf.Empty>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class SimControllerBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    SimControllerBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return protobuf.SimulationControllerProto.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("SimController");
    }
  }

  private static final class SimControllerFileDescriptorSupplier
      extends SimControllerBaseDescriptorSupplier {
    SimControllerFileDescriptorSupplier() {}
  }

  private static final class SimControllerMethodDescriptorSupplier
      extends SimControllerBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    SimControllerMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (SimControllerGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new SimControllerFileDescriptorSupplier())
              .addMethod(getSetMapMethod())
              .addMethod(getSetSectorMethod())
              .addMethod(getConfigureRMIMethod())
              .addMethod(getAddVehicleMethod())
              .addMethod(getStartSimulationMethod())
              .addMethod(getGetVehiclesMethod())
              .addMethod(getGetSimulationResultMethod())
              .addMethod(getGetMapMethod())
              .addMethod(getRemoveVehicleMethod())
              .addMethod(getResetSimulatorMethod())
              .addMethod(getUpdateTrajectoryMethod())
              .build();
        }
      }
    }
    return result;
  }
}
