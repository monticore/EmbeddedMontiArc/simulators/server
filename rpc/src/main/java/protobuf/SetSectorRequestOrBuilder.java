/* (c) https://github.com/MontiCore/monticore */
// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: simulationController.proto

package protobuf;

public interface SetSectorRequestOrBuilder extends
    // @@protoc_insertion_point(interface_extends:SimulationController.SetSectorRequest)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>repeated int32 osmIDs = 1;</code>
   */
  java.util.List<java.lang.Integer> getOsmIDsList();
  /**
   * <code>repeated int32 osmIDs = 1;</code>
   */
  int getOsmIDsCount();
  /**
   * <code>repeated int32 osmIDs = 1;</code>
   */
  int getOsmIDs(int index);
}
