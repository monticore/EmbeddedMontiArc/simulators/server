/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.errorflags;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.simulation.PhysicalObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Groups multiple ErrorDeterminator objects together. Should contain only one ErrorDeterminator per ErrorFlag, however,
 * this is not enforced
 */
public class ErrorDeterminatorProcessor {

    private List<ErrorDeterminator> determinators;

    public ErrorDeterminatorProcessor() {
        determinators = new ArrayList<>();
    }

    /**
     * Adds an ErrorDeterminator to the list
     * @param det The determinator to be added
     */
    public void add(ErrorDeterminator det) {
        determinators.add(det);
    }

    /**
     * Calls the check method for every ErrorDeterminator in the list to caluclate the error flags
     * @param info The container of information on the current simulator frame
     * @return a boolean[] containing all error flags (positioned according to their ordinal())
     */
    public boolean[] checkAll(SimFrameInfo info) {
        boolean[] result = new boolean[ErrorFlag.values().length];
        for (ErrorDeterminator e : determinators) {
            if (info.getSimulationLoopExecutable() instanceof PhysicalObject){
                result[e.getHandledErrorFlag().ordinal()] = e.check(info);
            }
        }
        return result;
    }

    /**
     * The ErrorDeterminatorProcessor used by the SimulationObserver to determine ErrorFlags. Please add your custom
     * ErrorDeterminators here.
     * @return Basic ErrorDeterminatorProcessor
     */
    public static ErrorDeterminatorProcessor getBasicProcessor() {
        ErrorDeterminatorProcessor edp = new ErrorDeterminatorProcessor();
        edp.add(new CollisionDeterminator());
        edp.add(new VehicleLeftRoadDeterminator());
        return edp;
    }

}
