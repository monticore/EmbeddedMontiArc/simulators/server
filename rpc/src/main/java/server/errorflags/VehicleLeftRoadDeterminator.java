/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.errorflags;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.simulation.PhysicalObject;
import simulation.environment.WorldModel;
import simulation.environment.geometry.osmadapter.GeomStreet;

/**
 * Calculates the value for the VEHICLE_LEFT_ROAD ErrorFlag
 */
public class VehicleLeftRoadDeterminator extends ErrorDeterminator {

    public VehicleLeftRoadDeterminator() {
        super(ErrorFlag.VEHICLE_LEFT_ROAD);
    }

    @Override
    public boolean check(SimFrameInfo info) {
        GeomStreet street = WorldModel.getInstance().getStreet((PhysicalObject)info.getSimulationLoopExecutable());
        double streetWidth = street.getDeterminator().getStreet().getStreetWidth().doubleValue();
        double distToSpline = street.getDeterminator().determineSplineDistance((PhysicalObject)info.getSimulationLoopExecutable());

        // The following line of code does exactly the same as distToSpline
        // Number distToMiddleOfStreet = WorldModel.getInstance().getDistanceToMiddleOfStreet((PhysicalObject) info.getSimulationLoopExecutable());

        /*String msg = String.format("street: %d, streetWidth: %e, distToSpline: %e, distToMiddle: %e",
                street.getObject().getOsmId(), streetWidth, distToSpline, distToMiddleOfStreet.doubleValue());
        LoggerFactory.getLogger(this.getClass().getName()).info(msg);*/


        return distToSpline > streetWidth / 2;
        // return distToMiddleOfStreet.doubleValue() > streetWidth / 2;
    }
}
