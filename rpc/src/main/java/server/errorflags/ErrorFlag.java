/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.errorflags;

/* this enum determines the semantics of the errorFlags entry in a simulation frame */
public enum ErrorFlag {

    COLLISION,
    VEHICLE_LEFT_ROAD

}
