/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.errorflags;

import simulation.vehicle.PhysicalVehicle;

/**
 * Calculates the Collision ErrorFlag for a SimFrameInfo
 */
public class CollisionDeterminator extends ErrorDeterminator {

    public CollisionDeterminator() {
        super(ErrorFlag.COLLISION);
    }

    @Override
    public boolean check(SimFrameInfo info) {
        return ((PhysicalVehicle) info.getSimulationLoopExecutable()).getCollision();
    }
}
