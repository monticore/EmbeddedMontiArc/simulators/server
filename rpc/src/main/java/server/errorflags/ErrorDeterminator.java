/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.errorflags;

/**
 * Calculates the value for a specific ErrorFlag in a SimFrameInfo
 */
public abstract class ErrorDeterminator {

    private final ErrorFlag errorFlag;

    public ErrorDeterminator(ErrorFlag errorFlag) {
        this.errorFlag = errorFlag;
    }

    public abstract boolean check(SimFrameInfo info);

    public ErrorFlag getHandledErrorFlag() { return errorFlag; }

}
