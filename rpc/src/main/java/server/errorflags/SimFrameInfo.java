/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.errorflags;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.simulation.SimulationLoopExecutable;

/**
 * Container of information on a simulator frame
 */
public class SimFrameInfo {

    private final SimulationLoopExecutable simulationLoopExecutable;

    public SimFrameInfo(SimulationLoopExecutable SimulationLoopExecutable) {
        this.simulationLoopExecutable = SimulationLoopExecutable;
    }

    public SimulationLoopExecutable getSimulationLoopExecutable() {
        return simulationLoopExecutable;
    }
}
