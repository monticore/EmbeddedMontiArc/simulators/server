/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.adapters.AutoPilot;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.BusEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.interfaces.FunctionBlockInterface;

import java.util.HashMap;
import java.util.Map;


/**
 * Autopilot that does nothing and stays in place
 */
public class StandStill implements FunctionBlockInterface {

    @Override
    public Map<String, Object> getOutputs() {
        Map<String, Object> result = new HashMap<>();
        result.put(BusEntry.ACTUATOR_ENGINE.toString(), 0.0d);
        result.put(BusEntry.ACTUATOR_BRAKE.toString(), 0.0d);
        result.put(BusEntry.ACTUATOR_STEERING.toString(), 0.0d);
        return result;
    }

    @Override
    public void execute(double v) {

    }

    @Override
    public void setInputs(Map<String, Object> map) {

    }

    @Override
    public String[] getImportNames() {
        return new String[0];
    }

}
