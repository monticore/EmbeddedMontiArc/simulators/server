/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.rpc;

import com.google.protobuf.ByteString;
import de.monticore.lang.montisim.carlang.CarContainer;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.BusEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.interfaces.FunctionBlockInterface;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.simulation.PhysicalObject;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.simulation.Sensor;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.structures.Graph;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.navigationBlock.components.FindPath;
import de.rwth.monticore.EmbeddedMontiArc.simulators.hardware_emulator.interfaces.SoftwareSimulatorManager;
import de.rwth.monticore.EmbeddedMontiArc.simulators.hardware_emulator.config.ControllerConfig;
import de.rwth.monticore.EmbeddedMontiArc.simulators.rmimodelserver.RMIServer;
import io.grpc.stub.StreamObserver;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import protobuf.*;
import sensors.abstractsensors.AbstractSensor;
import sensors.abstractsensors.StaticPlannedTrajectorySensor;
import server.adapters.AutoPilot.FullSpeedStraight;
import server.adapters.AutoPilot.StandStill;
import simulation.EESimulator.EESimulator;
import simulation.bus.InstantBus;
import simulation.environment.WorldModel;
import simulation.environment.object.ChargingStation;
import simulation.environment.osm.ApproximateConverter;
import simulation.environment.osm.ParserSettings;
import simulation.environment.util.VehicleType;
import simulation.environment.visualisationadapter.interfaces.Building;
import simulation.environment.visualisationadapter.interfaces.EnvBounds;
import simulation.environment.visualisationadapter.interfaces.EnvNode;
import simulation.environment.visualisationadapter.interfaces.EnvStreet;
import simulation.environment.weather.WeatherSettings;
import simulation.simulator.Simulator;
import simulation.vehicle.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.time.Duration;
import java.time.Instant;
import java.util.*;


/**
 * This is a gRPC wrapper of MontiSim simulator. It supports basic operations
 * for setting up, running a simulation task and retrieving its result.
 */
public class SimulationControllerImpl extends SimControllerGrpc.SimControllerImplBase {
    private Simulator simulator;

    private boolean mapInitialized;
    private SimulationObserver simulationObserver;
    private String simulationID;
    
    private final Logger logger = LoggerFactory.getLogger(SimulationControllerImpl.class);

    public SimulationControllerImpl() {
        this.simulator = Simulator.getSharedInstance();
        this.simulator.setSimulationLoopFrequency(30);
        this.mapInitialized = false;
        this.simulationObserver = new SimulationObserver();
        this.simulator.registerLoopObserver(this.simulationObserver);
    }

    /**
     * Start or continue the simulation
     * @param req
     * @param resp
     */
    public void startSimulation(StartSimulationRequest req, StreamObserver<StartSimulationReply> resp) {
        simulationID = req.getSimulationID();
        if (simulationID.isEmpty()){
            simulationID = UUID.randomUUID().toString();
        }

        if (!simulator.isRunning()){
            // logger.info("" + req.getTotalDuration());
            simulator.setSimulationDuration(Duration.ofMillis(req.getTotalDuration()/*ms*/));
            simulator.setSimulationPauseTime(req.getDuration());
            simulator.startSimulation();
        } else {
            if (simulator.isPaused()){
                simulator.continueSimulation(Duration.ofMillis(req.getDuration()));
            } else {
                simulator.setSimulationPauseTime(0);
            }
        }

        resp.onNext(StartSimulationReply.newBuilder().setSimulationID(simulationID).build());
        resp.onCompleted();
    }

    /**
     * Set map for the simulation
     * @param req
     * @param resp
     */
    public void setMap(SetMapRequest req, StreamObserver<Empty> resp) {
        String relativePath = "/" + req.getMapName() + ".osm";
        String absPath = Utils.getWorkDir() + relativePath;
        File targetFile = new File(absPath);

        List<Byte> bytes = new ArrayList<>();
        for (ByteString bs : req.getDataList()) {
            for (Byte b : bs.toByteArray()) {
                bytes.add(b);
            }
        }

        boolean appendToExistingFile = req.getIndex() > 1;
        Byte[] byteToWrite = bytes.toArray(new Byte[bytes.size()]);
        try {
            FileUtils.writeByteArrayToFile(targetFile, ArrayUtils.toPrimitive(byteToWrite), appendToExistingFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (req.getIndex() == req.getTotalChunk()) {
            // if received all map chunks, initialize the virtual world with the new map
            this.setMap(absPath);

            // delete map file. no need to store it in simulator after it has been parsed.
            try {
                FileUtils.forceDelete(new File(absPath));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        resp.onNext(Empty.newBuilder().build());
        resp.onCompleted();
    }

    /**
     * Initialize virtual world with OpenStreetMap data
     * @param mapPath absolute path to the map file
     */
    public void setMap(String mapPath) {
        try {
            WorldModel.init(
                    new ParserSettings(new FileInputStream(mapPath), ParserSettings.ZCoordinates.ALLZERO),
                    new WeatherSettings()
            );
            this.mapInitialized = true;

            for (ChargingStation cs: WorldModel.getInstance().getContainer().getChargingStations()) {
                simulator.registerSimulationObject(cs);
                logger.info("Registered charging station" + cs.getName());
            }

            logger.info("Map set to {}", mapPath);
        } catch (Exception e) {
           logger.error(e.getMessage(), e);
        }
    }

    /**
     * Add a vehicle into the simulator
     *
     * @param req
     * @param resp
     */
    public void addVehicle(AddVehicleRequest req, StreamObserver<AddVehicleReply> resp) {
        CarContainer carContainer = CarContainer.fromJson(req.getVehicleModel());

        if (!this.mapInitialized) {
            resp.onNext(AddVehicleReply.newBuilder().build());
            resp.onCompleted();
            return;
        }

        String globalID = req.getVehicleID();
        if (globalID.isEmpty()){
            String msg = "Global ID is for the vehicle required. Please set it.";
            logger.info(msg);
            resp.onNext(AddVehicleReply.newBuilder().setVehicleID("").build());
            resp.onCompleted();
            return;
        }

        
        try {
            PhysicalVehicle vehicle = createPhysicalVehicle(connectSoftwareSimulatorManager(req.getRmiHost(), (int) req.getRmiPort()), carContainer, globalID);

            // add battery if it's an EV
            if (req.getBatteryCapacity() > 0) {
                vehicle.setVehicleType(VehicleType.ELECTRIC);
                Battery b = new Battery(vehicle.getVehicle(), req.getBatteryCapacity(), req.getBatteryPercentage());
                vehicle.getVehicle().setBattery(b);
            }

            // compute trajectory for the vehicle
            List<Vertex> trajectory = this.setUpTrajectory(
            vehicle,
            Long.valueOf(req.getSourceOsmID()),
            Long.valueOf(req.getTargetOsmID())
            );

            // put vehicle on its start point
            this.simulator.registerAndPutObject(vehicle,
                trajectory.get(0).getPosition().toArray()[0],
                trajectory.get(0).getPosition().toArray()[1],
                Utils.getRotation(trajectory)
            );

            // add the vehicle to observer
            this.simulationObserver.addVehicle(vehicle.getVehicle().getGlobalId(), trajectory);

            resp.onNext(AddVehicleReply.newBuilder().setVehicleID(globalID).build());
            resp.onCompleted();
        } catch(Exception e) {
            e.printStackTrace();
        }
        
    }

    private List<Vertex> setUpTrajectory(PhysicalVehicle vehicle, long source, long target) throws Exception{
        // compute trajectory for the vehicle
        List<Vertex> trajectory = this.computeTrajectory(source, target);
        
        //  setup the vehicle's trajectory
        Map<String, List<Double>> trajectoryCoordinates = Utils.getTrajectoryCoordinates(trajectory);
        Vehicle simVehicle = vehicle.getVehicle();

        Optional<AbstractSensor> sensorX = simVehicle.getEEVehicle().getSensorByType(BusEntry.PLANNED_TRAJECTORY_X);
        if (sensorX.isPresent()) {
            ((StaticPlannedTrajectorySensor)sensorX.get()).initializeTrajectory(trajectoryCoordinates.get("x"));
        } else{
            //simVehicle.addSensor(new StaticPlannedTrajectoryXSensor(trajectoryCoordinates.get("x")));
            throw new Exception("A Vehicle must have planned trajectory sensors.");
        }

        Optional<AbstractSensor> sensorY = simVehicle.getEEVehicle().getSensorByType(BusEntry.PLANNED_TRAJECTORY_Y);
        if (sensorY.isPresent()) {
            ((StaticPlannedTrajectorySensor)sensorY.get()).initializeTrajectory(trajectoryCoordinates.get("y"));
        } else{
            //simVehicle.addSensor(new StaticPlannedTrajectoryYSensor(trajectoryCoordinates.get("y")));
            throw new Exception("A Vehicle must have planned trajectory sensors.");
        }

        logger.info(String.format(
                "Trajectory set to:\n%s\n%s",
                trajectoryCoordinates.get("x"),
                trajectoryCoordinates.get("y")
        ));
        return trajectory;
    }



    /**
     * Compute a trajectory in the map.
     * @param sourceOsmID start
     * @param targetOsmID destination
     * @return a list of vertices(OpenSteetMap nodes)
     */
    private List<Vertex> computeTrajectory(long sourceOsmID, long targetOsmID) {
        Graph graph = new Graph(WorldModel.getInstance().getControllerMap().getAdjacencies(), true);
        FunctionBlock findPath = new FindPath();
        Map<String, Object> inputs = new HashMap<String, Object>();
        inputs.put(ConnectionEntry.FIND_PATH_graph.toString(), graph);
        inputs.put(ConnectionEntry.FIND_PATH_start_vertex.toString(), Utils.getVertexForOsmId(graph, sourceOsmID));
        inputs.put(ConnectionEntry.FIND_PATH_target_vertex.toString(), Utils.getVertexForOsmId(graph, targetOsmID));
        findPath.setInputs(inputs);
        findPath.execute(0);

        List<Vertex> result = (List<Vertex>) findPath.getOutputs().get(ConnectionEntry.FIND_PATH_path.toString());
        for (Vertex v : result) {
            logger.info("{}, {}", v.getOsmId(), Arrays.toString(v.getPosition().toArray()));
        }
        return result;
    }

    /**
     * Create physical vehicle using a model of the Car Modeling Language
     * @param softwareSimManager SoftwareSimulatorManager (RMI)
     * @param carContainer the car model to be used
     * @return
     */
    private PhysicalVehicle createPhysicalVehicle(SoftwareSimulatorManager softwareSimManager, CarContainer carContainer, String golbalId) {
        EESimulator eeSimulator = new EESimulator(Instant.EPOCH);
        InstantBus bus = new InstantBus(eeSimulator);
        EEVehicleBuilder eeVehicleBuilder = new EEVehicleBuilder(eeSimulator);
        //eeVehicleBuilder.createAllSensors(bus);
        for (BusEntry be : BusEntry.values()) {
            if (carContainer.hasSensor(be)) {
                // logger.info(be.toString());
                eeVehicleBuilder.createSensor(be, bus);
            }
        }
        //always add planned trajectory sensors
        eeVehicleBuilder.createSensor(BusEntry.PLANNED_TRAJECTORY_X, bus);
        eeVehicleBuilder.createSensor(BusEntry.PLANNED_TRAJECTORY_Y, bus);
        //TODO move to builder
        HashMap<BusEntry, Integer> sizeByMessageId = new HashMap<>();
        sizeByMessageId.put(BusEntry.ACTUATOR_BRAKE, 8);
        sizeByMessageId.put(BusEntry.ACTUATOR_ENGINE, 8);
        sizeByMessageId.put(BusEntry.ACTUATOR_STEERING, 8);

        PhysicalVehicleBuilder builder;
        switch (carContainer.getPhysicsModel()) {
            case MODELICA:
                builder = new ModelicaPhysicalVehicleBuilder();
                eeVehicleBuilder.createAllActuators(bus);
                break;
            case MASS_POINT: default:
                builder = new MassPointPhysicalVehicleBuilder();
                eeVehicleBuilder.createMassPointActuators(bus);
                break;
        }

        for (ControllerConfig controllerConfig : carContainer.getControllers()){
            if (controllerConfig.getSoftwareName().equals("FullSpeedStraight")) {
                eeVehicleBuilder.createConstantFunctionBlock(bus, sizeByMessageId, new FullSpeedStraight());
            } else if (controllerConfig.getSoftwareName().equals("StandStill")) {
                eeVehicleBuilder.createConstantFunctionBlock(bus, sizeByMessageId, new StandStill());
            } else {
                System.out.println("Cpu Frequ: "+controllerConfig.getCpuFrequencyHertz());
                eeVehicleBuilder.createController(softwareSimManager, controllerConfig, bus);
            }
        }

        

        builder.setMass(carContainer.getMass());
        builder.setWidth(carContainer.getWidth());
        builder.setLength(carContainer.getLength());
        builder.setHeight(carContainer.getHeight());
        builder.setWheelRadius(carContainer.getWheelRadius());

        builder.setWheelDistLeftRightFrontSide(carContainer.getWheelDistFront());
        builder.setWheelDistLeftRightBackSide(carContainer.getWheelDistBack());
        builder.setWheelDistToFront(carContainer.getWheelDistToFront());
        builder.setWheelDistToBack(carContainer.getWheelDistBack());

        builder.setGlobalId(golbalId);

        // cannot set actuators min/max/step values via builder (probably not configurable after all)

        Vehicle vehicle = new Vehicle(builder, eeVehicleBuilder);
        vehicle.setStatusLogger(new RandomStatusLogger());
        PhysicalVehicle physicalVehicle = vehicle.getPhysicalVehicle();

        return physicalVehicle;
    }

    SoftwareSimulatorManager connectSoftwareSimulatorManager(String host, int port) throws Exception {
		try {
			Registry registry = LocateRegistry.getRegistry(host, port);
			return (SoftwareSimulatorManager) registry.lookup(RMIServer.SOFTWARE_SIMULATOR_MANAGER);
		} catch (RemoteException e) {
			throw new Exception("RMI connection problem: " + e);
		} catch (NotBoundException e) {
			throw new Exception("No RMI object associated with [" + RMIServer.SOFTWARE_SIMULATOR_MANAGER + "]: " + e);
		}
	}


    /**
     * Return simulation dafatrames collected by the observer
     *
     * @param req
     * @param resp
     */
    public void getSimulationResult(GetSimulationResultRequest req, StreamObserver<GetSimulationResultReply> resp) {
        resp.onNext(GetSimulationResultReply.newBuilder().addAllVehicles(this.simulationObserver.getResult()).build());
        resp.onCompleted();
    }

    /**
     * Return all registered vehicles. The result contains current position and distance to the destination of each
     * vehicles.
     * @param req
     * @param resp
     */
    public void getVehicles(GetVehiclesRequest req, StreamObserver<GetVehiclesReply> resp) {
        List<String> vehicleIDs = new ArrayList<>();
        List<GetVehiclesReply.VehicleState> states = new ArrayList<>();
        for (Object o : this.simulator.getPhysicalObjects()) {
            if (o instanceof PhysicalVehicle) {
                PhysicalVehicle physical_vehicle = (PhysicalVehicle) o;
                Vehicle vehicle = physical_vehicle.getVehicle();
                double[] position = physical_vehicle.getPosition().toArray();
                ApproximateConverter converter = null;
                try {
                    converter = WorldModel.getInstance().getParser().getConverter().getApproximateConverter();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                double longitude = converter.convertXToLong(position[0], position[1]);
                double latitude = converter.convertYToLat(position[1]);

                Sensor sensorX = vehicle.getSensorByType(BusEntry.PLANNED_TRAJECTORY_X).get();
                Sensor sensorY = vehicle.getSensorByType(BusEntry.PLANNED_TRAJECTORY_Y).get();
                List<Double> trajectoryX = (List<Double>) sensorX.getValue();
                List<Double> trajectoryY = (List<Double>) sensorY.getValue();
                double targetX = trajectoryX.get(trajectoryX.size() - 1);
                double targetY = trajectoryY.get(trajectoryY.size() - 1);
                double dist = Math.sqrt(Math.pow(targetX - position[0], 2) + Math.pow(targetY - position[1], 2));

                SimulationDataFrame sdf = SimulationDataFrame.newBuilder()
                        .setPosX((float) longitude)
                        .setPosY((float) latitude)
                        .setPosZ(0)
                        .build();

                states.add(GetVehiclesReply.VehicleState.newBuilder()
                        .setId(vehicle.getGlobalId())
                        .setState(sdf)
                        .setDistToTargetInMeters((float) dist)
                        .build()
                );
            }
        }

        resp.onNext(GetVehiclesReply.newBuilder().addAllStates(states).build());
        resp.onCompleted();
    }

    /**
     * Reset simulator so that it is like a brand new one.
     * @param req
     * @param resp
     */
    public void resetSimulator(ResetSimulatorRequest req, StreamObserver<Empty> resp) {
        System.out.println("reset simulator");
        if (simulator.isRunning()) {
            simulator.setSimulationPauseTime(0);
        }

        // remove all vehicleGlobalIdToLocalId from simulator
        for (Object o : simulator.getPhysicalObjects()) {
            if (o instanceof PhysicalVehicle) {
                simulator.unregisterPhysicalObject((PhysicalObject) o);
            }
        }

        // remove observer
        Simulator.resetSimulator();
        simulator = Simulator.getSharedInstance();
        simulationObserver = new SimulationObserver();
        simulator.registerLoopObserver(simulationObserver);

        resp.onNext(Empty.newBuilder().build());
        resp.onCompleted();
    }

    /**
     * Remove a vehicle from the simulator
     * @param req
     * @param resp
     */
    public void removeVehicle(RemoveVehicleRequest req, StreamObserver<Empty> resp) {
        for (PhysicalObject v : this.simulator.getPhysicalObjects()) {
            if ((v instanceof PhysicalVehicle) && (((PhysicalVehicle) v).getVehicle().getGlobalId().equals(req.getId()))) {
                simulator.unregisterPhysicalObject(v);
            }
        }

        resp.onNext(Empty.newBuilder().build());
        resp.onCompleted();
    }

    /**
     * Return map data that the simulator is currently using.
     * The result contains streets, buildings and bounds.
     *
     * @param req
     * @param resp
     */
    public void getMap(Empty req, StreamObserver<GetMapResponse> resp) {
        GetMapResponse.Builder mapResp = GetMapResponse.newBuilder();
        try {
            addStreets(mapResp);
            addBuildings(mapResp);
            addBound(mapResp);
        } catch (Exception e) {
            e.printStackTrace();
        }
        resp.onNext(mapResp.build());
        resp.onCompleted();
    }

    private void addStreets(GetMapResponse.Builder resp) throws Exception {
        for (EnvStreet street : WorldModel.getInstance().getContainer().getStreets()) {
            List<GetMapResponse.OsmNode> nodes = getLonLatNodesFromXYNodes(street.getNodes());
            List<GetMapResponse.OsmNode> intersections = getLonLatNodesFromXYNodes((List<EnvNode>) street.getIntersections());
            resp.addStreets(GetMapResponse.Street.newBuilder()
                    .setId(street.getOsmId())
                    .setWidth(street.getStreetWidth().floatValue())
                    .addAllNodes(nodes)
                    .addAllIntersections(intersections)
                    .setStreetType(street.getStreetType().toString())
                    .setIsOneWay(street.isOneWay())
                    .setStreetPavements(street.getStreetPavement().toString())
                    .setSpeedLimit((double) street.getSpeedLimit())
                    .build());
        }
    }

    /**
     * Convert nodes' positions from meters to longitude/latitude/altitude.
     *
     * @param nodes
     * @return
     */
    private List<GetMapResponse.OsmNode> getLonLatNodesFromXYNodes(List<EnvNode> nodes) {
        List<GetMapResponse.OsmNode> result = new ArrayList<>();
        for (EnvNode node : nodes) {
            double x = node.getX().doubleValue(), y = node.getY().doubleValue();
            ApproximateConverter converter = null;
            try {
                converter = WorldModel.getInstance().getParser().getConverter().getApproximateConverter();
            } catch (Exception e) {
                e.printStackTrace();
            }

            GetMapResponse.OsmNode osmNode = GetMapResponse.OsmNode.newBuilder()
                    .setId((int) node.getOsmId())
                    .setLonitude((float) converter.convertXToLong(x, y))
                    .setLatitude((float) converter.convertYToLat(x))
                    .setAltitude(node.getZ().floatValue())
                    .build();
            result.add(osmNode);
        }
        return result;
    }

    private void addBound(GetMapResponse.Builder resp) throws Exception {
        EnvBounds bound = WorldModel.getInstance().getContainer().getBounds();
        double minX = bound.getMinX(), minY = bound.getMinY(),
                maxX = bound.getMaxX(), maxY = bound.getMaxY();
        ApproximateConverter converter = null;
        try {
            converter = WorldModel.getInstance().getParser().getConverter().getApproximateConverter();
        } catch (Exception e) {
            e.printStackTrace();
        }
        resp.setBound(GetMapResponse.Bound.newBuilder()
                .setMinX((float) converter.convertXToLong(minX, minY))
                .setMinY((float) converter.convertYToLat(minY))
                .setMinZ((float) bound.getMinZ())
                .setMaxX((float) converter.convertXToLong(maxX, maxY))
                .setMaxY((float) converter.convertYToLat(maxY))
                .setMaxZ((float) bound.getMaxZ())
                .build());
    }

    private void addBuildings(GetMapResponse.Builder resp) throws Exception {
        for (Building building : WorldModel.getInstance().getContainer().getBuildings()) {
            resp.addBuildings(
                    GetMapResponse.Building.newBuilder().addAllNodes(
                            getLonLatNodesFromXYNodes(building.getNodes())).build()
            );
        }
    }

    public void updateTrajectory(UpdateTrajectoryRequest req, StreamObserver<Empty> resp) {
        for (Object o : this.simulator.getPhysicalObjects()) {
            if (o instanceof PhysicalVehicle && ((PhysicalVehicle) o).getVehicle().getGlobalId().equals(req.getVehicleID())) {
                try{
                    setUpTrajectory(
                        (PhysicalVehicle)o,
                        Long.valueOf(req.getTargetOsmID()),
                        Long.valueOf(req.getTargetOsmID())
                    );
                } catch(Exception e){
                    e.printStackTrace();
                }
                
                resp.onNext(Empty.newBuilder().build());
                resp.onCompleted();
                return;
            }
        }
    }
}

