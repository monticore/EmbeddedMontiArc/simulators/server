/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.rpc;

import org.I0Itec.zkclient.ZkClient;

public interface RegistableService {
    void register(ZkClient zk, String host, int posrt);
}
