/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.rpc;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.BusEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.simulation.Sensor;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.simulation.SimulationLoopExecutable;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.simulation.SimulationLoopNotifiable;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import protobuf.BooleanArray;
import protobuf.DoubleArray;
import protobuf.SimulationDataFrame;
import protobuf.VehicleSimulationDataFrame;
import server.errorflags.ErrorDeterminatorProcessor;
import server.errorflags.ErrorFlag;
import server.errorflags.SimFrameInfo;
import simulation.environment.util.IBattery;
import simulation.environment.osm.ApproximateConverter;
import simulation.environment.WorldModel;
import simulation.vehicle.PhysicalVehicle;
import simulation.vehicle.Vehicle;
import simulation.vehicle.VehicleActuatorType;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.Duration;
import java.time.Instant;
import java.util.*;


/**
 * SimulationObserver collects simulation result from simulators
 */
public class SimulationObserver extends SimulationLoopNotifiable {
    private String topic;
    private List<String> vehicleIDs;
    private List<SimulationDataframe> simulationFrames;
    private Map<String, VehicleSimulationDataFrame.Builder> frames;
    private HashMap<String, List<Vertex>> plannedTrajectory;
//    private SimulationControllerImpl controller;

    private boolean[] errorFlagBuffer;

    private final Logger logger = LoggerFactory.getLogger(SimulationObserver.class);

    public SimulationObserver() {
        this.vehicleIDs = new ArrayList<>();
        this.simulationFrames = new ArrayList<>();
        this.plannedTrajectory = new HashMap<>();
        frames = new HashMap<>();
    }

    public void addVehicle(String globalID, List<Vertex> plannedTrajectory) {
        if (!this.vehicleIDs.contains(globalID)) {
            this.vehicleIDs.add(globalID);
        }
        if (!this.frames.containsKey(globalID)) {
            this.frames.put(globalID, VehicleSimulationDataFrame.newBuilder().setVehicleID(globalID));
        }
        this.plannedTrajectory.put(globalID, plannedTrajectory);
    }

    /**
     * @return collected simulation results
     */
    public List<VehicleSimulationDataFrame> getResult() {
        List<VehicleSimulationDataFrame> result = new ArrayList<>();
        for (VehicleSimulationDataFrame.Builder builder : frames.values()) {
            result.add(builder.build());
        }
        return result;
    }

    @Override
    public void willExecuteLoopForObject(SimulationLoopExecutable simulationLoopExecutable, Instant l, Duration l1) {
        errorFlagBuffer = ErrorDeterminatorProcessor.getBasicProcessor().checkAll(new SimFrameInfo(simulationLoopExecutable));
    }

    @Override
    public void didExecuteLoopForObject(SimulationLoopExecutable simulationLoopExecutable, Instant l, Duration l1) {
        if (!(simulationLoopExecutable instanceof PhysicalVehicle)) return;
        long totalTimeMs = Duration.between(Instant.EPOCH, l).toMillis();
        long timeDiffMs = l1.toMillis();
        PhysicalVehicle physical_vehicle = (PhysicalVehicle) simulationLoopExecutable;
        Vehicle vehicle = physical_vehicle.getVehicle();
        double[] position = physical_vehicle.getPosition().toArray();
        String id = String.valueOf((int) physical_vehicle.getId());

        // uncomment the lines below to use longitude/latitude coordinate
        ApproximateConverter cvt = null;
        try {
            cvt = WorldModel.getInstance().getParser().getConverter().getApproximateConverter();
        } catch (Exception e) {
            e.printStackTrace();
        }
        double longitude = cvt.convertXToLong(position[0], position[1]);
        double latitude = cvt.convertYToLat(position[1]);

//        double longitude = position[0];
//        double latitude = position[1];

        SimulationDataframe frame = new SimulationDataframe();
        frame.setId(vehicle.getGlobalId());
        frame.setPosition(new double[]{longitude, latitude, position[2]});
        frame.setSteering(physical_vehicle.getSteeringAngle());
        frame.setTotalTime(totalTimeMs);
        frame.setDeltaTime(timeDiffMs);

        // Collision detection has to be handled in willExecuteLoopForObject
        /*if (physical_vehicle.getCollision()) {
            logger.info("collision in rpc (delfo)");
        }*/

        // check for error flags
        SimFrameInfo info = new SimFrameInfo(simulationLoopExecutable);
        ErrorDeterminatorProcessor processor = ErrorDeterminatorProcessor.getBasicProcessor();
        boolean[] computedErrorFlags = processor.checkAll(info);

        // compare with previously set error flags (didExecuteLoopForObject)
        for (ErrorFlag ef : ErrorFlag.values()) {
            computedErrorFlags[ef.ordinal()] |= errorFlagBuffer[ef.ordinal()];
        }

        frame.setErrorFlags(computedErrorFlags);

        simulationFrames.add(frame);
        Sensor sensorX = physical_vehicle.getVehicle().getSensorByType(BusEntry.PLANNED_TRAJECTORY_X).get();
        Sensor sensorY = physical_vehicle.getVehicle().getSensorByType(BusEntry.PLANNED_TRAJECTORY_Y).get();
        List<Double> trajectoryX = (List<Double>) sensorX.getValue();
        List<Double> trajectoryY = (List<Double>) sensorY.getValue();
        double targetX = trajectoryX.get(trajectoryX.size() - 1);
        double targetY = trajectoryY.get(trajectoryY.size() - 1);
        double dist = Math.sqrt(Math.pow(targetX - position[0], 2) + Math.pow(targetY - position[1], 2));

        String msg = String.format("id: %s, position: [%.8f, %.8f, %.6f], target: (%.8f, %.8f), dist: %.8f, torque: %.3f",
                id, position[0], position[1], position[2], targetX, targetY, dist,
                vehicle.getEEVehicle().getActuator(VehicleActuatorType.VEHICLE_ACTUATOR_TYPE_MOTOR).get().getActuatorValueCurrent());
        if (physical_vehicle.isChargeable()) {
            msg += String.format(", battery: %.2f", physical_vehicle.getBattery().get().getBatteryPercentage());
        }
        logger.info(msg);

        // send frame to kafka, so that clients can retrieve it in real time, or, anytime.
//        kafkaProducer.send(new ProducerRecord<>(topic, frame.toJSON().toString()));

        String globalID = vehicle.getGlobalId();
        SimulationDataFrame.Builder vframe = SimulationDataFrame.newBuilder();
        vframe.setPosX((double) longitude).setPosY((double) latitude).setPosZ((double) position[2]);
        vframe.setSteering((float) physical_vehicle.getSteeringAngle());
        vframe.setTotalTime(totalTimeMs);
        vframe.setDeltaTime(timeDiffMs);

        // set battery
        float batteryLevel = -1;
        Optional<IBattery> battery = vehicle.getBattery();
        if (battery.isPresent()){
            batteryLevel = (float) battery.get().getBatteryPercentage();
        }
        vframe.setBattery(batteryLevel);

        // set charging status
        boolean isCharging = physical_vehicle.isCharging();
        vframe.setIsCharging(isCharging);

        // set rotation
        double[][] rotation = physical_vehicle.getRotation().getData();
        for (int i = 0; i < rotation.length; i++) {
            DoubleArray.Builder builder = DoubleArray.newBuilder();
            for (int j = 0; j < rotation[i].length; j++) {
                builder.addValue(rotation[i][j]);
            }
            vframe.addRotation(builder.build());
        }

        // set velocity
        double[] data = physical_vehicle.getVelocity().toArray();
        DoubleArray.Builder velocity = DoubleArray.newBuilder();
        for (int i = 0; i < data.length; i++) {
            velocity.addValue(data[i]);
        }
        vframe.setVelocity(velocity.build());

        // set acceleration
        double mass = physical_vehicle.getMass();
        data = physical_vehicle.getForce().mapDivide(mass).toArray();
        DoubleArray.Builder acceleration = DoubleArray.newBuilder();
        for (int i = 0; i < data.length; i++) {
            acceleration.addValue(data[i]);
        }
        vframe.setAcceleration(acceleration.build());

        BooleanArray.Builder errorFlags = BooleanArray.newBuilder();
        for (boolean b : frame.getErrorFlags()) {
            errorFlags.addValue(b);
        }
        vframe.setErrorFlags(errorFlags);

        frames.get(globalID).addFrames(vframe.build());
    }

    @Override
    public void simulationStarted(List<SimulationLoopExecutable> list) {

    }

    /**
     * Return simulation result as JSON
     *
     * @return
     */
    public JSONObject toJSON() {
        // {
        //     "vehicleID":{
        //         "plannedTrajectory": [
        //             {"osmID": 123456, "position": [0,0,0]}
        //         ],
        //         "frames":[
        //             {
        //                 "position": [0,0,0],
        //                 "steering": 1,
        //                 "totalTime": 1,
        //                 "deltaTime" 1
        //             }
        //         ]
        //     }
        // }
        JSONObject result = new JSONObject();
        for (String id : this.vehicleIDs) {
            JSONObject vehicleData = new JSONObject();
            vehicleData.put("plannedTrajectory", this.parsePlannedTrajectory(this.plannedTrajectory.get(id)));
            vehicleData.put("frames", parseVehicleDataframes(id));
            result.put(id, vehicleData);
        }
        return result;
    }



    private JSONArray parsePlannedTrajectory(List<Vertex> trajectory) {
        JSONArray result = new JSONArray();
        for (Vertex vtx : trajectory) {
            JSONObject data = new JSONObject();
            data.put("osmID", vtx.getOsmId());
            data.put("position", arrayToJSONArray(vtx.getPosition().toArray()));
            result.add(data);
        }
        return result;
    }

    private JSONArray parseVehicleDataframes(String vehicleID) {
        JSONArray result = new JSONArray();
        for (SimulationDataframe frame : this.simulationFrames) {
            if (frame.getId() != vehicleID) {
                continue;
            }
            result.add(frame.toJSON());
        }

        return result;
    }

    private static JSONArray arrayToJSONArray(double[] array) {
        JSONArray result = new JSONArray();
        for (double data : array) {
            result.add(data);
        }
        return result;
    }

    private static JSONArray arrayToJSONArray(boolean[] array) {
        JSONArray result = new JSONArray();
        for (boolean data : array) {
            result.add(data);
        }
        return result;
    }

    public void dumpResult() {
        JSONObject result = this.toJSON();
        StringWriter out = new StringWriter();
        try {
            result.writeJSONString(out);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String jsonText = out.toString();
        PrintWriter writer = null;
        try {
            writer = new PrintWriter("simResult.json");
            writer.println(jsonText);
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private class SimulationDataframe {
        private String id;
        private double[] position;
        private double steering;
        private double gas;
        private double brake;
        private long totalTime;
        private long deltaTime; // ?
        private boolean[] errorFlags;

        public SimulationDataframe() {
            errorFlags = new boolean[ErrorFlag.values().length];
        }

        public JSONObject toJSON() {
            JSONObject result = new JSONObject();
            result.put("vehicleID", id);
            result.put("position", SimulationObserver.arrayToJSONArray(position));
            result.put("steering", steering);
            result.put("totalTime", totalTime);
            result.put("deltaTime", deltaTime);
            result.put("errorFlags", SimulationObserver.arrayToJSONArray(errorFlags));
            return result;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public double[] getPosition() {
            return position;
        }

        public void setPosition(double[] position) {
            this.position = position;
        }

        public double getSteering() {
            return steering;
        }

        public void setSteering(double steering) {
            this.steering = steering;
        }

        public double getGas() {
            return gas;
        }

        public void setGas(double gas) {
            this.gas = gas;
        }

        public double getBrake() {
            return brake;
        }

        public void setBrake(double brake) {
            this.brake = brake;
        }

        public long getTotalTime() {
            return totalTime;
        }

        public void setTotalTime(long totalTime) {
            this.totalTime = totalTime;
        }

        public long getDeltaTime() {
            return deltaTime;
        }

        public void setDeltaTime(long deltaTime) {
            this.deltaTime = deltaTime;
        }

        public void setErrorFlag(ErrorFlag flag, boolean value) {
            errorFlags[flag.ordinal()] = value;
        }

        public boolean getErrorFlag(ErrorFlag flag) {
            return errorFlags[flag.ordinal()];
        }

        public boolean[] getErrorFlags() {
            return errorFlags;
        }

        public void setErrorFlags(boolean[] errorFlags) {
            this.errorFlags = errorFlags;
        }
    }
}
