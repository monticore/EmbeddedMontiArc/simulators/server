/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.rpc;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.I0Itec.zkclient.ZkClient;
import org.I0Itec.zkclient.exception.ZkTimeoutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetAddress;

/**
 * A gRPC server that serves simulation requests
 */
public class RpcServer implements RegistableService{
    // this simulator will register its hostname and port in this node
    // so that clients can know it is live by querying /simulators
    private final String ZK_NODE = "/simulators";

    private  int port;
    private Server server;
    private String zkPath;

    private final Logger logger = LoggerFactory.getLogger(RpcServer.class);

    public RpcServer(int port){
        this.port = port;
    }

    private void start() throws IOException{
        logger.info("Server starting on port {}", port);

        server = ServerBuilder.forPort(port)
                .addService(new SimulationControllerImpl())
                .build()
                .start();
        logger.info("gRPC server started");

        String zooServers = System.getenv("ZOO_SERVERS");
        if (zooServers == null){
            zooServers = "localhost:2181";
        }

        ZkClient zk = null;
        try{
            zk = new ZkClient(zooServers, 5000, 5000);
            register(zk, InetAddress.getLocalHost().getHostName(), port);
        }catch (ZkTimeoutException e) {
            logger.info("Zookeeper not available, not using zookeeper");
        }

        ZkClient finalZk = zk;
        Runtime.getRuntime().addShutdownHook(new Thread(){
            @Override
            public void run(){
                logger.info("shutting down gRPC server since JVM is shutting down");
                RpcServer.this.stop();
                if (finalZk != null){
                    finalZk.delete(zkPath);
                }
                logger.info("server shut down");
            }
        });
    }

    private void stop(){
        if (server != null){
            server.shutdown();
        }
    }

    private void blockUntilShutdown() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }

    public static void main(String[] args) throws IOException, InterruptedException {

        final RpcServer server = new RpcServer(Integer.valueOf(args[0]));
        server.start();
        server.blockUntilShutdown();
    }

    @Override
    public void register(ZkClient zk, String host, int port) {
        String path = ZK_NODE + "/" + host + ":" + port;
        if (!zk.exists(ZK_NODE)){
            zk.createPersistent(ZK_NODE, true);
        }

        // delete in case a node is already there
        // this usually happens when a simulator was forced shutdown without unregister itself from zookeeper
        zk.delete(path);
        zk.createEphemeral(path);
        zkPath = path;
        logger.info("{} is online", path);
    }
}
