/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package server.rpc;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.structures.Graph;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import de.topobyte.osm4j.xml.dynsax.OsmXmlReader;

public class Utils {
    public static Vertex getVertexForOsmId(Graph graph, long osmId) {
        for (Vertex v : graph.getVertices())
            if (v.getOsmId().longValue() == osmId) return v;
        return null;
    }

    public static double getRotation(List<Vertex> trajectory) {
        if (trajectory.size() < 2) return 0d;
        //compare first two vectors, to get correct start rotation
        double[] v1pos = trajectory.get(0).getPosition().toArray();
        double[] v2pos = trajectory.get(1).getPosition().toArray();

        //slightly change the rotation from perfect straight line,
        //otherwise the simulator calculates strange starting trajectory
        return Math.atan2(v1pos[0] - v2pos[0], v2pos[1] - v1pos[1]) -0.35d;
    }

    public static Map<String, List<Double>> getTrajectoryCoordinates(List<Vertex> trajectory) {
        Map<String, List<Double>> result = new HashMap<String, List<Double>>();

        List<Double> x = new ArrayList<Double>();
        List<Double> y = new ArrayList<Double>();
        for (Vertex v : trajectory) {
            double[] pos = v.getPosition().toArray();
            x.add(pos[0]);
            y.add(pos[1]);
        }
        result.put("x", x);
        result.put("y", y);

        return result;
    }

    public static String getWorkDir(){
        String jarPath = RpcServer.class.getResource("/").getPath()
                .replaceAll("^.*file:", "")
                .replaceAll("jar!.*", "jar");
        if (jarPath.indexOf("/")==0){
            jarPath = jarPath.substring(1);
        }
        if(jarPath.contains("test-classes")){
            return Paths.get(Paths.get(jarPath).getParent().toString(), "test-classes").toString();
        }
        return Paths.get(Paths.get(jarPath).getParent().toString(),  "classes").toString();
    }
}


