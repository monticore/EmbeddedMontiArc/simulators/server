<!-- (c) https://github.com/MontiCore/monticore -->
This module implements a gRPC wrapper for MontiSim simulator.

## Install
Install using maven:
```
$ mvn clean install -settings=../settings.xml -DskipTests
```
Run the rpc server at **localhost:6000**:
```
$ java -cp "target/rpc-jar-with-dependencies.jar" server.rpc.RpcServer 6000
```
Or use IDE to start the main class at `/path/to/server/rpc/src/main/java/server/rpc/RpcServer.java`.   
Don't forget to set the program argument telling rpc server on which port it should listen.

## OpenModelica
The simulator supports 2 different physic models: OpenModelica and masspoint vehicle model.
To be able to use OpenModelica model, the `.fmu` files have to be compiled first.
For further instructions please refer to: 
[OpenModelica](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/simulators/server/blob/master/README.md#configuration-of-openmodelica)
The `.fmu` files should be put `rpc/lib` directory.

## Docker
Build the docker image after the jar is built:
```
$ ./build.sh
```
Run the rpc server:
```
$ docker run --rm -p 6000:6000 rpc-server:latest
```
This command will start a rpc service and serve requests at **localhost:6000** 

To bind other port, for example 6666:
```
$ docker run --rm -p 6666:6000 rpc-server:latest
```

## gRPC
The gRPC interfaces are defined in `rpc/src/main/proto/simulationController.proto` file.
By default the generated java code are provided in `rpc/src/proto` package.
If any modification are introduced into the `proto` file, it is necessary to use the 
protubuf java codegen to generate the new java code. To do so, uncomment the `os-maven-plugin` extension and
`protobuf-maven-plugin` in the pom, remove `rpc/src/proto` package and run mvn compile or mvn install.
The new generated code can be found in `rpc/target/generated-sources`.

MacOS does not contain maven protobuf code generator by default. To use maven to generate java code for the proto file, 
it is required to install the protobuf code generator into maven. To do so, go to [here](https://repo1.maven.org/maven2/io/grpc/protoc-gen-grpc-java/)
to download the generator for you OS, and use
```bash
$ mvn install:install-file -DgroupId=io.grpc -DartifactId=protoc-gen-grpc-java -Dversion=[version] -Dclassifier=[OS]-x86_64 -Dpackaging=exe -Dfile=/path/to/protoc-gen-grpc-java-[version]-[OS]-osx-x86_64.exe
```   


 
# Error flags

Error flags denote potential driving errors that the autopilot made during a simulation. They are calculated for every frame and every vehicle in the simulation. To add custom error flags, you have to:
* Add a new entry to the enum `ErrorFlag`
* Create a subclass of `ErrorDeterminator`
* Add an entry in `ErrorDeterminatorProcessor.getBasicProcessor()` for your new class

Error flags can be queried from the json results of a simulation. They are stored as a boolean array, where the position in the array corresponds to the ordinal of the specific `ErrorFlag`. I.e., if you want to query the value for the `ErrorFlag.COLLISION`, you can use:
```
errorFlags[ErrorFlag.COLLISION.ordinal()]
```

# Trajectory re-planing

Sometimes electric vehicles need to navigate to nearest charging station during the simulation. 
This function is built in in the simulator, however, in case of multi-sector simulation, this can't be done
without the help from restful server. As a result, in multi-sector simulation, we must inform
the simulator about the existence of the restful server. To do so, simply set 2 environment variables:

```bash
export SIM_SERVER=[ip or host name of the restful server]
export SIM_PORT=[port of the restful server, by default it is 8090]
```

For more details please refer to montisim-simulation:environment.util.ChargingStationNavigator
and montisim-server.restful.service.VehicleService::insertNewRoadPoint
